from math import ceil
class Room:

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.wd = []

    def addWD(self, w, h):
        return self.wd.append(w * h)
        
    def workSurface(self):
        new_square = Room.self_square(self)
        for i in self.wd:
            new_square -= i
        return new_square

    def self_square(self):
        self.square = 2 * self.z * (self.x + self.y)
        return self.square
    
    def wallpaper(self, x, y):
        num = (Room.workSurface(self)) / (x * y)
        return ceil(num)








