class Book:

  libr = {
    '001': {
        'name' : 'Черный обелиск',
        'author' : 'Ремарк',
        'pages' : '300',
        'isbn' : '001',
        'reserved_flag': False,
        'on_hands_flag': False
        },
    '002': {
            'name' : '1984',
            'author' : 'Оруэлл',
            'pages' : '350',
            'isbn' : '002',
            'reserved_flag': False,
            'on_hands_flag': False
        }
  }
  
  on_hands_libr = []
  

  def add_book(self, name, author, pages, isbn):
    Book.libr[isbn] = {
            'name' : name,
            'author' : author,
            'pages' : pages,
            'isbn' : isbn,
            'reserved_flag': False,
            'on_hands_flag': False
            }


class User:   

  def reserve_book(self, isbn):
    if Book.libr[isbn]['reserved_flag'] == False:
       Book.libr[isbn]['reserved_flag'] = True
    else:
      print('Sorry, no')

  def decline_reserve_book(self, isbn):
    if Book.libr[isbn]['reserved_flag'] == True:
        Book.libr[isbn]['reserved_flag'] = False
    else:
        print('Sorry, no')


  def give_book(self, isbn):
    if Book.libr[isbn]['on_hands_flag'] == False and Book.libr[isbn]['reserved_flag'] == False:
      Book.libr[isbn]['on_hands_flag'] = True
      Book.libr[isbn]['reserved_flag'] = True
      print(f"Your book {Book.libr[isbn]['name']}")
    elif Book.libr[isbn]['on_hands_flag'] == False and Book.libr[isbn]['reserved_flag'] == True:
      print('Book is reserved. Sorry')
    else: 
      print('Book is on hands. Sorry')
  


  def take_book(self, isbn):
    if Book.libr[isbn]['on_hands_flag'] == True  and Book.libr[isbn]['reserved_flag'] == True:
      Book.libr[isbn]['on_hands_flag'] == False
      Book.libr[isbn]['reserved_flag'] = False
    else:
      print('Not our book.')


