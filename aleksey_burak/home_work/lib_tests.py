import unittest
import library_home_work


class TestClassBook(unittest.TestCase):

    def setUp(self):
        library_home_work.Book.libr['test_isbn'] = {
            'name': 'test_name',
            'author': 'test_author',
            'pages': 'test_pages',
            'isbn': 'test_isbn',
            'reserved_flag': False,
            'on_hands_flag': False
        }

    def tearDown(self):
        library_home_work.Book.libr.pop('test_isbn')

    def test_add_book_positive(self):
        self.assertEqual(library_home_work.Book.libr['test_isbn']['name'], 'test_name')
        self.assertEqual(library_home_work.Book.libr['test_isbn']['author'], 'test_author')
        self.assertEqual(library_home_work.Book.libr['test_isbn']['pages'], 'test_pages')
        self.assertEqual(library_home_work.Book.libr['test_isbn']['isbn'], 'test_isbn')
        self.assertEqual(library_home_work.Book.libr['test_isbn']['reserved_flag'], False)
        self.assertEqual(library_home_work.Book.libr['test_isbn']['on_hands_flag'], False)


if __name__ == '__main__':
    unittest.main()


class TestClassUser(unittest.TestCase):
    def setUp(self):
        library_home_work.Book.libr['test_isbn'] = {
            'name': 'test_name',
            'author': 'test_author',
            'pages': 'test_pages',
            'isbn': 'test_isbn',
            'reserved_flag': False,
            'on_hands_flag': False
        }

    def tearDown(self):
        library_home_work.Book.libr.pop('test_isbn')

    def test_reserve_book_pos(self):
        library_home_work.User.reserve_book(self, 'test_isbn')
        self.assertTrue(library_home_work.Book.libr['test_isbn']['reserved_flag'])

    def test_decline_reserve_book_pos(self):
        library_home_work.User.reserve_book(self, 'test_isbn')
        library_home_work.User.decline_reserve_book(self, 'test_isbn')
        self.assertFalse(library_home_work.Book.libr['test_isbn']['reserved_flag'])

    def test_give_book_pos(self):
        library_home_work.User.give_book(self, 'test_isbn')
        self.assertTrue(library_home_work.Book.libr['test_isbn']['reserved_flag'])
        self.assertTrue(library_home_work.Book.libr['test_isbn']['on_hands_flag'])

    def test_take_book_pos(self):
        library_home_work.User.take_book(self, 'test_isbn')
        self.assertFalse(library_home_work.Book.libr['test_isbn']['reserved_flag'])
        self.assertFalse(library_home_work.Book.libr['test_isbn']['on_hands_flag'])

