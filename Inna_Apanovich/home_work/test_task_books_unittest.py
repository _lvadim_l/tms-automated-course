import unittest
from task_books import Book, Reader


class Test_Book(unittest.TestCase):

    def setUp(self):
        self.test_book = Book('American Gods', 'Neil Gaiman', 465, '0-380-97365-0')

    def tearDown(self):
        del self.test_book

    def test_book_initiation(self):
        self.assertIsInstance(self.test_book, Book)

    def test_book_title(self):
        self.assertEqual(self.test_book.title, 'American Gods')


class Test_Reader(unittest.TestCase):

    def setUp(self):
        self.test_reader = Reader('Inna', 'Apanovich')

    def tearDown(self):
        del self.test_reader

    def test_reader_initiation(self):
        self.assertIsInstance(self.test_reader, Reader)

    def test_borrow(self):
        self.test_book1 = Book('Good Omens', 'Neil Gaiman', 416, '978-1473227835')
        self.test_reader.borrow('Good Omens')
        self.assertIsNotNone(self.test_reader.borrowed_books)

    def test_take_back(self):
        self.test_book2 = Book('Neverwhere', 'Neil Gaiman', 464, '0-7553-2280-0')
        self.test_reader.borrow('Neverwhere')
        self.test_reader.take_back('Neverwhere')
        self.assertNotIn('Neverwhere', self.test_reader.borrowed_books)

    def test_reserve(self):
        self.test_book3 = Book('Coraline', 'Neil Gaiman', 157, '978-1408841754')
        self.test_reader.reserve('Coraline')
        self.assertEqual(self.test_book3.reservation, True)


if __name__ == '__main__':
    unittest.main()