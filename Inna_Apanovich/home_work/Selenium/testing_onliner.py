from selenium import webdriver
from selenium.webdriver.common.by import By
import time

def test(driver):
    driver.get('https://www.onliner.by/')
    time.sleep(1)

    search_elem = driver.find_element(By.XPATH, '//input[@class="fast-search__input"]')
    search_elem.send_keys("iphone12")
    time.sleep(1)

    iframe = driver.find_element(By.CLASS_NAME, 'modal-iframe')
    driver.switch_to.frame(iframe)
    time.sleep(1)

    baraholka_elem = driver.find_element(By.XPATH, '//div[contains(text(), "на барахолке")]')
    baraholka_elem.click()
    time.sleep(1)

    iphone_elem = driver.find_element(By.XPATH, '//img[@class="search__widget-image][1]')
    iphone_elem.click()
    time.sleep(1)

    driver.switch_to.window(driver.window_handles[0])
    time.sleep(1)

    iphone_heading = driver.find_element(By.XPATH, '//h1[@class="catalog-masthead__title"]')
    assert iphone_heading is ' Смартфон Apple iPhone 12 Pro 128GB (графитовый)'
    time.sleep(5)