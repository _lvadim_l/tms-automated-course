import pytest
from selenium import webdriver


@pytest.fixture()
def driver():
    print("\nstart browser for test..")
    browser = webdriver.Firefox(executable_path="/home/inna/tms-automated-course/Inna_Apanovich/geckodriver")
    browser.maximize_window()
    yield browser
    print("\nquit browser..")
    browser.quit()

