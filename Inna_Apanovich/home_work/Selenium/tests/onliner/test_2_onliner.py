"""
1. Перейти на сайт https://www.onliner.by/
2. Перейти в каталог
3. Выбрать раздел "Авто и мото"
4. Выбрать "Автоакустика"
5. Выбрать "Кабели для автоакустики"
6. Убедиться, что мы находимся в разделе "Автоакустика"
7. Кликнуть на первый товар
8. Добавить товар в корзину
9. Перейти в корзину
10. Проверить наличие товара в корзине
"""

from Inna_Apanovich.home_work.Selenium.pages.onliner.home_page import HomePage
from Inna_Apanovich.home_work.Selenium.pages.onliner.catalogue_page import CataloguePage
from Inna_Apanovich.home_work.Selenium.pages.onliner.cart_page import CartPage

cables_text = 'Кабели, адаптеры, разветвители'
item_cable_text = 'Aura SCC-3150'


def test_auto_acoustics_item_search(driver):
    driver.get("https://www.onliner.by/")
    home_page = HomePage(driver)
    home_page.switch_to_catalogue()
    catalogue_page = CataloguePage(driver)
    catalogue_page.switch_to_auto_and_moto_section()
    catalogue_page.switch_to_auto_acoustics_subsection()
    catalogue_page.search_auto_acoustics_cables()
    auto_acoustics_headline_text = catalogue_page.get_auto_acoustics_headline_text()
    assert cables_text in auto_acoustics_headline_text
    catalogue_page.click_to_the_first_item_in_catalogue()
    catalogue_page.click_on_cart_button()
    catalogue_page.switch_to_cart()
    cart_page = CartPage(driver)
    card_text = cart_page.get_card_text()
    assert item_cable_text in card_text

