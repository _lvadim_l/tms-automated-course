from Inna_Apanovich.home_work.Selenium.pages.onliner.home_page import HomePage
from Inna_Apanovich.home_work.Selenium.pages.onliner.product_page import ProductPage

iphone_text = 'Смартфон Apple iPhone 12 64GB (черный)'

def test_iphone(driver):
    driver.get('https://www.onliner.by/')
    home_page = HomePage(driver)
    home_page.send_text_in_input('iphone12')
    home_page.switch_to_flea_market()
    home_page.search_iphone()
    home_page.switch_to_current_window()
    product_page = ProductPage(driver)
    headline_text = product_page.get_headline_text()
    assert iphone_text in headline_text




