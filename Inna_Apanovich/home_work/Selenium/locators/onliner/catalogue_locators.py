from selenium.webdriver.common.by import By


AUTO_AND_MOTO = (By.XPATH, '//li[@class="catalog-navigation-classifier__item "][6]')

AUTO_ACOUSTICS = (By.XPATH, '//div[contains(text(), "Автоакустика")]')

AUTO_ACOUSTICS_CABLES = (By.XPATH, '//a[@href="https://catalog.onliner.by/cable?cable_usage%5B0%5D=auto&cable_usage%5Boperation%5D=union"]')

AUTO_ACOUSTICS_HEADLINE = (By.XPATH, '//h1[@class="schema-header__title"]')

FIRST_CABLE = (By.XPATH, '//span[@data-bind="html: product.extended_name || product.full_name"]')

ITEM_CART = (By.XPATH, '//a[@class="button-style button-style_expletive button-style_base-alter product-aside__item-button"]')

ORDER_PROCESSING = (By.XPATH, '//a[@class="button-style button-style_base-alter product-aside__item-button button-style_primary"]')
