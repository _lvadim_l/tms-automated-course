from selenium.webdriver.common.by import By

MAIN_INPUT = (By.XPATH, '//input[@class="fast-search__input"]')

IFRAME = (By.XPATH, '//iframe[@class="modal-iframe"]')

FLEA_MARKET = (By.XPATH, '//div[contains(text(), "на барахолке")]')

IPHONE_ELEM = (By.XPATH, '//img[@src="//content2.onliner.by/catalog/device/header/bf14a99b6b00fa25711a3e8e7a87d23a.jpeg"]')

CATALOGUE = (By.XPATH, '//span[@class="b-main-navigation__text"][1]')

