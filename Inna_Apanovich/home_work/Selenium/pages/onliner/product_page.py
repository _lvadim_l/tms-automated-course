from Inna_Apanovich.home_work.Selenium.base_page import BasePage
from Inna_Apanovich.home_work.Selenium.locators.onliner import product_page_locators
import time


class ProductPage(BasePage):
    def get_headline_text(self):
        headline = self.find_element(product_page_locators.HEADLINE)
        time.sleep(0.5)
        return headline.text

