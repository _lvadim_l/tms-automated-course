from Inna_Apanovich.home_work.Selenium.base_page import BasePage
from Inna_Apanovich.home_work.Selenium.locators.onliner import home_page_locators
import time

class HomePage(BasePage):

    def send_text_in_input(self, text):
        self.find_element(home_page_locators.MAIN_INPUT).send_keys(text)

    def switch_to_flea_market(self):
        self.switch_to_frame(home_page_locators.IFRAME)
        self.find_element(home_page_locators.FLEA_MARKET).click()

    def search_iphone(self):
        self.find_element(home_page_locators.IPHONE_ELEM).click()

    def switch_to_catalogue(self):
        self.find_element(home_page_locators.CATALOGUE).click()











