from Inna_Apanovich.home_work.Selenium.base_page import BasePage
from Inna_Apanovich.home_work.Selenium.locators.onliner import catalogue_locators
import time

class CataloguePage(BasePage):

    def switch_to_auto_and_moto_section(self):
        self.find_element(catalogue_locators.AUTO_AND_MOTO).click()

    def switch_to_auto_acoustics_subsection(self):
        self.find_element(catalogue_locators.AUTO_ACOUSTICS).click()

    def search_auto_acoustics_cables(self):
        self.find_element(catalogue_locators.AUTO_ACOUSTICS_CABLES).click()

    def get_auto_acoustics_headline_text(self):
        headline = self.find_element(catalogue_locators.AUTO_ACOUSTICS_HEADLINE).text
        time.sleep(0.5)
        return headline

    def click_to_the_first_item_in_catalogue(self):
        self.find_element(catalogue_locators.FIRST_CABLE).click()

    def click_on_cart_button(self):
        self.find_element(catalogue_locators.ITEM_CART).click()

    def switch_to_cart(self):
        self.find_element(catalogue_locators.ORDER_PROCESSING).click()

