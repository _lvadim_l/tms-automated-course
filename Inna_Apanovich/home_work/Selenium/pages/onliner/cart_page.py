from Inna_Apanovich.home_work.Selenium.base_page import BasePage
from Inna_Apanovich.home_work.Selenium.locators.onliner import cart_page_locators
import time

class CartPage(BasePage):

    def get_card_text(self):
        elem = self.find_element(cart_page_locators.PRODUCT_CARD)
        time.sleep(0.5)
        return elem.text

    def delete_product_from_cart(self):
        bin = self.find_element(cart_page_locators.DELETE_BUTTON)
        self.move_to_element(bin)
        self.find_element(cart_page_locators.DELETE_BUTTON).click()

    def check_absence_of_element(self):
        return self.check_absence_of_element(cart_page_locators.ORDER_PROCESSING)


