# Библиотека
# Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом, зарезервирована ли книги или нет.
# Создайте класс пользователь который может брать книгу, возвращать, бронировать.
# Если другой пользователь хочет взять зарезервированную книгу(или которую уже кто-то читает - надо ему про это сказать).
stock = {}

class Book:

    def __init__(self, title: str, author: str, pages: int, isbn: str, reservation: bool = False):
        self.title = title
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.reservation = reservation

        stock[self.title] = {
            'title': self.title,
            'author': self.author,
            'num_pages': self.pages,
            'ISBN': self.isbn,
            'reserved': self.reservation
            }

class Reader:

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname
        self.borrowed_books = []
        self.reserved_books = []

    def borrow(self, title):
        if self.check_reservation(title) is False:
            stock[title]['reserved'] = True
            self.borrowed_books.append(stock[title])
            print(f'You have borrowed {self.borrowed_books}')

    def take_back(self, title):
        stock[title]['reserved'] = False
        for i in self.borrowed_books:
            if i in self.borrowed_books:
                self.borrowed_books.remove(i)
                print(f'You have borrowed {self.borrowed_books}')

    def reserve(self, title):
        if self.check_reservation(title) is False:
            stock[title]['reserved'] = True
            self.reserved_books.append(stock[title])
            print(f'You have reserved {self.reserved_books}')

    @staticmethod
    def check_reservation(title):
        in_stock = stock.get(title)
        if in_stock:
            print('The book is in stock')
            if stock[title]['reserved'] is False:
                print('You can borrow the book')
                return False
            else:
                print('The book is reserved')
            return True
        else:
            print('The book is unavailable at the moment')


b_1 = Book('American Gods', 'Neil Gaiman', 465, '0-380-97365-0')
b_2 = Book('Norse Mythology', 'Neil Gaiman', 304, '1-526-63482-1')
b_3 = Book('Neverwhere', 'Neil Gaiman', 464, '0-7553-2280-0')
b_4 = Book('Good Omens', 'Neil Gaiman', 416, '978-1473227835')

r_1 = Reader('Inna', 'Apanovich')
r_2 = Reader('Sveta', 'Apanovich')

print(stock)

r_1.borrow('American Gods')
r_2.reserve('Norse Mythology')
r_1.take_back('American Gods')
r_2.borrow('Neverwhere')

print(stock)





