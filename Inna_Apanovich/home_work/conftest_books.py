import pytest
from task_books import Book, Reader


@pytest.fixture
def create_book():
    test_book = Book('American Gods', 'Neil Gaiman', 465, '0-380-97365-0')
    yield test_book


@pytest.fixture
def create_reader():
    test_reader = Reader('Inna', 'Apanovich')
    yield test_reader