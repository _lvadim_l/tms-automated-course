class WinDoor:
    def __init__(self, length, height):
        self.square = length * height 

class Room:
	def __init__(self, length, height, width):  
		self.__length = length
		self.__height = height
		self.__width = width
		self.wd = []  
	def get_length(self):
		return self.__length
	def set_length(self, l):
		self.__length = l  
	def get_height(self):
		return self.__height
	def set_height(self, h):
		self.__height = h    
	def get_width(self):
		return self.__width
	def set_width(self, w):
		self.__width = w  
	def full_area(self):
		self.full_area = 2 * self.__width * (self.__length + self.__height)
		return self.full_area  
	def addWD(self, length, height):  
		self.wd.append(WinDoor(length, height))
	def work_surface(self):  
		self.work_area = self.full_area  
		for i in self.wd:
			self.work_area -= i.square
		return self.work_area
	def roll_num(self, length, width):  
		self.roll_size = length * width
		self.num_rolls = round(self.work_area / self.roll_size)  
		return self.num_rolls

print('How big is your room?')
r1 = Room(float(input('length: ')), float(input('height: ')), float(input('width: ')))
print(f'The full suface of your walls is {r1.full_area()}')
print('Does it have any windows?')
answ_win = input('Y / N ')
while answ_win == 'Y':
	print('How big is your window?')
	r1.addWD(float(input('length: ')), float(input('height: ')))
	print('Any more?')
	answ_win = input('Y / N ')
while answ_win == 'N':
    print('How big is your door?')
    r1.addWD(float(input('length: ')), float(input('height: ')))
    print(f'You need to cover {r1.work_surface()} square metres of wall')
    print('How big are your wallpaper rolls?')
    roll = r1.roll_num(float(input('length: ')), float(input('width: ')))
    print(f'You need {roll} rolls')
    break
else: 
	print('Try again!')