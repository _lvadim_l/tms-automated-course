import pytest
from task_books import Book, Reader


class Test_Book:

    def test_book_initiation(self, create_book):
        assert type(create_book) == Book

    def test_book_title(self, create_book):
       assert create_book.title == 'American Gods'

class Test_Reader:

    def test_reader_initiation(self, create_reader):
        assert type(create_reader) == Reader

    def test_borrow(self, create_reader, create_book):
        create_reader.borrow(create_book.title)
        assert create_reader.borrowed_books is not None

    def test_take_back(self, create_reader, create_book):
        create_reader.borrow(create_book.title)
        create_reader.take_back(create_book.title)
        assert create_book.title not in create_reader.borrowed_books

    def test_reserve(self, create_reader, create_book):
        create_reader.reserve(create_book.title)
        assert create_book.reservation is True