from Inna_Apanovich.class_work_selenium.pages.onliner.home_page import HomePage
from Inna_Apanovich.class_work_selenium.pages.onliner.product_page import ProductPage
from Inna_Apanovich.class_work_selenium.pages.onliner.cart_page import CartPage

"""
1. Открыть https://www.onliner.by/
2. Ввести в поле поиска текст "iphone11"
3. Переключиться на вкладку "На барахолке"
4. Перейти по любому элементу в результате выдачи
5. Проверить header
6. Добавить в корзину
7. Перейти в корзину
8. Убедиться, что товар в корзине
"""

iphone_text = 'Apple Iphone'

def test_input(driver):
    driver.get('https://www.onliner.by/')
    home_page = HomePage(driver)
    home_page.send_text_in_input('iphone11')
    home_page.switch_to_flea_market()
    elements = home_page.get_search_results_from_catalogue()
    home_page.click_on_element_by_index(web_elem_list=elements, index=0)
    home_page.switch_to_current_window()
    product_page = ProductPage(driver)
    headline_text = product_page.get_headline_text()
    assert iphone_text in headline_text
    product_page.click_on_cart_button()
    cart_page = CartPage(driver)
    card_text = cart_page.get_card_text
    print(card_text)
    assert iphone_text in card_text
    cart_page.delete_product_from_cart()
    assert cart_page.check_absence_of_element() is None


