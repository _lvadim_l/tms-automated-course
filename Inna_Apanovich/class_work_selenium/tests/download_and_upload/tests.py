from Inna_Apanovich.class_work_selenium.pages.download_and_upload.home_page import Files
import time


file_name = 'ANTHONY EWOMA - CV.pdf'
file_name_1 = 'myFile.xlsx'
file_path = '/home/inna/tms-automated-course/README.md'
file_path_1 = '/home/inna/selenium/downloads/myFile.xlsx'

header_text = 'File Uploaded'

def test_download(driver):
    driver.get("http://the-internet.herokuapp.com/download")
    files = Files(driver)
    files.click_on_file(file_name)
    time.sleep(3)

def test_upload(driver):
    driver.get("http://the-internet.herokuapp.com/upload")
    files = Files(driver)
    files.upload_file(file_path)
    files.click_on_submit_button()
    time.sleep(1)
    header = files.get_header_text()
    assert header_text in header
    breakpoint()

def download_and_upload(driver):
    driver.get("http://the-internet.herokuapp.com/upload")
    files = Files(driver)
    files.click_on_file(file_name)
    time.sleep(3)
    files.upload_file(file_path)
    files.click_on_submit_button()
    header = files.get_header_text()
    assert header_text in header
    breakpoint()