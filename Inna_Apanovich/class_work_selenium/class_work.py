


# def test_cookie(driver):
#     driver.get('https://teachmeskills.by/')
#     cookies = driver.get_cookies()
#     print(cookies)
#
# def test(driver):
#    driver.get("https://teachmeskills.by/")
#    url_1 = driver.current_url
#    print(url_1)
#    print('---------------------------------')
#    title_1 = driver.title
#    print(title_1)
#    print('---------------------------------')
#    print(driver.current_window_handle)
#    driver.execute_script("window.open();")
#    driver.switch_to.window(driver.window_handles[1])
#    driver.get("https://www.onliner.by/")
#    url_2 = driver.current_url
#    print(url_2)
#    print('---------------------------------')
#    title_2 = driver.title
#    print(title_2)
#    print('---------------------------------')
#    driver.switch_to.window(driver.window_handles[0])


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def generate_alert_button(driver):
   driver.get("https://www.testandquiz.com/selenium/testing.html")
   generate_alert_button = driver.find_element(By.XPATH, "//button[contains(text(),'Generate Alert Box')]")
   generate_alert_button.click()
   alert = driver.switch_to.alert
   alert.accept()

def generate_confirm_box(driver):
   driver.get("https://www.testandquiz.com/selenium/testing.html")
   generate_confirm_box = driver.find_element(By.XPATH, "//button[contains(text(),'Generate Confirm Box')]")
   generate_confirm_box.click()
   alert = driver.switch_to.alert
   alert.accept()
   success_text = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//p[@id=demo]"))).click()
   assert success_text.text == "You pressed OK!"







