from Inna_Apanovich.class_work_selenium.base_page import BasePage
from Inna_Apanovich.class_work_selenium.locators.download_and_upload import home_page_locators


class Files(BasePage):

    def click_on_file(self, file_name):
        self.find_element(home_page_locators.file(file_name)).click()

    def upload_file(self, file_path):
        self.find_element(home_page_locators.FILE_UPLOAD).send_keys(file_path)

    def click_on_submit_button(self):
        self.find_element(home_page_locators.UPLOAD_BUTTON).click()

    def get_header_text(self):
        self.find_element(home_page_locators.HEADER).text()
