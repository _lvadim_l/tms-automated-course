from Inna_Apanovich.class_work_selenium.base_page import BasePage
from typing import List
import time
from Inna_Apanovich.class_work_selenium.locators.onliner import product_locators

class ProductPage(BasePage):
    def get_headline_text(self) -> str:
        element = self.find_element(product_locators.HEADLINE_IN_CATALOGUE)
        time.sleep(0.5)  #TODO refactor sleeps in tests
        return element

    def click_on_cart_button(self):
        self.find_element(product_locators.ADD_TO_CART).click()

    def click_on_active_cart_button(self):
        self.find_element(product_locators.IN_CART_ACTIVE_BUTTON).click()