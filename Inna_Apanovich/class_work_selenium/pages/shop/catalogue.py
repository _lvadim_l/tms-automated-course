import time

from Inna_Apanovich.class_work_selenium.base_page import BasePage
from Inna_Apanovich.class_work_selenium.locators.shop import catalogue_locators

class CataloguePage(BasePage):

    def click_show_more(self, index):
        locator = catalogue_locators.show_more(index)
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()
        time.sleep(1)


    def click_on_check_box_laptops(self, laptop_name):
        locator = catalogue_locators.check_box_item(laptop_name)
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        self.click_with_javascript(elem)


    def fill_price_before(self, price):
        locator = catalogue_locators.PRICE_BEFORE
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.send_keys(price)

    def fill_price_after(self, price):
        locator = catalogue_locators.PRICE_AFTER
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.send_keys(price)

    def open_diagonal_menu(self):
        locator = catalogue_locators.DIAGONAL
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        time.sleep(1)
        elem.click()

    def show_more_diagonals(self):
        locator = catalogue_locators.DIAGONAL_SHOW_MORE
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(elem, locator)
        elem.click()
        # time.sleep(1)
        # self.click_with_javascript(elem)

    def click_on_checkbox_item(self, item_name):
        locator = catalogue_locators.check_box_item(item_name)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        time.sleep(1)
        self.click_with_javascript(elem)

    def click_show_seach_results(self):
        time.sleep(2)
        self.find_element(catalogue_locators.SEARCH_RESULTS).click()

    def get_all_elements(self):
        return self.find_elements(catalogue_locators.ELEMENTS_ON_PAGE)

    def click_on_sorting_button(self):
        locator = catalogue_locators.SORTING_BUTTON
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        time.sleep(1)
        elem.click()

    def ascending_sorting(self):
        locator = catalogue_locators.ASCENDING_SORTING
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        time.sleep(1)
        elem.click()

    def get_first_elem_name(self):
        elems = self.find_elements(catalogue_locators.ELEMENTS_ON_PAGE)
        time.sleep(1)
        return elems[0].text

    def descending_sorting(self):
        locator = catalogue_locators.DESCENDING_SORTING
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        time.sleep(1)
        elem.click()

    def move_to_the_last_page(self):
        locator = catalogue_locators.LAST_PAGE
        elem = self.find_element(locator)
        time.sleep(1)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        time.sleep(1)
        elem.click()

    def get_last_elem_name(self):
        elems = self.find_elements(catalogue_locators.ELEMENTS_ON_PAGE)
        time.sleep(1)
        return elems[-1].text

