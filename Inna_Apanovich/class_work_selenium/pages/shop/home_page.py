from Inna_Apanovich.class_work_selenium.base_page import BasePage
from Inna_Apanovich.class_work_selenium.locators.shop import home_page_locators
import time

class HomePage(BasePage):

    def move_to_catalogue(self):
        elem = self.find_element(home_page_locators.CATALOGUE)
        self.move_to_element(elem)

    def move_to_section_computers(self):
        elem = self.find_element(home_page_locators.COMPUTERS)
        time.sleep(0.5)
        self.move_to_element(elem)

    def move_to_section_laptops_and_accessories(self):
        elem = self.find_element(home_page_locators.LAPTOPS_AND_ACCESSORIES)
        time.sleep(0.5)
        self.move_to_element(elem)

    def move_to_section_laptops(self):
        elem = self.find_element(home_page_locators.LAPTOPS)
        time.sleep(0.5)
        self.move_to_element(elem)

    def click_on_laptops(self):
        elem = self.find_element(home_page_locators.LAPTOPS)
        time.sleep(0.5)
        self.click_with_javascript(elem)