from selenium.webdriver.common.by import By

def show_more(index):
    return (By.XPATH, f'//span[@class="ModelFilter__OpenHideAttrTxt Page__DarkDotLink"][{index}]')

def check_box_item(name):
    return (By.XPATH, f'//label[@data-ga="{name}"]')

PRICE_BEFORE = (By.XPATH, f'//input[@name="price_before"]')

PRICE_AFTER = (By.XPATH, f'//input[@name="price_after"]')

DIAGONAL = (By.XPATH, '//span[contains(text(), "Диагональ экрана")]')

DIAGONAL_SHOW_MORE = (By.XPATH, '//span[@data-idgroup="prof_5828"][1]')

SEARCH_RESULTS = (By.XPATH, '//span[@data-ga="podbor"]')

ELEMENTS_ON_PAGE = (By.XPATH, '//div[@class="ModelList__ModelContentLine"]')

SORTING_BUTTON = (By.XPATH, '//span[@class="chzn-txt-sel"]')

ASCENDING_SORTING = (By.XPATH, '//li[contains(text(), "С дешевых")]')

DESCENDING_SORTING = (By.XPATH, '//li[contains(text(), "С дорогих")]')

LAST_PAGE = (By.XPATH, '//a[@class="Paging__PageLink hidden-xs"][4]')
