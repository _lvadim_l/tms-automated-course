from selenium.webdriver.common.by import By

CATALOGUE = (By.XPATH, '//span[@class="Header__ButtonCatalog Header__CatalogDescShow visible-lg"]')

# def locator_in_menu(section_name):
    # return (By.XPATH, f'//span[contains(text()), {section_name}]')

COMPUTERS = (By.XPATH, '//a[@href="/kompyutery/"]')

LAPTOPS_AND_ACCESSORIES = (By.XPATH, '//a[@href="/kompyutery/noutbuki_i_aksessuari/"]')

LAPTOPS = (By.XPATH, '//span[@data-link="/noutbuki/"][1]')