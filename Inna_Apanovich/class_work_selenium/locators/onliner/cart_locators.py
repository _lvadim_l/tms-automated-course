from selenium.webdriver.common.by import By

PRODUCT_CARD = (By.XPATH, '//div[@class="cart-form__offers-unit cart-form__offers-unit_primary"]')

DELETE_BUTTON = (By.XPATH, '//a[@class="button-style button-style_auxiliary button-style_small cart-form__button '
                           'cart-form__button_remove"]')

ORDER_PROCESSING = (By.XPATH, '//div[@class="cart-form__offers-item cart-form__offers-item_additional"]')