from selenium.webdriver.common.by import By

def file(file_name):
    return (By.XPATH, f'//a[contains(text(), "{file_name}")]')

FILE_UPLOAD = (By.XPATH, '//input[@id="file-upload"]')

UPLOAD_BUTTON = (By.XPATH, '//input[@id="file-submit"]')

HEADER = (By.XPATH, '//h3')