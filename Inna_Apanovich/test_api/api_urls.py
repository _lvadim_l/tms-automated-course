BASE_URL = 'https://petstore.swagger.io/v2'

# USERS
USER = BASE_URL + '/user'

USER_NAME = BASE_URL + '/user/{username}'

LOGIN = BASE_URL + '/user/login'

LOGOUT = BASE_URL + '/user/logout'

PET = BASE_URL + '/pet'

PET_ID = BASE_URL + '/pet/{petId}'

ORDER = BASE_URL + '/store/order'

ORDER_ID = BASE_URL + '/store/order/{orderId}'

