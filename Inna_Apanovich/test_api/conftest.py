import pytest
from random import randint
from Inna_Apanovich.test_api import randomaizer
from Inna_Apanovich.test_api.headers import HEADERS
from Inna_Apanovich.test_api import api_urls
from json import dumps
import requests

@pytest.fixture()
def user_data():
    random_data = randomaizer.RandomData()
    random_name = random_data.generate_word(6)
    random_word = random_data.generate_word(6)
    data = {
              "id": randint(1, 100),
              "username": random_name,
              "firstName": random_word,
              "lastName": random_word,
              "email": "string@sad.com",
              "password": "123456789",
              "phone": "9874563",
              "userStatus": randint(1, 100)
    }
    return data

@pytest.fixture()
def user(user_data):
    create_user_response = requests.post(
        url=api_urls.USER, headers=HEADERS, data=dumps(user_data)
    )
    assert create_user_response.status_code == 200
    yield user_data
    delete_user_response = requests.get(
        url=api_urls.USER_NAME.format(username=user_data['username']),
        headers=HEADERS
    )
    assert delete_user_response.status_code == 200

@pytest.fixture()
def pet_data():
    random_data = randomaizer.RandomData()
    random_name = random_data.generate_word(5)
    random_word = random_data.generate_word(7)
    data = {
              "id": randint(1, 100),
              "category": {
                "id": randint(1, 100),
                "name": random_name
              },
              "name": "doggie",
              "photoUrls": [
                random_word
              ],
              "tags": [
                {
                  "id": randint(1, 100),
                  "name": random_name
                }
              ],
              "status": "available"
            }
    return data

@pytest.fixture()
def order_data():
    data = {
            "id": randint(1, 100),
            "petId": randint(1, 100),
            "quantity": randint(1, 100),
            "shipDate": "2021-06-09T14:41:22.016Z",
            "status": "placed",
            "complete": True
            }
    return data