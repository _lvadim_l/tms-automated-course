import requests
from json import loads, dumps
from Inna_Apanovich.test_api.headers import HEADERS
from Inna_Apanovich.test_api import api_urls

def test_user_crud(user_data):
    user_response = requests.post(
        url=api_urls.USER, headers=HEADERS, data=dumps(user_data)
    )
    assert user_response.status_code == 200

    get_user_response = requests.get(
        url=api_urls.USER_NAME.format(username=user_data['username']),
        headers=HEADERS
    )
    assert get_user_response.status_code == 200
    assert get_user_response.json()['id'] == user_data['id']

    new_data = {"email": "update@sad.com"}
    updated_user_data = {**user_data, **new_data}
    updated_response = requests.put(
        url=api_urls.USER_NAME.format(username=user_data['username']),
        headers=HEADERS,
        data=dumps(updated_user_data)
    )
    assert updated_response.status_code == 200

    get_updated_user = requests.get(
        url=api_urls.USER_NAME.format(username=user_data['username']),
        headers=HEADERS
    )
    assert get_updated_user.status_code == 200
    assert get_updated_user.json()['email'] == new_data['email']

    delete_user_response = requests.delete(
        url=api_urls.USER_NAME.format(username=user_data['username']),
        headers=HEADERS
    )
    assert delete_user_response.status_code == 200
    get_deleted_user = requests.get(
        url=api_urls.USER_NAME.format(username=user_data['username']),
        headers=HEADERS
    )
    assert get_deleted_user.status_code == 404

def test_auth(user):
    login_response = requests.get(
        url=api_urls.LOGIN,
        headers=HEADERS,
        params={'username': user['username'], 'password': user['password']}
    )
    assert login_response.status_code == 200
    assert 'logged in user session' in login_response.json()['message']
    logout_response = requests.get(
        url=api_urls.LOGOUT,
        headers=HEADERS,
        params={'username': user['username'], 'password': user['password']}
    )
    assert logout_response.status_code == 200
    assert 'ok' in logout_response.json()['message']


def test_pet_crud(pet_data):
    pet_response = requests.post(
        url=api_urls.PET, headers=HEADERS, data=dumps(pet_data)
    )
    assert pet_response.status_code == 200
    get_pet_response = requests.get(
        url=api_urls.PET_ID.format(petId=pet_data['id']),
        headers=HEADERS
    )
    assert get_pet_response.status_code == 200
    assert get_pet_response.json()['id'] == pet_data['id']

    new_pet_data = {"name": "rattie"}
    updated_pet_data = {**pet_data, **new_pet_data}
    updated_response = requests.put(
        url=api_urls.PET,
        headers=HEADERS,
        data=dumps(updated_pet_data)
    )
    assert updated_response.status_code == 200

    get_updated_pet = requests.get(
        url=api_urls.PET_ID.format(petId=pet_data['id']),
        headers=HEADERS
    )
    print(get_updated_pet.status_code)
    print(get_updated_pet.text)
    assert get_updated_pet.status_code == 200
    assert get_updated_pet.json()['name'] == new_pet_data['name']

    delete_pet_response = requests.delete(
        url=api_urls.PET_ID.format(petId=pet_data['id']),
        headers=HEADERS
    )
    assert delete_pet_response.status_code == 200

    get_deleted_pet = requests.get(
        url=api_urls.PET_ID.format(petId=pet_data['id']),
        headers=HEADERS
    )
    assert get_deleted_pet.status_code == 404

def test_store_crud(order_data):
    order_response = requests.post(
        url=api_urls.ORDER, headers=HEADERS, data=dumps(order_data)
    )
    assert order_response.status_code == 200

    get_order_response = requests.get(
        url=api_urls.ORDER_ID.format(orderId=order_data['id']),
        headers=HEADERS
    )
    assert get_order_response.status_code == 200
    assert get_order_response.json()['id'] == order_data['id']

    delete_order_response = requests.delete(
        url=api_urls.ORDER_ID.format(orderId=order_data['id']),
        headers=HEADERS
    )
    assert delete_order_response.status_code == 200
    get_deleted_order = requests.get(
        url=api_urls.ORDER_ID.format(orderId=order_data['id']),
        headers=HEADERS
    )
    assert get_deleted_order.status_code == 404



