# Есть архив с книгами, необходимо сделать программу помощник читателя, прошу использовать ООП.
# Программа должна принять от пользователя слово или некоторое
# количество слов, в ответ программа должна выдать все названия книг в тексте которых есть все
# введенные пользователем слова.
import os


class Books:
    def __init__(self):
        self.books = []

    def add_book(self, book: str):
        self.books.append(book)

    def __str__(self):
        rep = ''
        for book in self.books:
            rep += str(book)+'\t'
        return rep


class Reader_helper:
    @staticmethod
    def find_the_book(library: Books, list: list):
        for book in library.books:
            with open(f"{book}", "r", errors="ignore") as text:
                text = text.read()
                for i in list:
                    if i.lower() not in text.lower():
                        break
                else:
                    print(str(book), end=' ')
    @staticmethod
    def input_keywards() -> list:
        keywards = []
        while True:
            a = input('enter keywards, if it enough just press Enter\n')
            if a == '':
                break
            keywards.append(a)
            print('\n', keywards, end='\n\n')
        return keywards

lib = Books()
for book in os.listdir():
    if '.txt' in book:
        lib.add_book(book)
Reader_helper.find_the_book(lib, Reader_helper.input_keywards())
input('')
# Reader_helper.find_the_book(lib)