# задача про вёдра
import time
qwerty = time.time()
v_5L = 0
v_3L = 0
n = 0
v_3L_default = 3
v_5L_default = 5
while v_5L != 4:
    print(f'в 5л ведре {v_5L} литров воды, в 3л ведре {v_3L} воды\n')
    q = input("Выберите действие \
                            \n1 - наполнить 5л ведро\
                            \n2 - перелить из 5л ведра воду в 3л ведро\
                            \n3 - вылить из 3х литрового ведра всю воду\
                            \n4 - перелить воду из 3л ведра в 5л ведро\
                            \n5 - наполнить 3л ведро водой\
                            \n6 - вылить воду из 5л ведра\n\n")
    n += 1
    if q == '1':
        v_5L = 5
    elif q == '2':
        if v_3L + v_5L > 3:
            v_5L -= (v_3L_default - v_3L)
            v_3L = 3
        else:
            v_3L += v_5L
            v_5L -= v_5L
    elif q == '3':
        v_3L -= v_3L
    elif q == '4':
        if v_5L + v_3L > 5:
            v_3L -= (v_5L_default - v_5L)
            v_5L = 5
        else:
            v_5L += v_3L
            v_3L -= v_3L
    elif q == '5':
        v_3L = 3
    elif q == '6':
        v_5L -= v_5L
    else:
        print("нет такой команды\n")

else:
    print(f'в нашем 5л ведре теперь осталось {v_5L} литра воды, за {n} действий\n')
print(time.time()-qwerty)
input('\n\nнажмите Enter чтобы выйти')

