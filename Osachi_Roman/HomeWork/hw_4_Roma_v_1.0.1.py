
numbers = {
    1 : 'one', 2 : 'two', 3: 'tree', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
    20: 'twenty', 30: 'thirty', 40: 'forty', 50: 'fifty', 60: 'sixty', 70: 'seventy', 80: 'eighty', 90: 'ninety',
    100: 'one hundred'
}

def hundred():
    print(numbers.get(100), end=' ')

def dozens(y):
    a = int(y + '0')
    if numbers.get(a) is None:
        print("",end='')
    else:
        print(numbers.get(a), end='-')

def units(z):
    z = int(z)
    if numbers.get(z) is None:
        print(f"\b",end='')
    else:
        print(numbers.get(z))

def main():
    try:
        num = input('enter the number')
        o = int(num)
        try:
            q = num[-3]
            if num[:-3]:
                print('number is not valid')
                main()
                import sys
                sys.exit(0)
            hundred()
        except IndexError:
            print('',end='')

        q = num[-2]
        dozens(q)
        q = num[-1]
        units(q)
    except ValueError:
        print('Это не число')
        main()
cycle = True
while cycle:
    main()

    a = input('\n\ntry again? y/n')
    if a == 'n':
        cycle = False
