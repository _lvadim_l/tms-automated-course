from class_work_selenium.pages.Onliner.home_page import HomePage
from class_work_selenium.pages.Onliner.catalog_page import Catalog_page


def test_home_page(driver):
    driver.get('https://www.onliner.by/')
    home_page = HomePage(driver)
    home_page.switch_to_catalog()

def test_catalog_page(driver):
    catalog_page = Catalog_page(driver)
    catalog_page.click_on_computers_and_network()
    catalog_page.move_to_computers_laptops_monitors()
    catalog_page.click_on_laptops()
    catalog_page.click_on_checkbox_item("HP")
    catalog_page.click_on_checkbox_item("Lenovo")
    catalog_page.click_on_checkbox_item("Dell")
    catalog_page.chose_scope_values("от", 1500)
    catalog_page.chose_scope_values("до", 3000)
    catalog_page.click_on_diagonal_choser('min')
    catalog_page.choose_diagonal(max_or_min='min', diagonal=12.1)
    catalog_page.click_on_diagonal_choser('max')
    catalog_page.choose_diagonal("max", 13.4)
    catalog_page.click_on_sorting_button()
    catalog_page.choose_sorting_from("cheap")
    first_product_on_page = catalog_page.get_name_of_first_product()
    catalog_page.click_on_sorting_button()
    catalog_page.choose_sorting_from("expensive")
    catalog_page.move_to_last_page()
    last_product_on_page = catalog_page.get_name_fo_the_last_product()
    assert first_product_on_page == last_product_on_page
