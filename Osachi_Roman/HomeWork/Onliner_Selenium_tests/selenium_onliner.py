from selenium.webdriver.common.by import By
from selenium import webdriver
from time import sleep

def test_baraholka(driver):
    driver.get('https://www.onliner.by/')
    sleep(0.5)
    elem = driver.find_element(By.XPATH, '//input[@class="fast-search__input"]')

    elem.send_keys("iphone12")
    sleep(0.5)

    iframe = driver.find_element(By.XPATH, '//iframe[@class="modal-iframe"]')

    driver.switch_to.frame(iframe)

    baraholka_elem = driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[2]/div/div/div[3]')
    baraholka_elem.click()
    sleep(2)
    iphone = driver.find_element(By.XPATH, '//a/img[@src="//content2.onliner.by/catalog/''device/header/bf14a99b6b00fa25711a3e8e7a87d23a.jpeg"]')
    iphone.click()
    sleep(2)
    driver.switch_to.window(driver.window_handles[0])
    sleep(0.5)
    driver.find_element_by_css_selector('a.offers-description-filter-control_switcher:nth-child(3)\
     > span:nth-child(2)').click()
    driver.switch_to.window(driver.window_handles[0])
    iphone = driver.find_element_by_css_selector('.catalog-masthead__title')
    sleep(0.5)
    assert iphone.text == "Смартфон Apple iPhone 12 256GB (черный)"
    price = driver.find_element_by_css_selector("a.offers-description__link:nth-child(1)")
    assert price.text == "2945,00 р."
    sleep(7)
