from selenium.webdriver.common.by import By
from class_work_selenium.base_pagee import BasePage
from time import sleep
from class_work_selenium.locators.onliner import home_page_locators, Iphone_Page_Locators
from class_work_selenium.pages.Onliner.home_page import HomePage



def test_baraholka(driver):
    driver.get("https://www.onliner.by/")
    home_page = HomePage(driver)
    home_page.send_text_input("iphone12")
    home_page.switch_to_flea_market()


    home_page.find_element(home_page_locators.FLEA_MARKET).click()

    iphone = home_page.find_element(home_page_locators.IPHONE)
    sleep(0.2)
    iphone.click()

    driver.switch_to.window(driver.window_handles[0])
    home_page.find_element(Iphone_Page_Locators.STORAGE256GB).click()

    assert home_page.find_element(Iphone_Page_Locators.IPHONE_TITLE).text == "Смартфон Apple iPhone 12 256GB (черный)"

    assert home_page.find_element(Iphone_Page_Locators.IPHONE_PRICE).text == "2943,00 р."
    sleep(7)