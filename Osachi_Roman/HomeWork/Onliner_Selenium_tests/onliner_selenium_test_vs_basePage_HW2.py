from class_work_selenium.pages.Onliner.home_page import HomePage
from class_work_selenium.pages.Onliner.catalog_page import Catalog_page
from class_work_selenium.pages.Onliner.cable_page import CablePage
from class_work_selenium.pages.Onliner.porduct_page import Product_page
from class_work_selenium.pages.Onliner.cart_page import CartPage
from time import sleep


def test_home_page(driver):
    driver.get('https://www.onliner.by/')
    home_page = HomePage(driver)
    home_page.switch_to_catalog()

def test_catalog_page(driver):
    catalog_page = Catalog_page(driver)
    catalog_page.click_to_auto_and_moto()
    catalog_page.move_to_car_aucustic_element()
    catalog_page.move_to_auto_cable_page()

def test_cable_page(driver):
    cable_page = CablePage(driver)
    assert cable_page.return_title_text() == "Кабели, адаптеры, разветвители"
    list_of_products = cable_page.return_list_of_products()
    cable_page.click_on_first_element(list_of_products)

def test_product_page(driver):
    product_page = Product_page(driver)
    global selected_item_name
    selected_item_name = product_page.get_headline_text()
    product_page.click_on_cart_button()
    product_page.click_on_activ_cart_button()

def test_cart_page(driver):
    cart_page = CartPage(driver)
    assert "1 товар" in cart_page.get_cart_page_description_text()
    assert selected_item_name.replace("Кабель ", "") in cart_page.get_card_text()
    sleep(3)






