BASE_URL = 'https://petstore.swagger.io/v2'

#Users
USER = BASE_URL + '/user'

USER_NAME = BASE_URL + "/user/{username}"

LOGIN = BASE_URL + "/user/login"

LOGOUT = BASE_URL + "/user/logout"

PET = BASE_URL + "/pet"

PET_ID = PET + "/{petID}"

PET_IMAGE_UPLOAD = PET + "/{petID}/uploadImage"

STORE_ORDER = BASE_URL + "/store/order"

