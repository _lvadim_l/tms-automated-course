import pytest
from random import randint

from faker import Faker
import requests
import api_urls
from headers import HEADERS
from json import dumps

fake = Faker()


@pytest.fixture()
def user_data():
    random_word = fake.name()
    random_name = fake.name()
    data = {
        "id": randint(1, 100),
        "username": random_name,
        "firstName": random_word,
        "lastName": random_word,
        "email": "string@sad.com",
        "password": "string",
        "phone": "string",
        "userStatus": 0
    }

    return data


@pytest.fixture()
def user(user_data):
    user_response = requests.post(
        url=api_urls.USER, headers=HEADERS, data=dumps(user_data)
    )
    assert user_response.status_code == 200
    yield user_data

    delete_user_response = requests.delete(url=api_urls.USER_NAME.format(username=user_data["username"]),
                                           headers=HEADERS)
    assert delete_user_response.status_code == 200


@pytest.fixture()
def pet():
    id = randint(100, 1000)
    name = fake.name()

    data = {
          "id": id,
          "category": {
            "id": id,
            "name": "string"
          },
          "name": name,
          "photoUrls": [
            "string"
          ],
          "tags": [
            {
              "id": id,
              "name": "string"
            }
          ],
          "status": "available"
        }
    return data

@pytest.fixture()
def store_order_for_pet(pet):
    id = randint(1, 9)
    petID = pet['id']
    data ={
            "id": id,
            "petId": petID,
            "quantity": 0,
            "shipDate": "2021-06-09T11:17:42.295Z",
            "status": "placed",
            "complete": True
}
    return data
