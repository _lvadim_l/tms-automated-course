import requests
import api_urls

from headers import HEADERS
from json import dumps


def test_user_crud(user_data):
    user_response = requests.post(
        url=api_urls.USER, headers=HEADERS, data=dumps(user_data)
    )
    print(user_response.status_code)
    print(user_response.text)
    assert user_response.status_code == 200
    get_user_response = requests.get(
        url=api_urls.USER_NAME.format(username=user_data["username"]),
        headers=HEADERS
    )
    assert get_user_response.status_code == 200
    print(get_user_response.text)
    assert get_user_response.json()["id"] == user_data["id"]
    update_data = {"email": "string@sad.com"}
    update_user_data = {**user_data, **update_data}
    updated_response = requests.put(url=api_urls.USER_NAME.format(username=user_data["username"])
                                    , headers=HEADERS,
                                    data=dumps(update_user_data))
    assert updated_response.status_code == 200
    get_updated_user = requests.get(url=api_urls.USER_NAME.format(username=user_data["username"]),
                                    headers=HEADERS)

    assert get_updated_user.status_code == 200
    assert get_updated_user.json()["email"] == "string@sad.com"
    delete_user_response = requests.delete(url=api_urls.USER_NAME.format(username=user_data["username"]),
                                           headers=HEADERS
                                           )
    assert delete_user_response.status_code == 200
    get_deleted_user = requests.get(url=api_urls.USER_NAME.format(username=user_data["username"]),
                                    headers=HEADERS)
    assert get_deleted_user.status_code == 404


def test_auth(user):
    login_response = requests.get(
        url=api_urls.LOGIN,
        headers=HEADERS,
        params={"username": user["username"], "password": user["password"]}
    )
    assert login_response.status_code == 200
    assert "logged in user session" in login_response.json()["message"]
    logout_response = requests.get(url=api_urls.LOGOUT,
                                   headers=HEADERS)
    assert logout_response.status_code == 200
    print(logout_response.text)
    assert logout_response.json()["message"] == 'ok'
    print(logout_response.headers)


def test_pet_creation(pet):
    create_pet_response = requests.post(
        url=api_urls.PET, headers=HEADERS, data=dumps(pet))
    assert create_pet_response.status_code == 200
    file_to_upload = requests.get(url='https://pp.userapi.com/c307200/v307200240/b867/CFhlyzcvLT0.jpg')
    with open("pet_image.jpg", "wb") as file:
        file.write(file_to_upload.content)
    image = open("pet_image.jpg", 'rb')
    pet_image = {'file': image}
    url_vs_ID = api_urls.PET_ID.format(
        petID=pet["id"])
    url_vs_ID_to_upload = api_urls.PET_ID.format(
        petID=pet["id"]) + "/uploadImage"
    upload_pet_image_response = requests.post(url=url_vs_ID_to_upload, files=pet_image)
    image.close()
    assert upload_pet_image_response.status_code == 200
    print(upload_pet_image_response.json())
    photos_url = {"photoUrls": ["https://pp.userapi.com/c307200/v307200240/b867/CFhlyzcvLT0.jpg"]}
    new_photos_url = {**pet, **photos_url}
    updated_pet_response = requests.put(url=api_urls.PET, headers=HEADERS,
                                        data=dumps(new_photos_url))
    assert updated_pet_response.status_code == 200
    assert updated_pet_response.json()['photoUrls'] == photos_url["photoUrls"]

    find_pet_response = requests.get(url=url_vs_ID, headers=HEADERS)
    print(find_pet_response.json(), '- find_pet_response')
    assert find_pet_response.status_code == 200
    delete_pet_response = requests.delete(url=url_vs_ID, headers=HEADERS)
    assert delete_pet_response.status_code == 200
    print(delete_pet_response.json())
    find_deleted_pet_response = requests.get(url=url_vs_ID, headers=HEADERS)
    assert find_deleted_pet_response.status_code == 404

def test_store(store_order_for_pet):
    plased_order_response = requests.post(url=api_urls.STORE_ORDER, headers=HEADERS, json=store_order_for_pet)
    assert plased_order_response.status_code == 200
    print(plased_order_response.json())
    order_id = plased_order_response.json()['id']
    print(order_id)
    finded_order_response = requests.get(url=api_urls.STORE_ORDER+f"/{order_id}")
    assert finded_order_response.status_code == 200
    print(finded_order_response.json())
    delete_order_response = requests.delete(url=api_urls.STORE_ORDER+f"/{order_id}")
    assert  delete_order_response.status_code == 200
    find_deleted_order_response = requests.get(url=api_urls.STORE_ORDER+f"/{order_id}")
    assert find_deleted_order_response.status_code == 404
