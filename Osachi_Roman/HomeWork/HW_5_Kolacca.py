from time import time
t = time()
# @functools.lru_cache(maxsize=128)
def main():
    a = {1}
    for i in range(1,1000000):
        n = 0
        while True:
            i = i * 0.5 if not i % 2  else i * 3 + 1
            n += 1
            if i == 1:
                break

        a.add(n)
    print(max(a))
    print(time()-t)
main()