# Создайте класс book с именем книги, автором, кол-м страниц,
# ISBN, флагом, зарезервирована ли книги или нет. Создайте класс пользователь который может
# брать книгу, возвращать, бронировать. Если другой пользователь хочет взять зарезервированную
# книгу
# (или которую уже кто-то читает - надо ему про это сказать).

class Book:
    def __init__(self,name:str,author:str,pages:int,ISBN:int, reserve=False):
        self.__name = name
        self.__author = author
        self.__pages = pages
        self.__ISBN = ISBN
        self._reserve = reserve
    def __str__(self):
        return f"name - {self.__name}, author  - {self.__author}, pages - {self.__pages},\
ISBN - {self.__ISBN}, reservation - {self._reserve}"
    @property
    def name(self):
        return self.__name

    def change_reservation(self,a:bool):
        self._reserve = a

class Library:
    def __init__(self):
        self.__books = []
    @property
    def book_list(self):
        return self.__books
    def add_book(self,book:Book):
        self.__books.append(book)
    def give_book(self,book:Book):
        self.__books.remove(book)
    def valid_books(self):
        if self.book_list == []:
            return "there is no books in the library"
        else:
            print('\nValid books')
            return self.__str__()
    def __str__(self):
        res=""
        for i in self.book_list:
            res+=str(i)+" \n"
        return res

class User(Library):
    def __init__(self,name):
        super(User, self).__init__()
        self.__name = name
    def take_book(self,book:Book,lib:Library):
        lib.give_book(book)
        self.book_list.append(book)

    def return_book(self,book:Book,lib:Library):
        if book._reserve == True:
            book.change_reservation(False)
        self.book_list.remove(book)
        lib.add_book(book)
    def reserve_book(self,book):
        book.change_reservation(True)




def main():
    lib = Library()
    Anuta = Book('Anuta','checkov',230,12341234)
    KapDaughter= Book('KapDaughter','pushkin',432,1241234435)
    LordOfTheRings = Book('LordOfTheRings', 'Tolkien', 1000, 13655345)
    testlib = {'anuta':Anuta, 'kapdaughter': KapDaughter, 'lordoftherings':LordOfTheRings}
    lib.add_book(Anuta)
    lib.add_book(KapDaughter)
    lib.add_book(LordOfTheRings)
    name = input('Your name?')
    user1 = User(name)
    while True:
        print(lib.valid_books())
        ans = input(f'''
            {name}
            choose your action
            1 - reserve book
            2 - take book
            3 - return book
            4  - information about book\n''')
        if ans == '1':
            ans2 = input('''
            what book you want to reserve??
            enter the name of the book\n''')
            try:
                user1.reserve_book(testlib[ans2.lower()])
            except :
                print('there is no book')
                continue
        elif ans == '2':
            ans3 = input('''
            what book you want to take??
            enter the name of the book\n''')
            try:
                user1.take_book(testlib[ans3.lower()],lib)
            except:
                print('there is no book')
                continue
        elif ans == '3':
            ans4 = input("""
            what book you want to take??
            enter the name of the book\n""")
            try:
                user1.return_book(testlib[ans4.lower()],lib)
            except:
                print('there is no book')
                continue
        elif ans == '4':
            ans5 = input("""
                    what book you want to know about?
                    enter the name of the book\n""")
            try:
                print(testlib[ans5.lower()])
            except:
                print('there is no such book\n')

if __name__ == '__main__':
    main()









