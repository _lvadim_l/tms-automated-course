from time import time
t = time()
# Задание: Числа Фибоначчи

def fibonachi(poryadkoviy_nomer:int, x1=1, x2=1):
    poryadkoviy_nomer -= 1
    x3 = x1 + x2
    if poryadkoviy_nomer == 2:
        return x3
    elif poryadkoviy_nomer in [0, 1]:
        return 1
    else:
        return fibonachi(poryadkoviy_nomer, x2, x3)
inp = int(input('enter index number of fibonachi number you want to know (3 - ထ)\n'))
print(fibonachi(inp))

# # то же самое циклом
# inp = int(input('\nenter index number of fibonachi number you want to know (3 - ထ)\n'))
# x1 = 1
# x2 = 1
# while inp != 2:
#     x3 = x1 + x2
#     x1 = x2
#     x2 = x3
#     inp -=1
# print(x3)
print(time()-t)
# input('')
