import unittest
import Library
from unittest.mock import patch


class Test_Book_class(unittest.TestCase):
    def setUp(self):
        self.test_objekt = Library.Book('LOR', 'Tolkien', 123, 12345435)

    def tearDown(self):
        del self.test_objekt

    def test_class_initiate(self):
        self.assertIsInstance(self.test_objekt, Library.Book)

        with self.assertRaises(TypeError):
            Library.Book(123,124,"123",'123',123,124)

    def test_str_method(self):
        self.assertEqual(str(self.test_objekt), 'name - LOR, author  - Tolkien, pages - 123,ISBN - 12345435,\
 reservation - False')
        self.assertNotEqual(str(self.test_objekt),'name - LOR@, author  - Tolkien@, pages - 123@, ISBN - 12345435@')

    def test_name_property(self):
        self.assertEqual(self.test_objekt.name, 'LOR')


    def test_change_reservation(self):
        self.test_objekt.change_reservation(True)
        self.assertEqual(self.test_objekt._reserve, True)
        self.assertNotEqual(self.test_objekt._reserve, False)


class Test_LibraryClass(unittest.TestCase):
    def setUp(self):
        self.test_objektBook = Library.Book('LOR', 'Tolkien', 123, 12345435)
        self.test_objektLib = Library.Library()

    def tearDown(self):
        del self.test_objektLib
        del self.test_objektBook

    def test_class_initiate(self):
        self.assertIsInstance(self.test_objektLib, Library.Library)
        with self.assertRaises(TypeError):
            self.test_objekt_Error_Lib = Library.Library(123, 123, "123")

    def test_property_book_list(self):
        self.assertEqual(self.test_objektLib.book_list, [])
        self.assertNotEqual(self.test_objektLib.book_list, None)

    def test_add_book(self):
        self.test_objektLib.add_book(self.test_objektBook)
        self.assertIs(self.test_objektLib.book_list[0], self.test_objektBook)
        self.assertIsNot(self.test_objektLib.book_list[0], [])

    def test_give_book(self):
        self.test_objektLib.add_book(self.test_objektBook)
        self.test_objektLib.give_book(self.test_objektBook)
        self.assertEqual(self.test_objektLib.book_list, [])
        self.assertNotEqual(self.test_objektLib.book_list, self.test_objektBook)

    def test_valid_books(self):
        self.assertEqual(self.test_objektLib.valid_books(), "there is no books in the library")
        self.test_objektLib.add_book(self.test_objektBook)
        self.assertEqual(self.test_objektLib.valid_books(), 'name - LOR, author  - Tolkien, pages - 123,ISBN - 12345435,\
 reservation - False \n')
        self.assertNotEqual(self.test_objektLib.valid_books(), "there is no books in the library")

    @patch('Library.Library.valid_books', return_value="there is no books in the library")
    def test_valid_books_vs_no_books(self, valid_books):
        self.assertEqual(valid_books(), "there is no books in the library")


def mock_User_book_list():
    return f'name - LOR, author  - Tolkien, pages - {123},ISBN - {12345435}, reservation - False'


class Test_UserClass(unittest.TestCase):
    def setUp(self):
        self.test_objektBook = Library.Book('LOR', 'Tolkien', 123, 12345435)
        self.test_objektUser = Library.User("Roma")
        self.test_objektLib = Library.Library()

    def tearDown(self):
        del self.test_objektUser
        del self.test_objektLib
        del self.test_objektBook

    @patch('Library.User.book_list', side_effect=mock_User_book_list)
    def test_book_list(self, book_list):
        self.assertEqual(self.test_objektUser.book_list(), f'name - LOR, author  - Tolkien, pages - {123},ISBN - {12345435},\
 reservation - False')

    def test_class_initiate(self):
        self.assertIsInstance(self.test_objektUser, Library.User)
        with self.assertRaises(TypeError):
            self.test_objekt_Error_Lib = Library.User()

    def test_take_book(self):
        self.test_objektLib.add_book(self.test_objektBook)
        self.test_objektUser.take_book(self.test_objektBook, self.test_objektLib)
        self.assertIs(self.test_objektUser.book_list[0], self.test_objektBook)
        self.assertIsNot(self.test_objektUser.book_list[0], [])
        self.assertListEqual(self.test_objektLib.book_list, [])

    def test_return_book(self):
        self.test_objektLib.add_book(self.test_objektBook)
        self.test_objektUser.take_book(self.test_objektBook, self.test_objektLib)
        self.test_objektUser.return_book(self.test_objektBook, self.test_objektLib)
        self.assertListEqual(self.test_objektUser.book_list, [])
        self.assertIs(self.test_objektLib.book_list[0], self.test_objektBook)

    def test_reserve_book(self):
        self.test_objektUser.reserve_book(self.test_objektBook)
        self.assertEqual(self.test_objektBook._reserve, True)
        self.assertNotEqual(self.test_objektBook._reserve, False)





