import Library
import pytest



class Test_Book_class:

    def test_class_initiate(self, test_object_book_fixture):
        assert type(test_object_book_fixture) == Library.Book

    def test_error_raise(self):
        with pytest.raises(TypeError):
            Library.Book(123, 124, "123", '123', 123, 124)

    def test_str_method(self, test_object_book_fixture):
        assert str(test_object_book_fixture) == 'name - LOR, author  - Tolkien, pages - 123,ISBN - 12345435,\
 reservation - False'
        assert str(test_object_book_fixture) != 'name - LOR@, author  - Tolkien@, pages - 123@, ISBN - 12345435@'

    def test_name_property(self, test_object_book_fixture):
        assert test_object_book_fixture.name == 'LOR'

    def test_change_reservation(self, test_object_book_fixture):
        test_object_book_fixture.change_reservation(True)
        assert test_object_book_fixture._reserve is True
        assert test_object_book_fixture._reserve is not False


class Test_LibraryClass:

    def test_class_initiate(self, test_object_lib_fixture):
        assert type(test_object_lib_fixture) is Library.Library

    def test_error_raise(self):
        with pytest.raises(TypeError):
            Library.Library(123)

    def test_property_book_list(self, test_object_lib_fixture):
        assert test_object_lib_fixture.book_list == []
        assert test_object_lib_fixture.book_list is not None

    def test_add_book(self, test_object_lib_fixture, test_object_book_fixture):
        test_object_lib_fixture.add_book(test_object_book_fixture)
        assert test_object_lib_fixture.book_list[0] is test_object_book_fixture
        assert test_object_lib_fixture.book_list != []

    def test_give_book(self, test_object_lib_fixture, test_object_book_fixture):
        test_object_lib_fixture.add_book(test_object_book_fixture)
        test_object_lib_fixture.give_book(test_object_book_fixture)
        assert test_object_lib_fixture.book_list == []
        assert test_object_lib_fixture.book_list != [test_object_book_fixture]

    def test_valid_books(self, test_object_lib_fixture, test_object_book_fixture):
        assert test_object_lib_fixture.valid_books() == "there is no books in the library"
        test_object_lib_fixture.add_book(test_object_book_fixture)
        assert test_object_lib_fixture.valid_books() == 'name - LOR, author  - Tolkien, pages - 123,ISBN - 12345435,\
 reservation - False \n'
        assert test_object_lib_fixture.valid_books() != "there is no books in the library"


class Test_UserClass:

    def test_book_list(self, test_object_user_fixture):
        assert test_object_user_fixture.book_list == []

    def test_take_book(self, test_object_user_fixture, test_object_lib_fixture, test_object_book_fixture):
        test_object_lib_fixture.add_book(test_object_book_fixture)
        test_object_user_fixture.take_book(test_object_book_fixture, test_object_lib_fixture)
        assert test_object_user_fixture.book_list[0] is test_object_book_fixture
        assert test_object_lib_fixture.book_list == []

    def test_error_cases(self, test_object_lib_fixture):
        with pytest.raises(TypeError):
            test_object_lib_fixture.add_book()

    def test_error_cases1(self, test_object_user_fixture, test_object_book_fixture):
        with pytest.raises(TypeError):
            test_object_user_fixture.take_book(test_object_book_fixture)

    def test_return_book(self, test_object_user_fixture, test_object_lib_fixture, test_object_book_fixture):
        test_object_lib_fixture.add_book(test_object_book_fixture)
        test_object_user_fixture.take_book(test_object_book_fixture, test_object_lib_fixture)
        test_object_user_fixture.return_book(test_object_book_fixture, test_object_lib_fixture)
        assert test_object_user_fixture.book_list == []
        assert test_object_lib_fixture.book_list[0] is test_object_book_fixture

    def test_reserve_book(self, test_object_user_fixture, test_object_book_fixture):
        test_object_user_fixture.reserve_book(test_object_book_fixture)
        assert test_object_book_fixture._reserve is True

    @pytest.mark.parametrize("atr, error_type", [(None, AttributeError),
                                                 (123, AttributeError),
                                                 ('qwerty', AttributeError),
                                                 (False, AttributeError),
                                                 ([1, 2, 3], AttributeError),
                                                 ((), AttributeError)])
    def test_error_case(self, test_object_user_fixture, atr, error_type):
        with pytest.raises(error_type):
            test_object_user_fixture.reserve_book(atr)
