from time import time, sleep
import threading
t = time()
set_ = {1}
kortj_= range(1,1000000)
def cycle(star,fish):
    t1 = time()
    global set_
    for i in kortj_[star:fish]:
        n = 0
        while True:
            n += 1
            i = i * 0.5 if not i % 2 else i * 3 + 1
            if i == 1:
                break
        if n not in set_:
            set_.add(n)
    print(time()-t1,f'{star}, {fish}',max(set_))

thr1= threading.Thread(target=cycle,name='thr1',args=(-10000,-1))
thr2= threading.Thread(target=cycle,name='thr2',args=(-60000,-10000))
thr3= threading.Thread(target=cycle,name='thr3',args=(-200000,-60000))
thr4= threading.Thread(target=cycle,name='thr4',args=(-950000,-200000))
thr5= threading.Thread(target=cycle,name='thr5',args=(-999999,-950000))
thr5.start()
thr4.start()
thr3.start()
thr2.start()
thr1.start()
thr3.join()
thr4.join()
thr5.join()
print(max(set_))
print(time()-t)
