import pytest
import Dop_task_OOP_CHESS

fig_dict = {'King': Dop_task_OOP_CHESS.King(4, 4),
            'KONb': Dop_task_OOP_CHESS.KONb(4, 4),
            'LADbYA': Dop_task_OOP_CHESS.LADbYA(4, 4),
            'OFICER': Dop_task_OOP_CHESS.OFICER(4, 4),
            'Ferz': Dop_task_OOP_CHESS.Ferz(4, 4)}
#

@pytest.fixture()
def figure_init():
    figure = None
    def figure_name_init(name):
        nonlocal figure
        figure = fig_dict.get(name)
        return figure
    yield figure_name_init
    del figure


