import Dop_task_OOP_CHESS
import pytest


def test_King_init(figure_init):
    assert type(figure_init('King')) == Dop_task_OOP_CHESS.King


@pytest.mark.parametrize('x, y, result', [(1, 1, False),
                                          (5, 5, True),
                                          (4, 5, True),
                                          (3, 4, True),
                                          (6, 3.8, False),
                                          (9, 9, False)])
def test_King_move(figure_init, x, y, result):
    assert figure_init("King").move(x, y) is result


def test_KONb_init(figure_init):
    assert type(figure_init('KONb')) == Dop_task_OOP_CHESS.KONb


@pytest.mark.parametrize('x, y, result', [(1, 1, False),
                                          (2, 4.6, False),
                                          (3, 2, True),
                                          (2, 3, True),
                                          (6, 3, True),
                                          (9, 8, False),
                                          (3, 6, True),
                                          (6, 5, True)])
def test_KONb_move(figure_init, x, y, result):
    assert figure_init('KONb').move(x, y) is result


def test_LADbYA_init(figure_init):
    assert type(figure_init('LADbYA')) == Dop_task_OOP_CHESS.LADbYA


@pytest.mark.parametrize('x, y, result', [(1, 1, False),
                                          (5, 5, False),
                                          (4, 5, True),
                                          (5, 4, True),
                                          (6, 3.1, False),
                                          (9, 9, False),
                                          (4, 1, True),
                                          (1, 4, True),
                                          (8, 4, True)])
def test_LADbYA_move(figure_init, x, y, result):
    assert figure_init('LADbYA').move(x, y) is result


def test_OFICER_init(figure_init):
    assert type(figure_init('OFICER')) == Dop_task_OOP_CHESS.OFICER


@pytest.mark.parametrize('x, y, result', [(1, 1, True),
                                          (5, 5, True),
                                          (8, 8, True),
                                          (3, 3, True),
                                          (3, 8, False),
                                          (9, 9, False),
                                          (1, 1, True),
                                          (2, 2, True),
                                          (2.3, 4, False)])
def test_OFICER_move(figure_init, x, y, result):
    assert figure_init('OFICER').move(x, y) is result


def test_Ferz_init(figure_init):
    assert type(figure_init('Ferz')) == Dop_task_OOP_CHESS.Ferz


@pytest.mark.parametrize('x, y, result', [(1, 1, True),
                                          (5, 5, True),
                                          (8, 8, True),
                                          (3, 3, True),
                                          (3, 8, False),
                                          (9, 9, False),
                                          (1, 1, True),
                                          (2, 2, True),
                                          (2.3, 5, False)])
def test_Ferz_move(figure_init, x, y, result):
    assert figure_init('Ferz').move(x, y) is result
