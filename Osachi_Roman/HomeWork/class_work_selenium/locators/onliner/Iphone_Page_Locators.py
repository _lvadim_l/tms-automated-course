from selenium.webdriver.common.by import By

STORAGE256GB = (By.CSS_SELECTOR, 'a.offers-description-filter-control_switcher:nth-child(3)> span:nth-child(2)')
IPHONE_TITLE = (By.CSS_SELECTOR, '.catalog-masthead__title')
IPHONE_PRICE = (By.CSS_SELECTOR, "a.offers-description__link:nth-child(1)")
