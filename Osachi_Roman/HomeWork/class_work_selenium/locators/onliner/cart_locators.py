from selenium.webdriver.common.by import By

PRODUCT_CARD = (By.CSS_SELECTOR, ".cart-form__body .cart-form__offers-unit_primary")
DELETE_BUTTON = (By.CSS_SELECTOR, '.cart-form__button_remove')
GO_TO_ORDER_PROCESSING = (By.CSS_SELECTOR, ".button-style_primary.cart-form__button")
CART_DESCRIPTION_TEXT = (By.CSS_SELECTOR, '.cart-form__description_extended')