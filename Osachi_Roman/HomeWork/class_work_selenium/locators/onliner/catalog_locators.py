from selenium.webdriver.common.by import By

AUTO_AND_MOTO = (By.XPATH, "//span[contains(text(),'Авто и')]")
CAR_ACOUSTICS = (By.XPATH, '//div[contains(text(),"Автоакустика")]')
ACOUSTICS_CABLES = (By.XPATH, '//span[contains(text(),"автоакустики")]')
COMPUTERS_AND_NETWORK = (By.CSS_SELECTOR, 'li[data-id="2"]')
COMPUTERS_LAPTOPS_MONITORS = (By.XPATH, '//div[contains(text(),"Ноутбуки, компьютеры, мониторы")]')
LAPTOPS = (By.CSS_SELECTOR, '[href="https://catalog.onliner.by/notebook"]')


def check_box_item(name):
    return (By.XPATH, f'//span[contains(text(),"{name}")][@data-bind="html: item.name"][1]')


def from_to_locators(name):
    if name == "до":
        return (By.XPATH,
                '//div/div[2]/input[@class="schema-filter-control__item schema-filter__number-input schema-filter__number-input_price"]')
    return (By.CSS_SELECTOR, f'[placeholder = "{name}"]')


def choose_diagonals_locators(max_or_min):
    if max_or_min == 'min':
        num = 1
    elif max_or_min == 'max':
        num = 2
    return (By.XPATH, f'//option[@value="101"]/ancestor::div[@class="schema-filter__group"]/div[{num}]/select')


def choose_diagonal(max_or_min, diagonal):
    if max_or_min == 'min':
        num = 1
    elif max_or_min == 'max':
        num = 2
    return (By.XPATH, f"//div[{num}]/select/option[text() = '{diagonal}\"']")


SORTING_BUTTON = (By.XPATH, '//span[@class="schema-order__text"]')


def sorting_locator(what_first):
    if what_first == "cheap":
        what_first = "Дешевые"
    elif what_first == "expensive":
        what_first = "Дорогие"
    elif what_first == "popuslar":
        what_first = 'Популярные'
    elif what_first == 'new':
        what_first = 'Новые'
    elif what_first == 'with_comments':
        what_first = 'С отзывами'
    return (By.XPATH, f'//span[text() = "{what_first}"]')


LIST_OF_PRODUCTS = (By.CSS_SELECTOR, 'span[data-bind ^="html: product.extended_name || product.full_name"')
PAGES_OF_CATALOG = (By.CSS_SELECTOR, '.schema-pagination__pages-link')
