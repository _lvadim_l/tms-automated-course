from selenium.webdriver.common.by import By

TITLE_OF_PAGE = (By.CSS_SELECTOR, 'h1.schema-header__title')
PRODUCT_ELEMENTS = (By.XPATH, '//span[@data-bind="html: product.extended_name || product.full_name"]')
