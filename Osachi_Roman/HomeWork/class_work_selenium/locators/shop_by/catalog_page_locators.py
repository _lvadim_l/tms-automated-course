from selenium.webdriver.common.by import By

def shpw_more():
    return (By.CSS_SELECTOR, f'span[class="ModelFilter__OpenHideAttrTxt Page__DarkDotLink"][data-idgroup="prof_1000"]')

def check_box_item(name):
    return (By.XPATH, f'//label[@data-ga="{name}"]')

PRICE_BEFORE = (By.XPATH, f'//input[@name="price_before"]')

PRICE_AFTER = (By.XPATH, f'//input[@name="price_after"]')

LOOK_RESULTS = (By.CSS_SELECTOR, '[data-ga="podbor"]')

DIAGONAL_SHOW = (By.XPATH, '//span[contains(text(), "Диагональ экрана")]')

DIAGONAL_SHOW_MORE = (By.XPATH, '//span[@data-idgroup="prof_5828"][1]')

CHECK_SEARCH_RESULTS = (By.CSS_SELECTOR, '.ModelFilter__TxtBtnFormBlock')

RESULTS_ELEMENTS_ON_FIRST_PAGE = (By.CSS_SELECTOR, '[class="ModelList__InfoModelBlock ModelList__DescModelBlock"]')

SORTING_ELEMENT = (By.CSS_SELECTOR, '.chzn-single.Page__SelectOnBg .chzn-txt-sel')
def price_sorting(name): #дешевых, дорогих
    return (By.XPATH, f'//li[contains(text(), "{name}")]')

ELEMENTS_OF_PAGE = (By.XPATH, '//a[@class="ModelList__LinkModel"]/span[@itemprop="name"]')

CATALOG_last_PAGE = (By.XPATH, '//a[@class="Paging__PageLink hidden-xs"]')