from selenium.webdriver.common.by import By

def file(file_name):
    return (By.XPATH, f'//a[contains(text(),"{file_name}")]')

FILE_UPLOAD = (By.XPATH, '//input[@id="file-upload"]')
UPLOAD = (By.CSS_SELECTOR, '[type="submit"]')
UPLOADED_TEXT= (By.TAG_NAME, 'h3')