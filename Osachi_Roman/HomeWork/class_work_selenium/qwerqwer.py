from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_qwer(driver):
    driver.get('https://www.testandquiz.com/selenium/testing.html')
    elem = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[11]/div/p/button')))
    elem.click()

    alert = driver.switch_to.alert
    print(alert.text)
    alert.accept()
    elem1 = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[12]/div/p[1]/button')))
    elem1.click()
    confirm = driver.switch_to.alert

    confirm.accept()
    # assert confirm.text == "You pressed OK!"
    assert driver.find_element_by_xpath('//*[@id="demo"]').text == "You pressed OK!"
