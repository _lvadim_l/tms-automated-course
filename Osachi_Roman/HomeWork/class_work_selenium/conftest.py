import pytest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions


def pytest_addoption(parser):
    parser.addoption(
        "--browser",
        action="store",
        default="firefox",
        help="browser can be: chrome or firefox",
    )
    parser.addoption(
            "--price",
            action="store",
            default="0",
            help="browser can be: chrome or firefox",
        )

@pytest.fixture(scope="session")
def driver(request):
    browser = request.config.getoption("--browser")

    geckodriver_path = "/home/osachi/Desktop/TMS_Work/tms-automated-course/Osachi_Roman/HomeWork/geckodriver"
    chromedriver_path = "/home/osachi/Desktop/TMS_Work/tms-automated-course/Osachi_Roman/HomeWork/chromedriver"
    download_path = "/home/osachi/selenium/downloads"
    f_type = (
        "application/pdf"
        "vnd.ms-excel,"
        "application/vnd.ms-excel.addin.macroenabled.12,"
        "application/vnd.ms-excel.template.macroenabled.12,"
        "application/vnd.ms-excel.template.macapplication/vnd.ms-excel.sheet.binaryroenabled.12,"
        "application/vnd.ms-excel.sheet.macroenabled.12,"
        "application/octet-stream"
    )
    if browser == "firefox":
        options = FirefoxOptions()
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.helperApps.alwaysAsk.force", False)
        profile.set_preference("browser.download.useDownloadDir", True)
        profile.set_preference("browser.download.dir", download_path)
        profile.set_preference("pdfjs.disabled", True)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", f_type)
        # options.add_argument("--headless")

        driver = webdriver.Firefox(
            profile, executable_path=geckodriver_path, options=options
        )
        driver.maximize_window()
        # driver.set_window_size(1920, 1080)
        yield driver
        driver.quit()

    elif browser == "chrome":
        chrome_options = ChromeOptions()
        prefs = {"download.default_directory": download_path}
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-setuid-sandbox")
        chrome_options.add_experimental_option("prefs", prefs)
        # chrome_options.add_argument("--headless")
        driver = webdriver.Chrome(
            executable_path=chromedriver_path, options=chrome_options
        )
        driver.maximize_window()
        # driver.set_window_size(1920, 1080)
        yield driver
        driver.quit()
