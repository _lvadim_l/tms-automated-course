from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.dowmload_upload_locators import home_page_locators
class Files(BasePage):
    def click_on_file(self, file_name):
        self.find_element(home_page_locators.file(file_name)).click()
    def upload_file(self, file_path):
        self.find_element(home_page_locators.FILE_UPLOAD).send_keys(file_path)
    def click_upload_button(self):
        self.find_element(home_page_locators.UPLOAD).click()
    def get_text_uploded(self):
        return self.find_element(home_page_locators.UPLOADED_TEXT).text