from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.onliner import catalog_locators
from time import sleep

class Catalog_page(BasePage):

    def click_to_auto_and_moto(self):
        self.find_element(catalog_locators.AUTO_AND_MOTO).click()

    def move_to_car_aucustic_element(self):
        elem = self.find_element(catalog_locators.CAR_ACOUSTICS)
        self.move_to_element(elem)

    def move_to_auto_cable_page(self):
        self.find_element(catalog_locators.ACOUSTICS_CABLES).click()

    def click_on_computers_and_network(self):
        sleep(0.5)
        self.find_element(catalog_locators.COMPUTERS_AND_NETWORK).click()

    def move_to_computers_laptops_monitors(self):
        sleep(0.5)
        elem = self.find_element(catalog_locators.COMPUTERS_LAPTOPS_MONITORS)
        self.move_to_element_vs_JS(elem, catalog_locators.COMPUTERS_LAPTOPS_MONITORS)
        elem.click()

    def click_on_laptops(self):
        elem = self.find_element(catalog_locators.LAPTOPS)
        self.move_to_element_vs_JS(elem, catalog_locators.LAPTOPS)
        elem.click()

    def click_on_checkbox_item(self, item_name):
        self.find_element(catalog_locators.check_box_item(item_name)).click()

    def chose_scope_values(self, scope, value):
        elem = self.find_element(catalog_locators.from_to_locators(scope))
        self.move_to_element_vs_JS(elem, catalog_locators.from_to_locators(scope))
        elem.click()
        elem.send_keys(value)
        sleep(1)

    def click_on_diagonal_choser(self, max_or_min):
        elem = self.find_element(catalog_locators.choose_diagonals_locators(max_or_min))
        sleep(0.2)
        self.move_to_element_vs_JS(elem, catalog_locators.choose_diagonals_locators(max_or_min))
        self.click_with_javascript(elem)
        sleep(1)
        elem.click()
        sleep(1)

    def choose_diagonal(self, max_or_min, diagonal):
        locator = catalog_locators.choose_diagonal(max_or_min=max_or_min, diagonal=diagonal)
        elem = self.find_element(locator)
        sleep(1)
        elem.click()

    def click_on_sorting_button(self):
        elem = self.find_element(catalog_locators.SORTING_BUTTON)
        self.move_to_element_vs_JS(elem, catalog_locators.SORTING_BUTTON)
        elem.click()

    def choose_sorting_from(self, what_first):
        elem = self.find_element(catalog_locators.sorting_locator(what_first))
        self.move_to_element_vs_JS(elem, catalog_locators.sorting_locator(what_first))
        elem.click()

    def get_name_of_first_product(self):
        sleep(1)
        elems = self.find_elements(catalog_locators.LIST_OF_PRODUCTS)
        return elems[0].text

    def get_name_fo_the_last_product(self):
        sleep(1)
        elems = self.find_elements(catalog_locators.LIST_OF_PRODUCTS)
        sleep(0.5)
        return elems[-1].text

    def move_to_last_page(self):
        elems = self.find_elements(catalog_locators.PAGES_OF_CATALOG)
        last_page = elems[-1]
        self.click_with_javascript(last_page)
