from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.onliner import home_page_locators
from typing import List
from time import sleep
class HomePage(BasePage):

    def send_text_input(self, text):
        self.find_element(home_page_locators.MAIN_INPUT).send_keys(text)

    def switch_to_flea_market(self):
        self.switch_to_frame(home_page_locators.IFRAME)
        self.find_element(home_page_locators.FLEA_MARKET).click()

    def get_search_results_from_catalog(self) -> List:
        elements = self.find_elements(home_page_locators.SEARCH_CATALOG_RESULTS)
        assert len(elements) > 0, "Element not found"

        return elements

    def click_on_element_by_index(self, index: int, web_elem_list: List):
        sleep(1)
        assert len(web_elem_list) > 0
        web_elem_list[index].click()

    def switch_to_catalog(self):
        self.find_element(home_page_locators.CATALOG).click()





