from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.onliner import product_locators
from typing import List
from time import sleep
class Product_page(BasePage):
    def get_headline_text(self) -> str:
        element = self.find_element(product_locators.HEAD_LINE_IN_CATALOG)
        sleep(0.5) #TODO refactor sleeps in tests
        return element.text

    def click_on_cart_button(self):
        self.find_element(product_locators.ADD_TO_CART).click()
        
    def click_on_activ_cart_button(self):
        self.find_element(product_locators.ACTIV_CART_BUTTON).click()