from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.onliner import cable_page_locators
from typing import List
from time import sleep

class CablePage(BasePage):
    def return_title_text(self):
        return self.find_element(cable_page_locators.TITLE_OF_PAGE).text

    def return_list_of_products(self) -> List:
        product_list = self.find_elements(cable_page_locators.PRODUCT_ELEMENTS)
        assert product_list != [], 'Elements not found'
        sleep(1)
        return product_list

    def click_on_first_element(self, list):
        list[0].click()

