from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.onliner import cart_locators

from time import sleep

class CartPage(BasePage):

    def get_card_text(self):
        elem = self.find_element(cart_locators.PRODUCT_CARD)
        sleep(0.5)
        return elem.text

    def delete_card(self):
        delete_button = self.find_element(cart_locators.DELETE_BUTTON)
        self.move_to_element(delete_button)
        sleep(0.5)
        self.find_element(cart_locators.DELETE_BUTTON).click()
    
    def check_absence_element(self, locator=cart_locators.GO_TO_ORDER_PROCESSING, time=5):
        super(CartPage, self).check_absence_element(locator=cart_locators.GO_TO_ORDER_PROCESSING)

    def get_cart_page_description_text(self):
        return self.find_element(cart_locators.CART_DESCRIPTION_TEXT).text

