from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.shop_by import catalog_page_locators
from time import sleep
class CatalogPage(BasePage):

    def click_show_more(self):
        locator = catalog_page_locators.shpw_more()
        sleep(1)
        elem = self.find_element(locator)
        sleep(1)
        self.move_to_element_vs_JS(elem, locator)
        sleep(1)
        self.click_with_javascript(elem)

    def click_on_check_box_element(self, laptop_name):
        sleep(1)
        locator = catalog_page_locators.check_box_item(laptop_name)
        elem = self.find_element(locator)
        sleep(1)
        self.move_to_element_vs_JS(elem, locator)
        sleep(1)
        self.click_with_javascript(elem)
        sleep(1)

    def fill_price_before(self, price):
        locator = catalog_page_locators.PRICE_BEFORE
        elem = self.find_element(locator)
        self.move_to_element_vs_JS(elem, locator)
        elem.send_keys(price)

    def fill_price_after(self, price):
        locator = catalog_page_locators.PRICE_AFTER
        elem = self.find_element(locator)
        self.move_to_element_vs_JS(elem, locator)
        elem.send_keys(price)

    def show_diagonals(self):
        elem = self.find_element(catalog_page_locators.DIAGONAL_SHOW)
        self.move_to_element_vs_JS(elem, catalog_page_locators.DIAGONAL_SHOW)
        sleep(1)
        self.click_with_javascript(elem)

        sleep(1)
        elem = self.find_element(catalog_page_locators.DIAGONAL_SHOW_MORE)
        sleep(1)
        self.move_to_element_vs_JS(elem, catalog_page_locators.DIAGONAL_SHOW_MORE)
        sleep(1)
        self.click_with_javascript(elem)

    def click_on_search_results(self):
        elem = self.find_element(catalog_page_locators.CHECK_SEARCH_RESULTS)
        self.move_to_element_vs_JS(elem, catalog_page_locators.CHECK_SEARCH_RESULTS)
        sleep(1)
        self.click_with_javascript(elem)

    def get_number_of_results_on_first_page(self):
        elements = self.find_elements(catalog_page_locators.RESULTS_ELEMENTS_ON_FIRST_PAGE)
        return len(elements)

    def sorting_elements_by_price(self, sorting):
        sleep(1)
        elem = self.find_element(catalog_page_locators.SORTING_ELEMENT)
        sleep(0.5)
        self.move_to_element_vs_JS(elem, catalog_page_locators.SORTING_ELEMENT)
        sleep(1)
        self.click_with_javascript(elem)
        elem.click()
        sleep(1)
        locator = catalog_page_locators.price_sorting(sorting)
        elem1 = self.find_element(locator)
        sleep(1)
        self.move_to_element_vs_JS(elem1, locator)
        sleep(1)
        elem1.click()
        sleep(3)

    def get_name_of_first_elem(self):
        sleep(3)
        elems = self.find_elements(catalog_page_locators.ELEMENTS_OF_PAGE)
        sleep(0.3)
        return elems[0].text

    def get_name_of_last_elem(self):
        sleep(3)
        elem = self.find_element(catalog_page_locators.CATALOG_last_PAGE)
        sleep(0.4)
        self.move_to_element_vs_JS(elem, catalog_page_locators.CATALOG_last_PAGE)
        sleep(3)
        elems = self.find_elements(catalog_page_locators.ELEMENTS_OF_PAGE)
        sleep(0.3)
        return elems[-1].text

    def move_to_the_last_page_of_catalog(self):
        elem = self.find_elements(catalog_page_locators.CATALOG_last_PAGE)
        sleep(0.5)
        self.click_with_javascript(elem[-1])
        sleep(0.5)