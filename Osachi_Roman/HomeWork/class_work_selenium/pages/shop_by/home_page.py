from class_work_selenium.base_pagee import BasePage
from class_work_selenium.locators.shop_by import home_page_locator
from time import sleep
class Home_page(BasePage):
    def move_to_catalog(self):
        elem = self.find_element(home_page_locator.CATALOG)
        self.move_to_element(elem)
        sleep(1)
    def move_to_computers(self):
        elem = self.find_element(home_page_locator.COMPUTERS)
        self.move_to_element(elem)
        sleep(1)
    def move_to_laptop_and_accs(self):
        elem = self.find_element(home_page_locator.LAPTOPS_AND_ACCS)
        self.move_to_element(elem)
        sleep(1)
    def click_to_laptops(self):
        elem = self.find_element(home_page_locator.LAPTOPS)
        sleep(1)
        self.click_with_javascript(elem)