from class_work_selenium.pages.Onliner.home_page import HomePage
from class_work_selenium.pages.Onliner.porduct_page import Product_page
from class_work_selenium.pages.Onliner.cart_page import CartPage
from time import sleep

iphone_text = "Apple iPhone"


def test_input(driver):
    driver.get("https://www.onliner.by/")
    home_page = HomePage(driver=driver)

    home_page.send_text_input("iphone 11")

    home_page.switch_to_flea_market()
    elements = home_page.get_search_results_from_catalog()
    home_page.click_on_element_by_index(web_elem_list=elements, index=0)
    home_page.switch_to_current_window()


def test_product_page(driver):
    product_page = Product_page(driver=driver)
    headlinetext = product_page.get_headline_text()
    assert iphone_text in headlinetext
    product_page.click_on_cart_button()
    product_page.click_on_activ_cart_button()


def test_card_page(driver):
    cart_page = CartPage(driver)
    card_text = cart_page.get_card_text()
    assert iphone_text in card_text
    cart_page.delete_card()
    sleep(0.3)
    cart_page.check_absence_element()
    sleep(4)
