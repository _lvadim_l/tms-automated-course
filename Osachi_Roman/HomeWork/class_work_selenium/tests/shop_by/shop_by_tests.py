from class_work_selenium.pages.shop_by.home_page import Home_page
from time import sleep
from class_work_selenium.pages.shop_by.catalog_page import CatalogPage

def test_home_page(driver):
    driver.get('https://shop.by/')
    home_page = Home_page(driver)
    home_page.move_to_catalog()
    home_page.move_to_computers()
    home_page.move_to_laptop_and_accs()
    home_page.click_to_laptops()

    catalog_page = CatalogPage(driver)
    catalog_page.click_show_more()
    catalog_page.click_on_check_box_element("Lenovo")
    catalog_page.click_on_check_box_element("Dell")
    catalog_page.click_on_check_box_element("HP")
    catalog_page.fill_price_before(1500)
    catalog_page.fill_price_after(3000)
    catalog_page.show_diagonals()
    breakpoint()
    catalog_page.click_on_check_box_element("12.5")
    catalog_page.click_on_check_box_element("13.3")
    catalog_page.click_on_search_results()
    catalog_page.get_number_of_results_on_first_page()
    catalog_page.sorting_elements_by_price("дешевых")
    text_of_first_item = catalog_page.get_name_of_first_elem()
    catalog_page.sorting_elements_by_price("дорогих")
    catalog_page.move_to_the_last_page_of_catalog()
    text_of_last_item = catalog_page.get_name_of_last_elem()
    assert text_of_first_item == text_of_last_item





