import pytest
import Library


@pytest.fixture()
def test_object_book_fixture():
    test_object_book = Library.Book('LOR', 'Tolkien', 123, 12345435)
    print(type(test_object_book))
    yield test_object_book
    del test_object_book


@pytest.fixture()
def test_object_lib_fixture():
    test_object_lib = Library.Library()
    yield test_object_lib
    del test_object_lib


@pytest.fixture()
def test_object_user_fixture():
    test_object_user = Library.User("Roma")
    yield test_object_user
    del test_object_user
