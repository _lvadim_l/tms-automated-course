"""
Создать класс Store и добавить в него словарь, где будет храниться item: price.
Реализовать в классе методы add_item, remove_item
Реализовать метод overall_price_discount который считает полную сумму со скидкой добавленных в магазин товаров
Реализовать метод overall_price_no_discount
который считает сумму товаров без скидки."""
pricelist = {}
class Store:

    @staticmethod
    def add_item(item:str,price:int,discount=False):
        pricelist[item] = {}
        pricelist[item]['price'] = price
        pricelist[item]['discount_in_percents'] = discount

    @staticmethod
    def remove_item(item=str):
        pricelist.pop(item)

    @staticmethod
    def overall_price_discount():
        sum = 0
        for item in pricelist:
            if pricelist.get(item)['discount_in_percents'] is not False:
                sum += pricelist.get(item).get('price') - pricelist.get(item).get('price') * pricelist[item]['discount_in_percents'] / 100
            else:
                sum += pricelist.get(item).get('price')
        return sum


    @staticmethod
    def overall_price_discount_no_discount():
        sum = 0
        for item in pricelist:
            sum += pricelist.get(item).get('price')
        return sum



Store.add_item(item='phone', price=330, discount=30)
Store.add_item(item='bike', price=1230)
Store.add_item('mouse', 70)
Store.add_item('SmartWach', 500, discount=15)
Store.add_item(item='bike123456789', price=1230)
print(pricelist)
Store.remove_item(item='bike123456789')
print(pricelist)
print(Store.overall_price_discount_no_discount())
print(Store.overall_price_discount())