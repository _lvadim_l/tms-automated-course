from datetime import datetime

import pytest
import os

from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions

def pytest_addoption(parser):
    parser.addoption(
        "--browser",
        action="store",
        default="chrome",
        help="browser can be: chrome or firefox",
    )
    parser.addoption(
        "--start_price",
        action="store",
        default="1500",
        help="",
    )
    parser.addoption(
        "--end_price",
        action="store",
        default="3000",
        help="",
    )
    parser.addoption(
        "--mode",
        action="store",
        default="headless",
        help="browser mode can be: headless or normal",
    )

@pytest.fixture(scope="function")
def driver(request):
    browser = request.config.getoption("--browser")
    mode = request.config.getoption("--mode")

    chromedriver_path = "/home/vadim/tms-automated-course/chromedriver"
    geckodriver_path = "/home/vadim/tms-automated-course/geckodriver"

    download_path = "/home/vadim/selenium/Downloads"

    f_type = (
        "application/pdf"
        "vnd.ms-excel,"
        "application/vnd.ms-excel.addin.macroenabled.12,"
        "application/vnd.ms-excel.template.macroenabled.12,"
        "application/vnd.ms-excel.template.macapplication/vnd.ms-excel.sheet.binaryroenabled.12,"
        "application/vnd.ms-excel.sheet.macroenabled.12,"
        "application/octet-stream"
    )

    if browser == "firefox":
        options = FirefoxOptions()
        options.add_argument("--" + mode)
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.helperApps.alwaysAsk.force", False)
        profile.set_preference("browser.download.useDownloadDir", True)
        profile.set_preference("browser.download.dir", download_path)
        profile.set_preference("pdfjs.disabled", True)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", f_type)
        driver = webdriver.Firefox(
            profile, options=options, executable_path=geckodriver_path
        )
        driver.maximize_window()

        yield driver
        driver.quit()

    elif browser == "chrome":
        chrome_options = ChromeOptions()
        prefs = {"download.default_directory": download_path}
        chrome_options.add_argument("--" + mode)
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-setuid-sandbox")
        chrome_options.add_experimental_option("prefs", prefs)
        driver = webdriver.Chrome(
             options=chrome_options, executable_path=chromedriver_path
        )
        driver.maximize_window()

        yield driver
        driver.quit()

    elif browser == "selenoid_firefox":
        capabilities = {
            "browserName": "firefox",
            "browserVersion": "88.0",
            "selenoid:options": {
                "enableVNC": True,
                "enableVideo": False
            }
        }
        options = FirefoxOptions()
        options.set_preference("browser.download.folderList", 2)
        options.set_preference("browser.download.manager.showWhenStarting", False)
        options.set_preference("browser.helperApps.alwaysAsk.force", False)
        options.set_preference("browser.download.useDownloadDir", True)
        options.set_preference("browser.download.dir", download_path)
        options.set_preference("pdfjs.disabled", True)
        options.set_preference("browser.helperApps.neverAsk.saveToDisk", f_type)

        driver = webdriver.Remote(
            command_executor="http://localhost:4444/wd/hub",
            desired_capabilities=capabilities,
            options=options)

        driver.maximize_window()

        yield driver
        driver.quit()


@pytest.fixture(scope="function")
def start_price(request):
    start_price = request.config.getoption("--start_price")
    return start_price

@pytest.fixture(scope="function")
def end_price(request):
    end_price = request.config.getoption("--end_price")
    return end_price
