import unittest
from books import User, Book


class TestUser(unittest.TestCase):

    def setUp(self):
        self.user = User('Anton')
        self.user1 = User('Alex')
        self.book = Book("Сияние", "С. Кинг", 500, 111111)

    def test_user_positive(self):
        q = self.user
        self.assertIsNotNone(q)

    def test_username_negative(self):
        with self.assertRaises(AssertionError):
            n = self.user.name
            self.assertIsNone(n)

    def test_book_positive(self):
        b = self.book
        self.assertIsNotNone(b)

    def test_book_author_negative(self):
        with self.assertRaises(AssertionError):
            a = self.book.author
            self.assertIsNone(a)

    def test_takeabook_positive(self):
        b = self.book
        u = self.user
        self.assertIsInstance(b, Book)
        self.assertIsInstance(u, User)
        b.readby = None
        b.reservedby = None
        self.assertIsNone(b.readby, b.reservedby)
        u.takeabook(b)
        self.assertIsNotNone(b.readby)

    def test_takeabook_negative(self):
        b = self.book
        u = self.user
        u1 = self.user1
        b.readby = u
        self.assertIs(b.readby, u)
        with self.assertRaises(AssertionError):
            u1.takeabook(b)
            self.assertIs(b.readby, u1)

    def test_reserveabook_positive(self):
        b = self.book
        u = self.user
        self.assertIsInstance(b, Book)
        self.assertIsInstance(u, User)
        b.readby = None
        b.reservedby = None
        self.assertIsNone(b.readby, b.reservedby)
        u.reserveabook(b)
        self.assertIsNotNone(b.reservedby)

    def test_reserveabook_negative(self):
        b = self.book
        u = self.user
        u1 = self.user1
        b.reservedby = u
        self.assertIs(b.reservedby, u)
        with self.assertRaises(AssertionError):
            u1.reserveabook(b)
            self.assertIs(b.reservedby, u1)

    def test_returnabook_positive(self):
        b = self.book
        u = self.user
        self.assertIsInstance(b, Book)
        self.assertIsInstance(u, User)
        b.readby = u
        self.assertIs(b.readby, u)
        u.returnabook(b)
        self.assertIsNone(b.readby)

    def test_returnabook_negative(self):
        b = self.book
        u = self.user
        self.assertIsInstance(b, Book)
        self.assertIsInstance(u, User)
        b.readby = u
        self.assertIs(b.readby, u)
        with self.assertRaises(AssertionError):
            u.returnabook(b)
            self.assertIsNotNone(b.readby)









