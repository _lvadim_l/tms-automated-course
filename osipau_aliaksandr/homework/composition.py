class WinDoor:

    def __init__(self, x, y, n):
        self.name = n
        self.length = x
        self.width = y

    def windoorSurface(self):
        return self.length * self.width


class Room:

    def __init__(self, x, y, z):
        self.length1 = x
        self.length2 = y
        self.length3 = z
        self.wd = []

    def roomSurface(self):
        return 2 * self.length3 * (self.length1 + self.length2)

    def addWD(self, w, h, n):
        self.wd.append(WinDoor(w, h, n))

    def workSurface(self):
        new_square = self.roomSurface()
        for i in self.wd:
            new_square -= i.square
        return new_square

    def wallpaper(self, lwp, wwp):
        wpsquare = lwp * wwp
        wrkSrfc = self.workSurface()
        quantity = wrkSrfc // wpsquare + 1
        return quantity



wd1 = WinDoor(x=int(input('Длина окна или двери: ')), y=int(input('Ширина окна или двери: ')),
              n=input('Окно или дверь: '))
print(f'{wd1.name} имеет площадь {wd1.windoorSurface()}')
r1 = Room(x=int(input('Длина стены: ')), y=int(input('Длина 2-ой стены: ')), z=int(input('Высота стены: ')))
print(f'Общая площадь комнаты равна {r1.roomSurface()}')
lwp = int(input('Длина рулона: '))
wwp = int(input('Ширина рулона: '))
print(f'Нужное количество рулонов = {r1.wallpaper(lwp, wwp)}')


