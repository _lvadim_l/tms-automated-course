class Book:

    def __init__(self, n, a, p, i):
        self.name = n
        self.author = a
        self.pages = p
        self.isbn = i
        self.reservedby = None
        self.readby = None

class User:

    def __init__(self, n):
        self.name = n

    def takeabook(self, book):
        if isinstance(book, Book):
            if book.reservedby is not None or book.readby is not None:
                print('К сожалению, эта книга отсутсвует или зарезервирована.')
            else:
                book.readby = self
        else:
            print('К сожалению, такой книги не существует в нашей библиотеке.')

    def reserveabook(self, book):
        if isinstance(book, Book):
            if book.reservedby is not None or book.readby is not None:
                print('К сожалению, эта книга отсутсвует или зарезервирована.')
            else:
                book.reservedby = self
        else:
            print('К сожалению, такой книги не существует в нашей библиотеке.')

    def returnabook(self, book):
        if isinstance(book, Book):
            if book.readby is self:
                book.readby = None
        else:
            print('К сожалению, такой книги не существует в нашей библиотеке.')


book1 = Book("Сияние", "С. Кинг", 500, 111111)
book2 = Book("Доктор Сон", "С. Кинг", 700, 111112)
book3 = Book("Мертвые души", "Н. Гоголь", 400, 111113)
book4 = Book("Затерянный мир", "А. Конан Дойл", 550, 111114)
user1 = User(input('Ваше имя? : '))
a = int(input('Выберите операцию (введите цифру): 1) взять 2) зарезервировать 3) вернуть \n'))
if a == 1:
    b = input(f'Выберите книгу: \n {book1.name},\n {book2.name},\n {book3.name},\n {book4.name}\n')
    if b == book1.name:
        user1.takeabook(book1)
        print(f'Вы взяли книгу {book1.name}')
    elif b == book2.name:
        user1.takeabook(book2)
        print(f'Вы взяли книгу {book2.name}')
    elif b == book3.name:
        user1.takeabook(book3)
        print(f'Вы взяли книгу {book3.name}')
    elif b == book4.name:
        user1.takeabook(book4)
        print(f'Вы взяли книгу {book4.name}')
    else:
        print('Выберите книгу из предложенных.')
elif a == 2:
    b = input(f'Выберите книгу: \n {book1.name},\n {book2.name},\n {book3.name},\n {book4.name}\n')
    if b == book1.name:
        user1.reserveabook(book1)
        print(f'Вы зарезервировали книгу {book1.name}')
    elif b == book2.name:
        user1.reserveabook(book2)
        print(f'Вы зарезервировали книгу {book2.name}')
    elif b == book3.name:
        user1.reserveabook(book3)
        print(f'Вы зарезервировали книгу {book3.name}')
    elif b == book4.name:
        user1.reserveabook(book4)
        print(f'Вы зарезервировали книгу {book4.name}')
    else:
        print('Выберите книгу из предложенных.')
elif a == 3:
    b = input(f'Выберите книгу: \n {book1.name},\n {book2.name},\n {book3.name},\n {book4.name}\n')
    if b == book1.name:
        user1.returnabook(book1)
        print(f'Спасибо за возврат книги {book1.name}')
    elif b == book2.name:
        user1.returnabook(book2)
        print(f'Спасибо за возврат книги {book2.name}')
    elif b == book3.name:
        user1.returnabook(book3)
        print(f'Спасибо за возврат книги {book3.name}')
    elif b == book4.name:
        user1.returnabook(book4)
        print(f'Спасибо за возврат книги {book4.name}')
    else:
        print('Выберите книгу из предложенных.')
else:
    print('Выберите одну из трех операций.')

