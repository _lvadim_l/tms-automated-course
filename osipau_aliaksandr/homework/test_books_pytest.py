import pytest
from books import User, Book

@pytest.fixture()
def create_book():
    book1 = Book("Сияние", "С. Кинг", 500, 111111)
    yield book1
    del book1

@pytest.fixture()
def create_user():
    user1 = User("Alex")
    yield user1
    del user1

@pytest.fixture()
def create_user1():
    user2 = User("Arnold")
    yield user2
    del user2

def test_book_positive(create_book):
    assert isinstance(create_book, Book)
    assert create_book.name == "Сияние"
    assert create_book.author == "С. Кинг"
    assert create_book.pages == 500
    assert create_book.isbn == 111111
    assert create_book.readby == None
    assert create_book.reservedby == None


def test_book_negative(create_book):
    assert isinstance(create_book, Book)
    with pytest.raises(AssertionError):
        assert create_book.author == "С. Кинг 2"
        assert create_book.name == "Оно"
        assert create_book.pages == 650
        assert create_book.isbn == 111113
        assert type(create_book.name) == float
    with pytest.raises(AttributeError):
        assert create_book.status == "New"

def test_user_positive(create_user):
    assert isinstance(create_user, User)
    assert create_user.name == "Alex"

def test_user_negative(create_user):
    assert isinstance(create_user, User)
    with pytest.raises(AssertionError):
        assert create_user.name == "Alexandr"
    with pytest.raises(AttributeError):
        assert create_user.surname == "Lincoln"

def test_takeabook_positive(create_book, create_user):
    assert isinstance(create_book, Book)
    assert isinstance(create_user, User)
    assert create_book.readby == None
    assert create_book.reservedby == None
    create_user.takeabook(create_book)
    assert create_book.readby == create_user

def test_takeabook_negative(create_book, create_user, create_user1):
    assert isinstance(create_book, Book)
    assert isinstance(create_user1, User)
    assert isinstance(create_user, User)
    create_book.readby = create_user
    with pytest.raises(AssertionError):
        create_user1.takeabook(create_book)
        assert create_book.readby == create_user1

def test_reserveabook_positive(create_book, create_user):
    assert isinstance(create_book, Book)
    assert isinstance(create_user, User)
    assert create_book.readby == None
    assert create_book.reservedby == None
    create_user.reserveabook(create_book)
    assert create_book.reservedby == create_user

def test_reserveabook_negative(create_book, create_user, create_user1):
    assert isinstance(create_book, Book)
    assert isinstance(create_user, User)
    assert isinstance(create_user1, User)
    with pytest.raises(NameError):
        create_book.reservedby = create_user2
    create_book.reservedby = create_user
    with pytest.raises(AssertionError):
        create_user1.reserveabook(create_book)
        assert create_book.reservedby == create_user1

def test_returnabook_positive(create_book, create_user):
    assert isinstance(create_book, Book)
    assert isinstance(create_user, User)
    create_book.readby = create_user
    assert create_book.readby == create_user
    create_user.returnabook(create_book)
    assert create_book.readby == None

def test_returnabook_negative(create_book, create_user):
    assert isinstance(create_book, Book)
    assert isinstance(create_user, User)
    with pytest.raises(NameError):
        create_book.reservedby = create_user7
    create_book.readby = create_user
    assert create_book.readby == create_user
    with pytest.raises(AssertionError):
        create_user.returnabook(create_book)
        assert create_book.readby is not None






