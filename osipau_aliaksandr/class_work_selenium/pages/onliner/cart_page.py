import time
from osipau_aliaksandr.class_work_selenium.base_page import BasePage
from osipau_aliaksandr.class_work_selenium.locators.onliner import cart_locators

class CartPage(BasePage):

    def get_card_text(self) -> str:
        elem = self.find_element(cart_locators.PRODUCT_CARD)
        time.sleep(0.5)
        return elem.text

    def delete_product_from_cart(self):
        trash = self.find_element(cart_locators.DELETE_FROM_CART_BUTTON)
        self.move_to_element(trash)
        self.find_element(cart_locators.DELETE_FROM_CART_BUTTON).click()

    def check_absence_of_element_in_cart(self):
        self.check_absence_element(cart_locators.ORDER_PROCESSING)