from osipau_aliaksandr.class_work_selenium.base_page import BasePage
from osipau_aliaksandr.class_work_selenium.locators.onliner import catalogue_locators
from typing import List
import time

class CataloguePage(BasePage):

    def get_results_from_catalogue_categories(self) -> List:
        elements = self.find_elements(catalogue_locators.CATALOGUE_CATEGORIES)
        assert len(elements) > 0, "Elements not found"
        return elements

    def click_on_element_by_index(self, index: int, web_elem_list: List):
        assert len(web_elem_list) > 0, "List of elements is empty"
        web_elem_list[index].click()

    def move_to_car_acoustics(self):
        element = self.find_element(catalogue_locators.CAR_ACOUSTICS)
        self.move_to_element(element)

    def click_on_car_audio_cables(self):
        self.find_element(catalogue_locators.CAR_AUDIO_CABLES).click()

    def get_header_text(self):
        element = self.find_element(catalogue_locators.CATALOGUE_HEADER)
        return element.text

    def get_results_from_catalogue_products(self) -> List:
        elements = self.find_elements(catalogue_locators.CATALOGUE_PRODUCTS)
        assert len(elements) > 0, "Elements not found"
        return elements

    def move_to_laptops_computers_monitors(self):
        element = self.find_element(catalogue_locators.LAPTOPS_COMPUTERS_MONITORS)
        self.move_to_element(element)

    def click_on_laptops(self):
        self.find_element(catalogue_locators.LAPTOPS).click()

    def click_all_options(self, index):
        locator = catalogue_locators.all_options(index)
        element = self.find_element(locator)
        self.move_to_element_js(web_element=element, locator=locator)
        time.sleep(1)
        element.click()
        time.sleep(1)

    def click_laptop_name_checkbox(self, name):
        locator = catalogue_locators.laptop_name(name)
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        time.sleep(1)
        elem.click()
        time.sleep(3)

    def fill_price_from(self, value):
        locator = catalogue_locators.PRICE_FROM
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        time.sleep(1)
        elem.send_keys(value)
        time.sleep(1)

    def fill_price_to(self, value):
        locator = catalogue_locators.PRICE_TO
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        time.sleep(1)
        elem.send_keys(value)
        time.sleep(1)

    # def click_show_more_list(self, index):
    #     locator = catalogue_locators.show_more_list(index)
    #     elem = self.find_element(locator)
    #     self.move_to_element_js(web_element=elem, locator=locator)
    #     time.sleep(1)
    #     elem.click()
    #     time.sleep(2)
    #
    # def select_diagonal(self, index, value):
    #     locator = catalogue_locators.diagonal_select(index, value)
    #     elem = self.find_element(locator)
    #     self.move_to_element_js(web_element=elem, locator=locator)
    #     time.sleep(1)
    #     elem.click()
    #     time.sleep(1)

    def click_diagonal_value(self, value):
        locator = catalogue_locators.diagonal_select(value)
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        time.sleep(2)
        elem.click()
        time.sleep(2)

    def get_all_elements_on_page(self):
        elements = self.find_elements(catalogue_locators.ELEMENTS_ON_PAGE)
        print(len(elements))

    def click_on_sort_menu(self):
        locator = catalogue_locators.SORT_MENU
        element = self.find_element(locator)
        self.move_to_element_js(web_element=element, locator=locator)
        time.sleep(1)
        element.click()
        time.sleep(1)

    def select_low_price_sort(self):
        self.find_element(catalogue_locators.LOW_PRICE_SORT).click()
        time.sleep(1)

    def first_item_title(self):
        locator = catalogue_locators.FIRST_ITEM_TITLE
        elems = self.find_elements(locator)
        print(len(elems))
        first_item = elems[1]
        self.move_to_element_js(web_element=first_item, locator=locator)
        print(first_item.text)

    def select_high_price_sort(self):
        self.find_element(catalogue_locators.HIGH_PRICE_SORT).click()
        time.sleep(1)

    def click_on_pagination_dropdown(self):
        locator = catalogue_locators.PAGINATION_DROPDOWN
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        elem.click()
        time.sleep(1)

    def last_page_of_catalogue(self):
        locator = catalogue_locators.PAGES_LIST
        elems = self.find_elements(locator)
        print(len(elems))
        last_page = elems[-1]
        self.move_to_element_js(web_element=last_page, locator=locator)
        time.sleep(2)
        last_page.click()
        time.sleep(1)

    def last_item_title(self):
        locator = catalogue_locators.LAST_ITEM_TITLE
        elems = self.find_elements(locator)
        print(len(elems))
        last_item = elems[-1]
        self.move_to_element_js(web_element=last_item, locator=locator)
        print(last_item.text)
