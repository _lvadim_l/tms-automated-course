import time
from osipau_aliaksandr.class_work_selenium.base_page import BasePage
from osipau_aliaksandr.class_work_selenium.locators.shop_by import home_page_locators

class HomePage(BasePage):

    def move_to_catalogue(self):
        elem = self.find_element(home_page_locators.CATALOGUE)
        self.move_to_element(elem)

    def move_to_computers(self):
        elem = self.find_element(home_page_locators.COMPUTERS)
        self.move_to_element(elem)

    def move_to_laptops_and_accessoires(self):
        elem = self.find_element(home_page_locators.LAPTOPS_AND_ACCESSOIRES)
        self.move_to_element(elem)

    def click_on_laptops(self):
        elem = self.find_element(home_page_locators.LAPTOPS)
        time.sleep(0.5)
        self.click_with_javascript(elem)
