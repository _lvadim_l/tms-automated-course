import time
from osipau_aliaksandr.class_work_selenium.base_page import BasePage
from osipau_aliaksandr.class_work_selenium.locators.shop_by import catalogue_page_locators

class CataloguePage(BasePage):

    def click_show_more(self, index):
        locator = catalogue_page_locators.show_more(index)
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        time.sleep(1)
        elem.click()
        time.sleep(1)


    def click_on_check_box_laptop(self, laptop_name):
        locator = catalogue_page_locators.laptop(laptop_name)
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        elem.click()
        time.sleep(1)

    def fill_price_before(self, price):
        locator = catalogue_page_locators.PRICE_BEFORE
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        elem.send_keys(price)

    def fill_price_after(self, price):
        locator = catalogue_page_locators.PRICE_AFTER
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        elem.send_keys(price)

    def open_diagonal_menu(self):
        elem = self.find_element(catalogue_page_locators.DIAGONAL)
        self.move_to_element_js(web_element=elem, locator=catalogue_page_locators.DIAGONAL)
        elem.click()

    def click_on_checkbox_item(self, item_name):
        locator = catalogue_page_locators.check_box_item(item_name)
        elem = self.find_element(locator)
        self.move_to_element_js(web_element=elem, locator=locator)
        elem.click()

    def click_show(self):
        time.sleep(2)
        self.find_element(catalogue_page_locators.SHOW).click()

    def get_all_elements(self):
        return self.find_elements(catalogue_page_locators.ELEMENTS_ON_CATALOGUE)