from osipau_aliaksandr.class_work_selenium.pages.onliner.home_page import HomePage
from osipau_aliaksandr.class_work_selenium.pages.onliner.product_page import ProductPage
from osipau_aliaksandr.class_work_selenium.pages.onliner.cart_page import CartPage
from osipau_aliaksandr.class_work_selenium.pages.onliner.catalogue_page import CataloguePage
import time

"""
1. Перейти на сайт https://www.onliner.by
2. Ввести в строку поиска iphone 11
3. Переключиться на вкладку барахолка
4. Клинуть на товар
5. Проверить header 
6. Добавить в корзину
7. Перейти в корзину
8. Убедиться, что товар в корзине
9. Удалить товар из корзины
10. Убедиться, что товар удален 
"""

"""
1. Перейти на сайт https://www.onliner.by/
2. Перейти в каталог
3. Выбрать раздел "Авто и мото"
4. Выбрать "Автоакустика"
5. Выбрать "Кабели для автоакустики"
6. Убедиться, что мы находимся в разделе "Автоакустика"
7. Кликнуть на первый товар
8. Добавить товар в корзину
9. Перейти в корзину
10. Проверить наличие товара в корзине
"""

iphone_text = "Apple iPhone"
cable_text = "Кабель"
cables_header_text = "Кабели, адаптеры, разветвители"

def test_input(driver):
    driver.get("https://www.onliner.by/")
    home_page = HomePage(driver)
    home_page.send_text_in_input("iphone11")
    home_page.switch_to_flea_market()
    elements = home_page.get_search_results_from_catalogue()
    home_page.click_on_element_by_index(web_elem_list=elements, index=1)
    home_page.switch_to_current_window()
    product_page = ProductPage(driver)
    headline_text = product_page.get_headline_text()
    assert iphone_text in headline_text
    product_page.click_on_cart_button()
    product_page.click_on_active_cart_button()
    cart_page = CartPage(driver)
    card_text = cart_page.get_card_text()
    assert iphone_text in card_text
    cart_page.delete_product_from_cart()
    cart_page.check_absence_of_element_in_cart()
    assert cart_page.check_absence_of_element_in_cart() is None



def test_catalogue(driver):
    driver.get("https://www.onliner.by/")
    assert driver.current_url == "https://www.onliner.by/"
    home_page = HomePage(driver)
    main_menu = home_page.get_results_from_main_menu()
    home_page.click_on_element_by_index(index=0, web_elem_list=main_menu)
    assert driver.current_url == "https://catalog.onliner.by/"
    catalogue_page = CataloguePage(driver)
    catalogue_categories = catalogue_page.get_results_from_catalogue_categories()
    catalogue_page.click_on_element_by_index(index=5, web_elem_list=catalogue_categories)
    catalogue_page.move_to_car_acoustics()
    catalogue_page.click_on_car_audio_cables()
    catalogue_header_text = catalogue_page.get_header_text()
    assert cables_header_text in catalogue_header_text
    catalogue_products = catalogue_page.get_results_from_catalogue_products()
    catalogue_page.click_on_element_by_index(index=0, web_elem_list=catalogue_products)
    product_page = ProductPage(driver)
    headline_text = product_page.get_headline_text()
    assert cable_text in headline_text
    product_page.click_on_cart_button()
    product_page.click_on_active_cart_button()
    cart_page = CartPage(driver)
    card_text = cart_page.get_card_text()
    assert cables_header_text in card_text


"""
Кандидату нужно решить следующую задачу: разработать решение, используя любой инструмент.

СЦЕНАРИЙ ДОЛЖЕН ВЫПОЛНЯТЬ СЛЕДУЮЩИЕ ДЕЙСТВИЯ:
1. Открыть браузер и развернуть на весь экран.
2. Зайти на onliner.by.
3. Перейти к разделу "Компьютеры" и выбрать "Ноутбуки".
4. Задать параметры поиска:
	- Производитель: Lenovo, Dell, HP
	- Цена: от 1500 руб. до 3000 руб.
	- Размер экрана от 12 до 13.4 дюймов.
5. Нажать кнопку Посмотреть * товаров.
6. Определить количество элементов на странице.
7. Отсортировать список по цене (по возрастанию).
8. Запомнить первый элемент в списке.
9. Отсортировать список по цене в обратном порядке.
10. Убедиться, что сохраненный элемент находится в другом конце списка.
11. Закрыть браузер.

ПРИМЕЧАНИЯ:
Предполагается полная свобода в действиях. При оценке в обязательном порядке будут учитываться:
- работоспособность сценария в браузере на ваш выбор;
- проверки осуществляемые во время выполнения теста;
- гибкость и адаптивность сценария (насколько просто заменить часть входных данных, например, изменить диагональ или цену);
- стабильность работы сценария (обработка ошибок и исключений);
- инструкция/скрипт по настройке запуска тестов

"""

def test_laptops(driver):
    driver.get("https://www.onliner.by/")
    assert driver.current_url == "https://www.onliner.by/"
    home_page = HomePage(driver)
    main_menu = home_page.get_results_from_main_menu()
    home_page.click_on_element_by_index(index=0, web_elem_list=main_menu)
    assert driver.current_url == "https://catalog.onliner.by/"
    catalogue_page = CataloguePage(driver)
    catalogue_categories = catalogue_page.get_results_from_catalogue_categories()
    catalogue_page.click_on_element_by_index(index=1, web_elem_list=catalogue_categories)
    catalogue_page.move_to_laptops_computers_monitors()
    catalogue_page.click_on_laptops()
    assert driver.current_url == "https://catalog.onliner.by/notebook"
    catalogue_page.click_all_options("2")
    catalogue_page.click_laptop_name_checkbox("Haier")
    catalogue_page.click_laptop_name_checkbox("Dell")
    catalogue_page.click_laptop_name_checkbox("HP")
    catalogue_page.click_laptop_name_checkbox("Lenovo")
    catalogue_page.fill_price_from("1500")
    catalogue_page.fill_price_to("3000")
    # catalogue_page.click_show_more_list("1")
    # catalogue_page.select_diagonal("1", "121")
    # catalogue_page.click_show_more_list("2")
    # catalogue_page.select_diagonal("2", "134")
    catalogue_page.click_diagonal_value('менее 13')
    catalogue_page.click_diagonal_value('13-14')
    catalogue_page.get_all_elements_on_page()
    catalogue_page.click_on_sort_menu()
    catalogue_page.select_low_price_sort()
    first_item_name = catalogue_page.first_item_title()
    catalogue_page.click_on_sort_menu()
    catalogue_page.select_high_price_sort()
    catalogue_page.click_on_pagination_dropdown()
    time.sleep(1)
    catalogue_page.last_page_of_catalogue()
    last_item_name = catalogue_page.last_item_title()
    assert first_item_name == last_item_name
    breakpoint()