from selenium.webdriver.common.by import By

MAIN_INPUT = (By.XPATH, '//input[@class="fast-search__input"]')
IFRAME = (By.XPATH, '//iframe[@class="modal-iframe"]')
FLEA_MARKET = (By.XPATH, '//div[contains(text(), "на барахолке")]')
SEARCH_CATALOGUE_RESULTS = (By.XPATH, '//div[@class="search__widget-item"]')
MAIN_MENU = (By.XPATH, '//span[@class="b-main-navigation__text"]')
