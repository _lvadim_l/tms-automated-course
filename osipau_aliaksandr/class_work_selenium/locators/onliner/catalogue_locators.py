from selenium.webdriver.common.by import By

CATALOGUE_CATEGORIES = (By.XPATH, '//li[@class="catalog-navigation-classifier__item "]')
CAR_ACOUSTICS = (By.XPATH, '//div[contains(text(), "Автоакустика")]')
CAR_AUDIO_CABLES = (By.XPATH, '//a[@href="https://catalog.onliner.by/cable?cable_usage%5B0%5D=auto&cable_usage%5Boperation%5D=union"]')
CATALOGUE_HEADER = (By.XPATH, '//h1[@class="schema-header__title"]')
CATALOGUE_PRODUCTS = (By.XPATH, '//div[@class="schema-product__part schema-product__part_1"]')
LAPTOPS_COMPUTERS_MONITORS = (By.XPATH, '//div[contains(text(), "Ноутбуки, компьютеры, мониторы ")]')
LAPTOPS = (By.XPATH, '//a[@href="https://catalog.onliner.by/notebook"]')

def all_options(index):
    return (By.XPATH, f'(//div[@class="schema-filter-control__item"])[{index}]')

def laptop_name(name):
    return (By.XPATH, f'//span[@class="schema-filter__checkbox-text"][contains(text(), "{name}")]')

PRICE_FROM = (By.XPATH, '(//input[@class="schema-filter-control__item schema-filter__number-input schema-filter__number-input_price"])[1]')
PRICE_TO = (By.XPATH, '(//input[@class="schema-filter-control__item schema-filter__number-input schema-filter__number-input_price"])[2]')

# def show_more_list(index):
#     return (By.XPATH, f'(//div[@class="schema-filter-control schema-filter-control_select"])[{index}]')
#
# def diagonal_select(index, value):
#     return (By.XPATH, f'//option[@value="{value}"][{index}]')

def diagonal_select(value):
    return (By.XPATH, f'//span[@class="schema-filter__checkbox-text"][contains(text(), "{value}")]')

ELEMENTS_ON_PAGE = (By.XPATH, '//div[@class="schema-product__part schema-product__part_1"]')

SORT_MENU = (By.XPATH, '//a[@class="schema-order__link"]')
LOW_PRICE_SORT = (By.XPATH, '//span[contains(text(),"Дешевые")]')
FIRST_ITEM_TITLE = (By.XPATH, '//div[@class="schema-product__title"]')
HIGH_PRICE_SORT = (By.XPATH, '//span[contains(text(),"Дорогие")]')
PAGINATION_DROPDOWN = (By.XPATH, '//div[@class="schema-pagination__secondary"]')
PAGES_LIST = (By.XPATH, '//li[@class="schema-pagination__pages-item"]')
LAST_ITEM_TITLE = (By.XPATH, '//div[@class="schema-product__title"]')