from selenium.webdriver.common.by import By
import time


def test_find_element(driver):
   driver.get("https://www.onliner.by/")
   time.sleep(2)
   elem = driver.find_element(By.XPATH, '//input[@class="fast-search__input"]')
   elem.send_keys("iphone 12")
   time.sleep(3)
   iFrame = driver.find_element(By.CLASS_NAME, 'modal-iframe')
   driver.switch_to.frame(iFrame)
   driver.find_element(By.XPATH, '//div[@class="search__tabs-item"][2] ').click()
   time.sleep(3)
   driver.find_element(By.XPATH, '//div[@class="search__widget-item"][3] ').click()
   time.sleep(6)
   driver.find_element(By.ID, "product-compare-control")
   driver.find_element(By.ID, "product-bookmark-control")
   time.sleep(4)
   driver.find_element(By.XPATH, '//a[@class="button-style button-style_adscititious button-style_base-alter product-aside__item-button js-buy-now-button"] ').click()
   time.sleep(5)
   driver.find_element(By.XPATH, '//div[@class="cart-form__title cart-form__title_big-alter cart-form__title_condensed-additional"] ')
   driver.find_element(By.XPATH, '//a[@class="button-style button-style_primary button-style_small cart-form__button"] ').click()
   time.sleep(5)
   # elem1 = driver.find_element(By.XPATH, '//input[@class="auth-input auth-input_primary auth-input_base auth-form__input auth-form__input_width_full"] ')
   # elem1.send_keys("osipovtest41@mail.ru")
   # input = driver.find_element(By.XPATH,
   #                                    "(//input[@class='auth-input auth-input_primary auth-input_base auth-form__input auth-form__input_width_full'])[1]")
   # input.send_keys("test")
   # breakpoint()