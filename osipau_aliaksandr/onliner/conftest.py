import pytest
from selenium import webdriver

@pytest.fixture(scope="function")
def driver():
    print("\nstart browser for test..")
    browser = webdriver.Chrome(executable_path="/Users/mac/tms-automated-course/osipau_aliaksandr/chromedriver")
    browser.maximize_window()
    yield browser
    print("\nquit browser..")
    browser.quit()