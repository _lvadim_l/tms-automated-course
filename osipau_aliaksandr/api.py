import json
import requests

response = requests.get("https://www.nbrb.by/api/exrates/currencies")
print(response.status_code)
# assert response.status_code == 200, f"Failed to request. Response: {response}"
# assert response.status_code == requests.codes.ok, f"Failed to request. Response: {response}"
data = json.loads(response.text)

curr = ["USD", "EUR"]

cur_ids = []

for cur in data:
    if cur["Cur_Abbreviation"] in curr:
        cur_ids.append(cur["Cur_ID"])


for cur_id in cur_ids:
    response2 = requests.get(f"https://www.nbrb.by/api/exrates/rates/{cur_id}")
    if response2.status_code == 200:
        print(json.loads(response2.text)["Cur_OfficialRate"])

curr2 = ["USD", "COP", "MNT", "ESP"]

for cur_abb in data:
    if cur_abb["Cur_Abbreviation"] in curr2:
        print(cur_abb["Cur_Name"], cur_abb["Cur_Name_Bel"], cur_abb["Cur_Name_Eng"])

response3 = requests.get("https://www.nbrb.by/api/exrates/rates/145?ondate=2019-2-6")
print(response3.status_code)
data = json.loads(response3.text)['Cur_OfficialRate']
belki = 100/data
print(belki)

response4 = requests.get("https://www.nbrb.by/api/exrates/rates/145?ondate=2021-6-2")
print(response4.status_code)
data = json.loads(response4.text)['Cur_OfficialRate']
belki2 = 100/data
print(belki2)


