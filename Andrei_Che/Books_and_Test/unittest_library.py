import unittest
from books import Book
from books import Reader
from main_dict import MakeBook

class TestBook(unittest.TestCase):

	'''Testing class book'''

	def test_status(self):	
		boo = Book()
		name_book = '1984'
		self.assertNotEqual(boo.status(),'Книга в наличии')
		
	def test_info(self):
		boo = Book()
		name_book = '1984'
		a = 'Автор: Джордж Оруэлл. Год: 1949. Кол-во страниц: 320'
		self.assertTrue('Автор: Джордж Оруэлл. Год: 1949. Кол-во страниц: 320')
		self.assertFalse(False)

class TestReader(unittest.TestCase):

	'''Testing class Reader'''
	
	def test_book_a_book(self):
		a = 'Автор: Джордж Оруэлл. Год: 1949. Кол-во страниц: 320'
		self.assertEqual ('Автор: Джордж Оруэлл. Год: 1949. Кол-во страниц: 320', a)

if __name__ == '__main__':
    unittest.main()
