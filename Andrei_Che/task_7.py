'''Библиотека
	Создайте класс book с именем книги, автором, кол-м страниц, ISBN, 
флагом, зарезервирована ли книги или нет. Создайте класс пользователь 
который может брать книгу, возвращать, бронировать. Если другой пользователь 
хочет взять зарезервированную книгу(или которую уже кто-то читает - надо ему про 
это сказать).

'''


class Book():
	'''Описание книги'''
	
	def __init__(self,name,autor,pages,flag = True):
		self.name = name
		self.autor = autor
		self.pages = pages
		self.flag = False
				
	def status(self):
		'''Указывается статус'''
		if self.flag == True:
			print("no book")
		else:
			print('the boor is here')	


class Reader(Book):
	'''Описание читателя.'''		
		
	def __init__(self,name,age,flagbook=True):	
		self.age = age
		self.name = name
		self.flagbook = flagbook
		
	def status(self):
		'''Указывается статус'''
		if self.flagbook == False:
			print("no book")
		else:
			print('the boor is here')		


b = Book('Букварь', 'AGL', 33,)
print(b.autor)
print(b.name)
print(b.pages)
b.status()
		
r = Reader('Senya',98,)	
print(r.name)	
print(r.age)
r.status()
	


#myfile=open ('myfile.txt', 'w')
#myfile.write('Кодировки\n')
#myfile.write('Работа с файлами\n')
#myfile.write('Работа с внешними данными: JSON, CSV,Exel,\n')
#a =''
#for line in open('myfile.txt'):
#	a+=line
#print(a)

#myfile.close()   



#with open('test.txt', 'w') as f:
#	f.write (a)
#for line in open('test.txt'):
#	print(line,end='')
#f.close() 


a = 'String'
c = ''
for b in a:
	c+=b*2
e = 'Hello World'
f = ''
for d in e:
	f+=d*2
num = '1234'  
m = ''
for n in num:
	m+=n*4
print(m)
print(f,sep='')
print(c)


#Сложите все числа в списке, они могут быть отрицательными, если список пустой вернуть 0
[] == 0
[1, 2, 3] == 6
[1.1, 2.2, 3.3] == 6.6
[4, 5, 6] == 15
range(101) == 5050

def sum_spis(a = []):
	c = 0
	for b in a:
		c +=b 
	print(c) 
	if a == 0: print('0')


b = range(101)
sum_spis(b)   

a = [15,26,1,45,7,9,6]
sum_spis(a)  

c=[]
sum_spis(c) 
