
class Room ():
	'''Площадь комнаты минус окна и двери.'''
	
	def __init__(self):
		'''Ввод необходимых данных.'''
		self.x1 = float(input("Длина стен: "))
		self.y1 = float(input("Ширина стен: "))
		self.z1 = float(input("Высота стен: "))
		self.x = float(input("Длина проемов (окно+дверь): "))
		self.y = float(input("Ширина проемов (окно+дверь): "))

	
	def squareWD(self):
		'''Вычисление общей площади дверей и окон.'''
		self.squareWD = self.x * self.y 
		print("Размеры, которые надо вычесть: ", self.squareWD)	
		return self.squareWD	
			
	def square(self):
		'''Общий размер комнаты'''
		self.square = 2 * self.z1 * (self.x1 + self.y1)
		print("Размеры стен с проемами: ", self.square)	
		
	def total_square(self):
		'''Итоговый размер под обои'''
		self.total = self.square - self.squareWD
		print("Размеры стен минус проемы: ",self.total)
		
	def wallpaper(self):
		'''Количество обоев.'''
		self.paper = (self.y1+self.x1)/0.53  # для рулона стандартной ширины.
		print("Необходимое кол_во полос: ",(self.paper))
		print("Необходимое кол_во катушек: ", round(self.paper/4))# по 4 полосы в катушке



my_room = Room()
my_room.squareWD()
my_room.square()
my_room.total_square()
my_room.wallpaper()
