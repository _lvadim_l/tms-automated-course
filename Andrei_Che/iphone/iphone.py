import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


driver = webdriver.Chrome()
driver.maximize_window()
driver.get("http://www.onliner.by/")
elem = driver.find_element_by_class_name('fast-search__input')
elem.send_keys("iphone12")
iFrame = driver.find_element_by_class_name('modal-iframe')
driver.switch_to.frame(iFrame)
driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div/div/div[3]').click()
WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div[2]/div[1]/div/div/div[7]/a"))).click()
time.sleep(20)
driver.quit()

