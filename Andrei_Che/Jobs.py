#BOSS-tack.
#Сроки выполнения:  до 29.03.21 Навыки: код + логика
#Задание: Есть загадка как говорится "от Стива Джобса". Имеется 2 ведра, одно 3 литра другое 5 литров и неограниченное количество воды. Пользователь может
#1. Набирать воду в любое ведро. 2. Выливать воду из любого ведра.
#3. Переливать воду из одного ведра в другое.
#Пользователь побеждает если ему удаётся получить ровно 4 литра.
#Реализация:
#Всё происходит в цикле.
#Пользователь вводит команду, например "fill_up, bucket_5" программа пополняет ведро.
#За один оборот цикла одно действие.
#Пользователь может выйти из программы (цикла).
#Каждую итерацию(оборот) нам выводяться данные по нашим вёдрам.
#Программа работает до тех пор пока пользователь не получит 4 литра либо не выйдет.
#В вёдрах не может быть отрицательного количества воды.
#В вёдрах не может быть больше литров чем предусмотрено конструкцией вёдер (3л и 5л).
#Используйте только то что мы проходили на занятиях.
#Бонусом:1. Добавить счётчик ходов и решить загадку за их минимальное количество.
#2. Приятный и понятный интерфейс, то есть вывод какого-то меню, показателей вёдер и тд.
#3. Защита от дурака, что бы пользователь не ввёл - программа не должна падать с ошибкой или выдавать неправильный результат.

#import prettytable

inst_one = "Наполнить ведро: f"
inst_two = "Перелить в другое ведро: d"
inst_three = "Вылить ведро: s"
inst_four = "Выход из цикла: q"
print("\tThe buckets.")
print("Есть два ведра (3л и 5л) и бесконечный источник воды.\nКак получить 4 литра воды?\n")
print(f"{inst_four}\n{inst_three}\n{inst_two}\n{inst_one}\n")
buc_3=0
buc_5=0
while buc_5 != 4:
	print(f"\nBедро 3л: {buc_3} ведро 5л: {buc_5}")
	cur_buck = input('Bыбрать ведро(3\\5\\q):')
	if cur_buck == 'q':
		break
	print(f"выбрано ведро: {cur_buck}")
	cmd = input("выбор действия(f\\d\\s\\q):")
	if cur_buck == '3':
		if cmd == 'f':
			buc_3=3
		if cmd == 'd':
			if buc_3==3 and buc_5==5:
				print("\tПеребор")
			if buc_3==3 and buc_5==4:
				buc_3-=1
				buc_5+=1
			if buc_3==3 and buc_5==3:
				buc_3-=2
				buc_5+=2
			if buc_3==3 and buc_5==2:
				buc_3=0
				buc_5+=3	
			if buc_3==3 and buc_5==1:
				buc_3=0
				buc_5+=3
			if buc_3==3 and buc_5==0:
				buc_3=0
				buc_5+=3			
			if buc_3==2 and buc_5==4:
				print("\tМимо!")
			if buc_3==2 and buc_5==5:
				print("\tОпять не то!")	
			if buc_3==2 and buc_5==3:
				buc_5+=2
				buc_3=0
			if buc_3==2 and buc_5==2:
				buc_5+=2
				buc_3=0
			if buc_3==2 and buc_5==1:
				buc_5+=2
				buc3=0
			if buc_3==2 and buc_5==0:
				buc_5+=2
				buc_3=0			
			if buc_3==1 and buc_5==0:
				buc_5+=1
				buc_3=0
			if buc_3==1 and buc_5==1:
				buc_5+=1
				buc_3=0
			if buc_3==1 and buc_5==2:
				buc_5+=1
				buc_3=0				
			if buc_3==1 and buc_5==3:
				buc_5+=1
				buc_3=0				
			if buc_3==1 and buc_5==4:
				buc_5+=1
				buc_3=0	
			if buc_3==0:
				print("\tВедро 3 пусто!")	
				continue	
		if cmd == 's':
			buc_3=0
		if cmd == 'q':
			break
	if cur_buck == '5':   
		if cmd == 'f':
			buc_5=5
		if cmd == 'd':
			if buc_5==2 and buc_3==2:
				print("\tПеребор!")
				continue
			if buc_5==2 and buc_3==1:
				buc_3+=2
				buc_5=0	
			if buc_5==2 and buc_3==3:
				print("\t!")
				continue
			if buc_5==2 and buc_3==0:
				buc_3+=2
				buc_5=0			
			if buc_5==3 and buc_3==0:
				buc_3+=3
				buc_5=0
			if buc_5==3 and buc_3==1:
				print("\tНе то!")
				continue
			if buc_5==3 and buc_3==2:
				print("\tНе то пальто!")
				continue	
			if buc_5==1 and buc_3==0:
				buc_3+=1
				buc_5=0
			if buc_5==1 and buc_3==1:
				buc_3+=1
				buc_5=0	
			if buc_5==1 and buc_3==2:
				buc_3+=1
				buc_5=0
			if buc_5==1 and buc_3==3:
				print("\t Неа")
			if buc_5==5 and buc_3==0:
				buc_3=3
				buc_5=2	
			if buc_5==5 and buc_3==1:
				buc_3+=2
				buc_5=3
			if buc_5==5 and buc_3==2:
				buc_5-=1
				buc_3=3
				print("Победа!")
				break
			if buc_5==5 and buc_3==3:
				print("\tТам уже все занято")	
			if buc_5==0:
				print("\tВедро 5 пусто")
				continue			
		if cmd == 's':
			buc_5=0
		if cmd == 'q':
			break   
	if cmd == 'q':
		break
else:
     print("Вы победили!") 
