import json
import requests

# Получить список иностранных валют у НБРБ
response = requests.get("https://www.nbrb.by/api/exrates/currencies")
data = json.loads(response.text)
print(data)

# Напечатать курс USD и EUR к BYN
response = requests.get("https://www.nbrb.by/api/exrates/rates/145")  # USD
data = json.loads(response.text)['Cur_OfficialRate']
print("Курс $ на сегодня: ", data)
print("\n")	
response2 = requests.get("https://www.nbrb.by/api/exrates/rates/EUR?parammode=2")  # EUR
data2 = json.loads(response2.text)['Cur_OfficialRate']
print("Курс EUR на сегодня: ", data2)

# Напечатать только наименования валют USD, COP, MNT, ESP(на русском, белорусском и английском языках)
print("\n")
response = requests.get("https://www.nbrb.by/api/exrates/currencies")
data = json.loads(response.text)
curr = ["USD", "COP", "MNT", "ESP"]
cur_ids = []
for cur in data:
	if cur["Cur_Abbreviation"] in curr:
		cur_ids.append(cur["Cur_Name_Eng"])
print(cur_ids)
print("\n")
for cur in data:
	if cur["Cur_Abbreviation"] in curr:
		cur_ids.append(cur["Cur_Name_Bel"])
print(cur_ids)
print("\n")
for cur in data:
	if cur["Cur_Abbreviation"] in curr:
		cur_ids.append(cur["Cur_Name"])
print(cur_ids)




