import time

print("\tThe Collatz problem.\n")
def collatz(bb=range(1,1000001)):
	'''
	The Collatz problem from 1 to million
	
	'''
	a = []
	b = list(bb)
	start = time.time()
	for n in b:
		while n!=1:
			if n%2==0:
				n=n/2
			else:
				n=n*3+1
			#a.append(n)	
	print("Total time: ",time.time() - start)  	
	#print(f"Max. chain: {max(a)}")
collatz()

print("\n\tFibonacci numbers")
def fib(n):
	'''
	Fibonacci numbers.
	
	'''
	if (n <= 1): return n
	else: return (fib(n-1) + fib(n-2))
n = int(input("Number :"))
print("The Fibonacci sequence: ")
for i in range(n): print(fib(i))

