import pytest
from selenium import webdriver



@pytest.fixture()
def driver_chrome():
    print("\nstart browser for test..")
    driver = webdriver.Chrome(executable_path="/home/nmelnik/tms-automated-course/chromedriver")
    driver.maximize_window()
    yield driver
    print("\nquit browser..")
    driver.quit()
