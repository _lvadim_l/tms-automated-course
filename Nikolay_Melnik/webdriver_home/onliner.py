from selenium.webdriver.common.by import By
import time


def test_search(driver_chrome):
    driver_chrome.get("https://www.onliner.by/")
    element = driver_chrome.find_element_by_xpath('//input[@class="fast-search__input"]')
    element.send_keys('iphone11')
    time.sleep(3)
    iFrame = driver_chrome.find_element(By.CLASS_NAME, 'modal-iframe')
    driver_chrome.switch_to.frame(iFrame)
    driver_chrome.find_element_by_xpath('//div[@class="search__tabs-item"][2]').click()
    time.sleep(2)
    driver_chrome.find_element_by_xpath('//div[@class="baraholka__title"]').click()
    time.sleep(2)
    driver_chrome.find_element_by_xpath('//div[@class="m-title"]/h1[@title="Оригинальные, Новые Apple iPhone / Watch / MacBook / AirPods..."]')
