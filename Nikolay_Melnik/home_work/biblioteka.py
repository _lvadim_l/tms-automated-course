book_storage = {}
class Book:
    def __init__(self,book_name,author,pages,isbn,reserved=False):
        self.book_name = book_name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.reserved = reserved

        book_storage[self.book_name] = {
            "author": self.author,
            "pages": self.pages,
            "isbn": self.isbn,
            "reserved": self.reserved}



class User:
    def check_reservation(self, book_name):
        if book_storage[book_name]["reserved"] is True:
            return True
        else:
            return False
    def take_book(self, book_name):
        if self.check_reservation(book_name) is True:
            print("The book is in use or reserved")
        else:
            book_storage[book_name]["reserved"] = True
            print("Book is yours")

    def give_back(self,book_name):
        book_storage[book_name]["reserved"] = False

    def reserve(self, book_name):
        if self.check_reservation(book_name) is True:
            print("The book is in use or reserved")
        else:
            book_storage[book_name]["reserved"] = True
            print("Book is reserved by you")


Book("книга1", "Стивен Хокинг", "174", "978-5-17-102280-8")
Book("книга2", "Стивен Хокинг", "174", "978-5-17-102280-8")
Book("книга3", "Стивен Хокинг", "174", "978-5-17-102280-8")

user = User()
user.take_book("книга1")
user.reserve("книга2")
user.give_back("книга1")





