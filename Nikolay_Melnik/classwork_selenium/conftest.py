import pytest
from selenium import webdriver

@pytest.fixture()
def driver():
    print("\nstart browser for test..")
    browser = webdriver.Chrome(executable_path='/home/user/tms-automated-course/Nikolay_Melnik/chromedriver_linux64/chromedriver')
    browser.maximize_window()
    yield browser
    print("\nquit browser..")
    browser.quit()