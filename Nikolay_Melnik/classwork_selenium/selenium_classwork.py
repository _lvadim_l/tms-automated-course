from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC



def test_alert(browser):
    browser.get("https://www.testandquiz.com/selenium/testing.html")
    button = WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Generate Confirm Box')]"))
    ).click()
    alert = browser.switch_to.alert
    alert.accept()
    succes_text = WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.XPATH, "//p[@id='demo']")))
    assert succes_text.text == "You pressed OK!"


