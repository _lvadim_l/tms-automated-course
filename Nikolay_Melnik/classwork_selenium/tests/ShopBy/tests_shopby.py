from Nikolay_Melnik.classwork_selenium.pages.ShopBy.home_page import HomePage
from Nikolay_Melnik.classwork_selenium.pages.ShopBy.catalog_page import CatalogPage



def test_shop_by(driver):
    driver.get("https://shop.by/")
    home_page = HomePage(driver)
    home_page.move_to_catalog()
    home_page.move_to_section_computers()
    home_page.move_to_section_laptops_and_accessories()
    home_page.click_on_laptops()
    catalog_page = CatalogPage(driver)
    catalog_page.click_show_more("1")
    catalog_page.click_on_chekbox_laptop("HP")
    catalog_page.click_on_chekbox_laptop("Dell")
    catalog_page.click_on_chekbox_laptop("Lenovo")
    catalog_page.fill_price_before("1500")
    catalog_page.fill_price_after("3000")
    catalog_page.open_diagonal_menu()
    catalog_page.click_show_more("4")
    catalog_page.click_on_checkbox_name("12.5")
    catalog_page.click_on_checkbox_name("13.3")
    catalog_page.click_show()
    all_elements_on_page = catalog_page.get_all_elements()
    assert len(all_elements_on_page) == 48

    breakpoint()


