from Nikolay_Melnik.classwork_selenium.pages.onliner.home_page import HomePage
from Nikolay_Melnik.classwork_selenium.pages.onliner.product_page import Product_page
from Nikolay_Melnik.classwork_selenium.pages.onliner.cart_page import CardPage
from Nikolay_Melnik.classwork_selenium.pages.onliner.catalog_page import CatalogPage
import time

"""
1. Перейти на сайт https://www.onliner.by/
2 Ввести в строку поиска iphone12
3 Переключиться на барахолку
4 Кликнуть на товар
5 Убедиться, что находимся на нужной странице барахолки
6 Добавить в корзину
7 Перейти в корзину
8 Убедиться, что товар в корзине
"""
iphone_text = "Apple iPhone"
cable_text = "Кабели, адаптеры, разветвители"
def test_input(driver):
    driver.get("https://www.onliner.by/")
    home_page = HomePage(driver)
    home_page.send_text_in_input("iphone12")
    home_page.switch_to_flea_market()
    elements = home_page.get_search_results_from_catalog()
    home_page.click_on_element_by_index(index=0, web_elem_list=elements)
    home_page.switch_to_current_window()
    product_page = Product_page(driver=driver)
    headline_text = product_page.get_headline_text()
    assert iphone_text in headline_text
    product_page.click_on_cart_button()
    product_page.click_on_active_cart_button()
    cart_page = CardPage(driver=driver)
    card_text = cart_page.get_card_text()
    assert iphone_text in card_text
    cart_page.delete_product_from_card()
    assert cart_page.check_absence_of_element() is None

def test_car_moto(driver):
    driver.get("https://www.onliner.by/")
    home_page = HomePage(driver)
    home_page.click_on_catalog_ref()
    catalog_page = CatalogPage(driver)
    catalog_page.expand_auto_moto()
    catalog_page.car_acoustics()
    catalog_page.car_acoustics_cable()
    catalog_page.cable_title_check()
    time.sleep(3)
    catalog_page.click_on_first_cable_offer()
    product_page = Product_page(driver)
    product_page.click_on_cart_button()
    product_page.click_on_active_cart_button()
    cart_page = CardPage(driver)
    cart_text = cart_page.get_cable_card_text()
    assert cable_text in cart_text






