import time

from Nikolay_Melnik.classwork_selenium.base_page import BasePage
from Nikolay_Melnik.classwork_selenium.locators.ShopBy import catalog_page_locators

class CatalogPage(BasePage):

    def click_show_more(self, index):
        locator = catalog_page_locators.show_more(index)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()

    def click_on_chekbox_laptop(self, name):
        locator = catalog_page_locators.laptop(name)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()

    def fill_price_before(self, price):
        locator = catalog_page_locators.PRICE_BEFORE
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.send_keys(price)

    def fill_price_after(self, price):
        locator = catalog_page_locators.PRICE_AFTER
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.send_keys(price)

    def open_diagonal_menu(self):
        elem = self.find_element(catalog_page_locators.DIAGONAL)
        self.move_to_element_with_js(locator=catalog_page_locators.DIAGONAL, web_element=elem)
        elem.click()

    def click_on_checkbox_name(self,item_name):
        locator =catalog_page_locators.checkbox_item(item_name)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()

    def click_show(self):
        time.sleep(2)
        self.find_element(catalog_page_locators.SHOW).click()

    def get_all_elements(self):
        return self.find_elements(catalog_page_locators.ELEMENTS_ON_CATALOG)



