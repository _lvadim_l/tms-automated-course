from Nikolay_Melnik.classwork_selenium.base_page import BasePage
from Nikolay_Melnik.classwork_selenium.locators.onliner import catalog_page_locators


class CatalogPage(BasePage):

    def expand_auto_moto(self):
        self.find_element(catalog_page_locators.AUTO_MOTO).click()

    def car_acoustics(self):
        self.find_element(catalog_page_locators.CAR_ACOUSTICS).click()

    def car_acoustics_cable(self):
        self.find_element(catalog_page_locators.CAR_ACOUSTICS_CABLE).click()

    def cable_title_check(self):
        self.find_element(catalog_page_locators.CABLE_TITLE)

    def click_on_first_cable_offer(self):
        self.find_element(catalog_page_locators.FIRST_CABLE_OFFER).click()