from Nikolay_Melnik.classwork_selenium.base_page import BasePage
from Nikolay_Melnik.classwork_selenium.locators.onliner import product_page_locators
import time


class Product_page(BasePage):

    def get_headline_text(self) -> str:
        element = self.find_element(product_page_locators.HEAD_LINE_IN_CATALOG)
        time.sleep(0.5)
        return element.text

    def click_on_cart_button(self):
        self.find_element(product_page_locators.ADD_TO_CART).click()

    def click_on_active_cart_button(self):
        self.find_element(product_page_locators.IN_CART_ACTIVE_BUTTON).click()