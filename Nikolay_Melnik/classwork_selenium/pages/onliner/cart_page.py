import time

from Nikolay_Melnik.classwork_selenium.base_page import BasePage
from Nikolay_Melnik.classwork_selenium.locators.onliner import cart_locators


class CardPage(BasePage):

    def get_card_text(self):
        elem = self.find_element(cart_locators.PRODUCT_CARD)
        time.sleep(0.5)
        return elem.text

    def delete_product_from_card(self):
        trash = self.find_element(cart_locators.DELETE_BUTTON)
        self.move_to_element(trash)
        self.find_element(cart_locators.DELETE_BUTTON).click()

    def check_absence_of_element(self):
        self.check_absence_element(cart_locators.ORDER_PROCESSING)

    def get_cable_card_text(self):
        elem = self.find_element(cart_locators.CABLE_PRODUCT_CARD)
        return elem.text


