from selenium.webdriver.common.by import By

AUTO_MOTO = (By.XPATH, '//ul/li[@class="catalog-navigation-classifier__item "][6]')
CAR_ACOUSTICS = (By.XPATH, '//div[contains (text(), "Автоакустика")]')
CAR_ACOUSTICS_CABLE = (By.XPATH, '//a[contains (@href, "https://catalog.onliner.by/cable?cable_usage%5B0%5D=auto&cable_usage%5Boperation%5D=union")]')
CABLE_TITLE = (By.XPATH, '//h1[text()="Кабели, адаптеры, разветвители"]')
FIRST_CABLE_OFFER = (By.XPATH, '//div[@class="schema-product__title"]/a')
