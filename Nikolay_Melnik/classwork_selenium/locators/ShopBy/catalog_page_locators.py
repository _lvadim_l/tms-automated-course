from selenium.webdriver.common.by import By

def show_more(index):
    return (By.XPATH, f'(//span[@class="ModelFilter__OpenHideAttrTxt Page__DarkDotLink"])[{index}]')

def laptop(name):
    return (By.XPATH, f'//label[@data-ga="{name}"]')

PRICE_BEFORE = (By.XPATH, '//input[@name="price_before"]')
PRICE_AFTER = (By.XPATH, '//input[@name="price_after"]')
DIAGONAL = (By.XPATH, '//span[@data-id="prof_5828"]')

def checkbox_item(item_name):
    return (By.XPATH, f'//label[@data-ga="{item_name}"]')

SHOW = (By.XPATH, '//span[@data-ga="podbor"]')

ELEMENTS_ON_CATALOG = (By.XPATH, '//div[@class="ModelList__InfoModelBlock ModelList__DescModelBlock"]')
