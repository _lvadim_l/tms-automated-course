import pytest
from selenium import webdriver

@pytest.fixture()
def browser():
    print("\nstart browser for test..")
    browser = webdriver.Chrome(executable_path='/home/user/tms-automated-course/Nikolay_Melnik/chromedriver')
    browser.maximize_window()
    yield browser
    print("\nquit browser..")
    browser.quit()

@pytest.fixture()
def create_room():
    room = Room(x=6,y=7,z=8)
    yield room
    print("end test")

