from math import ceil


class Room:

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.wd = []

    def addWD(self, w, h, name='unk'):
        self.wd.append(WinDoor(w, h, name))

    def workSurface(self):
        new_square = Room.self_square(self)
        for i in self.wd:
            new_square -= i.square
        return new_square

    def self_square(self):
        self.square = 2 * self.z * (self.x + self.y)
        return self.square

    def wallpaper(self, x, y):
        num = (Room.workSurface(self)) / (x * y)
        return ceil(num)


class WinDoor:
    def __init__(self, x, y, name="unk"):
        self.x = x
        self.y = y
        self.name = name
        self.square = x * y

    def __repr__(self):
        return f'{self.name} {self.x}x{self.y}'


def is_there(wd):
    while wd == 'Y':
        x_wd = int(input('введите ширину окна/двери >> '))
        y_wd = int(input('введите высоту окна/двери >> '))
        wd = input('еще есть есть окна или двери? Y / N >> ')
        return r1.addWD(x_wd, y_wd)
    else:
        return


x = int(input('введите длинну комнаты >> '))
y = int(input('введите ширину комнаты >> '))
z = int(input('введите высоту комнаты >> '))
r1 = Room(x, y, z)
wd = input('в комнате есть окна или двери? Y / N >> ')
is_there(wd)
wpp1 = float(input('введите ширину рулона обоев >> '))
wpp2 = float(input('введите длинну рулона обоев >> '))
print(r1.wallpaper(wpp1, wpp2))



