from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    "http://slinky.me/uploads/pic/8/tumblr_nbw9c3BDsM1tej061o1_400.gif",
    "https://31.media.tumblr.com/2c83ae7daf5fb09122205bf96826501e/tumblr_n32nwqgpRY1twjuhfo1_500.gif,",
    "https://25.media.tumblr.com/c02cd5c85f456341bdcdf780689aad96/tumblr_mgoqp3UW4v1rkfpt8o1_500.gif",
    "https://66.media.tumblr.com/6d70b894ebc631946e9b70f0d2208984/7465bfc205fe4ad6-e7/s500x750/9b1fa02b76751a09312d3a1a33ed693bb604fbc8.gif,"
    "https://37.media.tumblr.com/48de2fbf3a0c3079f42c8945b35c3558/tumblr_n72jenAoND1t3gucoo1_500.gif"
    ]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
