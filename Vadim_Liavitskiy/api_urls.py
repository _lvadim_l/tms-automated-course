BASE_URL = "https://petstore.swagger.io/v2"

#USER
CREATE_USER = BASE_URL + "/user"

USER_NAME = BASE_URL + "/user/{username}"

USER_LOGIN = BASE_URL + "/user/login"

USER_LOGOUT = BASE_URL + "/user/logout"

#PET STORE
PET_STORE = BASE_URL + "/store/inventory"
PET_STORE_ORDER = BASE_URL + "/store/order"
PET_STORE_ORDER_ID = BASE_URL + "/store/order/{orderId}"


# PETS
PET = BASE_URL + "/pet"

PET_ID = BASE_URL + "/pet/{petId}"
