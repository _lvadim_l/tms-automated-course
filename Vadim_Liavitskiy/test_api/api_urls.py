BASE_URL = "https://petstore.swagger.io/v2"

# USERS
USER = BASE_URL + "/user"

USER_NAME = BASE_URL + "/user/{username}"

LOGIN = BASE_URL + "/user/login"

LOGOUT = BASE_URL + "/user/logout"
