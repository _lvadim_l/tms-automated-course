import time

from Vadim_Liavitskiy.class_work_selenium.base_page import BasePage
from Vadim_Liavitskiy.class_work_selenium.locators.ShopBy import catalog_page_locators

class CatalogPage(BasePage):

    def click_show_more(self, index):
        locator = catalog_page_locators.show_more(index)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()

    def click_on_check_box_laptop(self, laptop_name):
        locator = catalog_page_locators.laptop(laptop_name)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()
        time.sleep(0.5)

    def fill_price_before(self, price):
        locator = catalog_page_locators.PRICE_BEFORE
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.send_keys(price)

    def fill_price_after(self, price):
        locator = catalog_page_locators.PRICE_AFTER
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.send_keys(price)

    def open_diagonal_menu(self):
        elem = self.find_element(catalog_page_locators.DIAGONAL)
        locator = catalog_page_locators.DIAGONAL
        self.move_to_element_with_js(web_element=elem, locator=locator)
        elem.click()

    def click_on_checkbox_item(self, item_name):
        locator = catalog_page_locators.check_box_item(item_name)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()

    def click_show(self):
        time.sleep(2)
        self.find_element(catalog_page_locators.SHOW).click()

    def get_all_elements(self):
        return self.find_elements(catalog_page_locators.ELEMENTS_ON_CATALOG)

    def click_on_filter_by_name(self, name):
        self.find_element(catalog_page_locators.filter(name))



