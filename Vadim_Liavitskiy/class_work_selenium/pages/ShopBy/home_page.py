import time

from Vadim_Liavitskiy.class_work_selenium.base_page import BasePage
from Vadim_Liavitskiy.class_work_selenium.locators.ShopBy import home_page_locators


class HomePage(BasePage):

    def move_to_catalog(self):
        elem = self.find_element(home_page_locators.CATALOG)
        self.move_to_element(elem)

    def move_to_section_computers(self):
        elem = self.find_element(home_page_locators.COMPUTERS)
        self.move_to_element(elem)

    def move_to_section_laptops_and_accessories(self):
        elem = self.find_element(home_page_locators.LAPTOPS_AND_ACCESSORIES)
        self.move_to_element(elem)

    def click_on_laptops(self):
        elem = self.find_element(home_page_locators.LAPTOPS)
        time.sleep(0.5)
        self.click_with_javascript(elem)


