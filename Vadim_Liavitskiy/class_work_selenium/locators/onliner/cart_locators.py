from selenium.webdriver.common.by import By

PRODUCT_CARD = (By.XPATH, '//div[@class="cart-form__offers-unit cart-form__offers-unit_primary"]')

PRICE = (By.XPATH, '//div[@class="cart-form__description cart-form__description_primary cart-form__description_base-alter cart-form__description_font-weight_semibold cart-form__description_ellipsis cart-form__description_condensed-another"]')

DELETE_BUTTON = (By.XPATH, '//a[@class="button-style button-style_auxiliary button-style_small cart-form__button '
                           'cart-form__button_remove"]')

ORDER_PROCESSING = (By.XPATH, '//div[@class="cart-form__offers-item cart-form__offers-item_additional"]')