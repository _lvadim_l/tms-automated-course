from selenium.webdriver.common.by import By

def show_more(index):
    return (By.XPATH,
            f'(//span[@class="ModelFilter__OpenHideAttrTxt Page__DarkDotLink"])[{index}]')

def laptop(name):
    return (By.XPATH, f'//label[@data-ga="{name}"]')

PRICE_BEFORE = (By.XPATH, f'//input[@name="price_before"]')

PRICE_AFTER = (By.XPATH, f'//input[@name="price_after"]')

DIAGONAL = (By.XPATH, "//span[contains(text(), 'Диагональ экрана')]")

def check_box_item(item_name):
    return (By.XPATH, f'//label[@data-ga="{item_name}"]')

SHOW = (By.XPATH, '//span[@data-ga="podbor"]')

ELEMENTS_ON_CATALOG =(By.XPATH, '//div[@class="ModelList__InfoModelBlock ModelList__DescModelBlock"]')

def filter(name):
    return (By.XPATH, f'//span[contains(text(), "{name}")]')