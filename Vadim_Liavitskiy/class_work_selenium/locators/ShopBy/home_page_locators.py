from selenium.webdriver.common.by import By

CATALOG = (By.XPATH, '//span[@class="Header__ButtonCatalog Header__CatalogDescShow visible-lg"]')

COMPUTERS = (By.XPATH, '//a[@href="/kompyutery/"]')

LAPTOPS_AND_ACCESSORIES = (By.XPATH, '//a[@href="/kompyutery/noutbuki_i_aksessuari/"]')

LAPTOPS = (By.XPATH, '(//span[@data-link="/noutbuki/"])[1]')
