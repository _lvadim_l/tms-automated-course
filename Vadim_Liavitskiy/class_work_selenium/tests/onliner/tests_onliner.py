from Vadim_Liavitskiy.class_work_selenium.pages.onliner.home_page import HomePage
from Vadim_Liavitskiy.class_work_selenium.pages.onliner.product_page import ProductPage
from Vadim_Liavitskiy.class_work_selenium.pages.onliner.cart_page import CartPage
import allure


"""
1. Перейти на сайт https://www.onliner.by/
2. Ввести в строку поиска iphone11
3. Переключиться на барахолку
4. Кликнуть на товар
5. Проверить header
6. Добавить товар в корзину
7. Перейти в корзину
8. Проверить наличие товара в корзине
"""

iphone_text = "Apple iPhone"

@allure.story('Add product to cart')
def test_input(driver):
    with allure.step('Open onliner home page'):
        driver.get("https://www.onliner.by/")
        home_page = HomePage(driver=driver)
    with allure.step('Search iphone11'):
        home_page.send_text_in_input("iphone11")
        home_page.switch_to_flea_market()
        elements = home_page.get_search_results_from_catalog()
    with allure.step('Go to product'):
        home_page.click_on_element_by_index(web_elem_list=elements, index=0)
        home_page.switch_to_current_window()
        product_page = ProductPage(driver=driver)
        headline_text = product_page.get_headline_text()
    with allure.step('Add product to cart'):
        assert iphone_text in headline_text
        product_page.click_on_cart_button()
        product_page.click_on_active_cart_button()
        cart_page = CartPage(driver=driver)
        card_text = cart_page.get_card_text()
        assert iphone_text in card_text
        cart_page.delete_product_from_cart()
        assert cart_page.check_absence_of_element_in_cart() is None

@allure.feature('Add product to cart')
def test_input2(driver):
    with allure.step('Open onliner home page'):
        driver.get("https://www.onliner.by/")
        home_page = HomePage(driver=driver)
    with allure.step('Search iphone 11 '):
        home_page.send_text_in_input("iphone11")
        home_page.switch_to_flea_market()
        elements = home_page.get_search_results_from_catalog()
    with allure.step('Go to product'):
        home_page.click_on_element_by_index(web_elem_list=elements, index=0)
        home_page.switch_to_current_window()
        product_page = ProductPage(driver=driver)
        headline_text = product_page.get_headline_text()
        assert iphone_text in headline_text
    with allure.step('Add product in cart'):
        product_page.click_on_cart_button()
        product_page.click_on_active_cart_button()
        cart_page = CartPage(driver=driver)
        card_text = cart_page.get_card_text()
        assert iphone_text in card_text
        cart_page.delete_product_from_cart()
        assert cart_page.check_absence_of_element_in_cart() is None


@allure.story('Onliner products')
def test_input3(driver):
    with allure.step('Open onliner home page'):
        driver.get("https://www.onliner.by/")
        home_page = HomePage(driver=driver)
    with allure.step('Search iphone 11 '):
        home_page.send_text_in_input("iphone11")
        home_page.switch_to_flea_market()
        elements = home_page.get_search_results_from_catalog()
    with allure.step('Go to product'):
        home_page.click_on_element_by_index(web_elem_list=elements, index=0)
        home_page.switch_to_current_window()
        product_page = ProductPage(driver=driver)
        headline_text = product_page.get_headline_text()
        assert iphone_text in headline_text

@allure.story('Onliner products')
def test_input4(driver):
    with allure.step('Open onliner home page'):
        driver.get("https://www.onliner.by/")
        home_page = HomePage(driver=driver)
    with allure.step('Search iphone 11 '):
        home_page.send_text_in_input("iphone11")
        home_page.switch_to_flea_market()
        elements = home_page.get_search_results_from_catalog()
    with allure.step('Go to product'):
        home_page.click_on_element_by_index(web_elem_list=elements, index=0)
        home_page.switch_to_current_window()
        product_page = ProductPage(driver=driver)
        headline_text = product_page.get_headline_text()
        assert iphone_text in headline_text
