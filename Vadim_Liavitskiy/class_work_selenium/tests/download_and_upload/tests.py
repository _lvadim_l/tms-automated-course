from Vadim_Liavitskiy.class_work_selenium.pages.downloads_and_uploads.home_page import Files
import time

import requests

file_name = "JONATHAN MBIRIRI GICHOHI.pdf"

file_path = "/home/vadim/tms-automated-course/README.md"

header_text = "File Uploaded!"

def test_download(driver):
    driver.get("http://the-internet.herokuapp.com/download")
    files = Files(driver)
    files.click_on_file(file_name)
    time.sleep(10)
    session_id = driver.session_id
    response = requests.get(f"http://localhost:4444/download/{session_id}/{file_name}")
    assert response.status_code == 200


def test_upload(driver):
    driver.get("http://the-internet.herokuapp.com/upload")
    files = Files(driver)
    files.upload_file(file_path)
    files.click_on_submit_button()
    header = files.get_header_text()
    assert header == header_text
    breakpoint()
