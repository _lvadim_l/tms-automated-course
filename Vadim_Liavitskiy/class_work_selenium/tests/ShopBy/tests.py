"""
Кандидату нужно решить следующую задачу: разработать решение, используя любой инструмент.

СЦЕНАРИЙ ДОЛЖЕН ВЫПОЛНЯТЬ СЛЕДУЮЩИЕ ДЕЙСТВИЯ:
1. Открыть браузер и развернуть на весь экран.
2. Зайти на shop.by.
3. Перейти к разделу "Компьютеры" и выбрать "Ноутбуки".
4. Задать параметры поиска:
	- Производитель: Lenovo, Dell, HP
	- Цена: от 1500 руб. до 3000 руб.
	- Размер экрана от 12 до 13.4 дюймов.
5. Нажать кнопку Посмотреть * товаров.
6. Определить количество элементов на странице.
7. Отсортировать список по цене (по возрастанию).
8. Запомнить первый элемент в списке.
9. Отсортировать список по цене в обратном порядке.
10. Убедиться, что сохраненный элемент находится в другом конце списка.
11. Закрыть браузер.

ПРИМЕЧАНИЯ:
Предполагается полная свобода в действиях. При оценке в обязательном порядке будут учитываться:
- работоспособность сценария в браузере на ваш выбор;
- проверки осуществляемые во время выполнения теста;
- гибкость и адаптивность сценария (насколько просто заменить часть входных данных, например, изменить диагональ или цену);
- стабильность работы сценария (обработка ошибок и исключений);
- инструкция/скрипт по настройке запуска тестов"""


from Vadim_Liavitskiy.class_work_selenium.pages.ShopBy.home_page import HomePage
from Vadim_Liavitskiy.class_work_selenium.pages.ShopBy.catalog_page import CatalogPage

def test_shop_by(driver, start_price, end_price):
    driver.get("https://shop.by")
    home_page = HomePage(driver=driver)
    home_page.move_to_catalog()
    home_page.move_to_section_computers()
    home_page.move_to_section_laptops_and_accessories()
    home_page.click_on_laptops()
    catalog_page = CatalogPage(driver=driver)
    catalog_page.click_show_more("1")
    catalog_page.click_on_check_box_laptop("Dell")
    catalog_page.click_on_check_box_laptop("HP")
    catalog_page.click_on_check_box_laptop("Lenovo")
    catalog_page.fill_price_before(start_price)
    catalog_page.fill_price_after(end_price)
    catalog_page.open_diagonal_menu()
    catalog_page.click_show_more("4")
    catalog_page.click_on_checkbox_item("12.5")
    catalog_page.click_on_checkbox_item("13.3")
    catalog_page.click_show()
    all_elements_on_page = catalog_page.get_all_elements()
    assert len(all_elements_on_page) == 48
    catalog_page.click_on_filter_by_name()
    breakpoint()

