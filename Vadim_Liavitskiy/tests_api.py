import json

import requests
from Vadim_Liavitskiy import api_urls

headers = {"Content-Type": "application/json"}


def test_user_crud(user_data):
    create_response = requests.post(url=api_urls.CREATE_USER, headers=headers, data=json.dumps(user_data))
    assert create_response.status_code == 200
    updated_data = {"email": "updated@dsad"}
    body = {**user_data, **updated_data}
    update_response = requests.put(url=api_urls.USER_NAME.format(username="Oleg_ivanov3"), headers=headers, data=json.dumps(body))
    assert update_response.status_code == 200
    get_response = requests.get(url=api_urls.USER_NAME.format(username="Oleg_ivanov3"), headers=headers)
    assert get_response.json()["email"] == updated_data["email"]
    response_delete = requests.delete(url=api_urls.USER_NAME.format(username="Oleg_ivanov3"), headers=headers)
    assert response_delete.status_code == 200


def test_user_auth(create_user):
    login_response = requests.get(
        url=api_urls.USER_LOGIN,
        headers=headers,
        params={'username': create_user['username'], 'password': create_user['password']}
    )
    assert login_response.status_code == 200
    assert "logged in user session" in login_response.json()["message"]
    logout_response = requests.get(url=api_urls.USER_LOGOUT, headers=headers)
    assert logout_response.status_code == 200


def test_pets_crud(pet_data):
    create_pet_response = requests.post(url=api_urls.PET, headers=headers,  data=json.dumps(pet_data))
    assert create_pet_response.status_code == 200
    get_pet = requests.get(url=api_urls.PET_ID.format(petId=pet_data["id"]), headers=headers)
    assert get_pet.status_code == 200
    updated_data = {"name": "naked cat"}
    body = {**pet_data, **updated_data}
    update_pet_response = requests.put(url=api_urls.PET, headers=headers,  data=json.dumps(body))
    assert update_pet_response.status_code == 200
    assert update_pet_response.json()["name"] == updated_data["name"]
    delete_response = requests.delete(url=api_urls.PET_ID.format(petId=pet_data["id"]), headers=headers)
    assert delete_response.status_code == 200
    get_deleted_pet = requests.get(url=api_urls.PET_ID.format(petId=pet_data["id"]), headers=headers)
    assert get_deleted_pet.status_code == 404


def test_store(pet, order_data):
    store_response = requests.get(url=api_urls.PET_STORE, headers=headers)
    assert store_response.status_code == 200
    pet_data = {"petId": pet["id"]}
    data = {**order_data, **pet_data}
    create_order = requests.post(url=api_urls.PET_STORE_ORDER, headers=headers, data=json.dumps(data))
    assert create_order.status_code == 200
    get_order = requests.get(url=api_urls.PET_STORE_ORDER_ID.format(orderId=order_data["id"]), headers=headers)
    assert get_order.status_code == 200
    delete_order_response = requests.delete(
        url=api_urls.PET_STORE_ORDER_ID.format(orderId=order_data["id"]),
        headers=headers
    )
    assert delete_order_response.status_code == 200

