import datetime
import json
from random import randint

import requests

import pytest

from Vadim_Liavitskiy import api_urls

headers = {"Content-Type": "application/json"}


@pytest.fixture()
def user_data():
    body = {
        "id": randint(1, 1000),
        "username": "Oleg_ivanov3",
        "firstName": "oleg",
        "lastName": "ivanov",
        "email": "string@dsad",
        "password": "123456789",
        "phone": "+3232343424",
        "userStatus": randint(1, 100)
    }
    return body


@pytest.fixture()
def create_user():
    body = {
        "id": randint(1, 1000),
        "username": "Ivan_olegov",
        "firstName": "Ivan",
        "lastName": "Olegov",
        "email": "ivan_olegov@dsad",
        "password": "123456789",
        "phone": "+3232343424",
        "userStatus": randint(1, 100)
    }

    response = requests.post(url=api_urls.CREATE_USER, headers=headers, data=json.dumps(body))

    assert response.status_code == 200, response.text

    yield body

    response_delete = requests.delete(url=api_urls.USER_NAME.format(username=body["username"]), headers=headers)

    assert response_delete.status_code == 200, response_delete.text


@pytest.fixture()
def pet_data():
    data = {
        "id": randint(1, 1000),
        "category": {
            "id": randint(1, 1000),
            "name": "mammals"
        },
        "name": "naked mole rat",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": randint(1, 1000),
                "name": "rat"
            }
        ],
        "status": "available"
    }

    yield data

@pytest.fixture()
def pet():
    data = {
        "id": randint(1, 1000),
        "category": {
            "id": randint(1, 1000),
            "name": "mammals"
        },
        "name": "fixtured alpaka",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": randint(1, 1000),
                "name": "rat"
            }
        ],
        "status": "available"
    }

    create_pet_response = requests.post(url=api_urls.PET, headers=headers,  data=json.dumps(data))
    assert create_pet_response.status_code == 200
    get_pet = requests.get(url=api_urls.PET_ID.format(petId=data["id"]), headers=headers)
    assert get_pet.status_code == 200

    yield data

    delete_response = requests.delete(url=api_urls.PET_ID.format(petId=data["id"]), headers=headers)
    assert delete_response.status_code == 200

@pytest.fixture()
def order_data():
    data = {
  "id": randint(1, 10),
  "petId": 0,
  "quantity": 1,
  "shipDate": "2021-06-05T10:28:35.849Z",
  "status": "available",
  "complete": True
}
    yield data