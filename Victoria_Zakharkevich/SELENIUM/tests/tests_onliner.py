from locators.Onliner import catalog_locators
from pages.Onliner.catalog_page import catalog_page
from locators.Onliner.home_page_locators import CATALOG
from pages.Onliner.home_page import HomePage
from pages.Onliner.product_page import ProductPage
from pages.Onliner.cart_page import CartPage
from locators.Onliner.catalog_locators import AVTO_MOTO_ITEM,AVTOACUSTIKA,KABELI
import time 


"""
1. Перейти на сайт https://www.onliner.by/
2. Перейти в каталог
3. Выбрать раздел "Авто и мото"
4. Выбрать "Автоакустика"
5. Выбрать "Кабели для автоакустики"
6. Убедиться, что мы находимся в разделе "Автоакустика"
7. Кликнуть на первый товар
8. Добавить товар в корзину
9. Перейти в корзину
10. Проверить наличие товара в корзине
"""

def test_auto(driver_chrome):
    driver_chrome.get("https://www.onliner.by/")
    home_page = HomePage(driver=driver_chrome)
    home_page.click_catalog()
    catalog = catalog_page(driver=driver_chrome)
    time.sleep(15)
    catalog.avto_moto_click()
    catalog.switch_to_avtoacustica()
    catalog.switch_to_cabeli()
    appointment_cables = catalog_page.find_value()
    assert appointment_cables == 'для автоакустики'
    catalog_page.choose_title_catalog()
    product_page = ProductPage(driver=driver_chrome)
    product_title = product_page.get_title()
    product_page.click_add_to_cart_button()
    product_page.go_to_cart()
    cart_page = CartPage(driver=driver_chrome)
    cart_title = cart_page.get_cart_title()
    assert product_title.lower() == cart_title.lower()










