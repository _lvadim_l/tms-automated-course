from selenium.webdriver.common.by import By

MAIN_INPUT = (By.XPATH, '//input[@class="fast-search__input"]')

IFRAME = (By.XPATH, '//iframe[@class="modal-iframe"]')

FLEA_MARKET = (By.XPATH, '//div[contains(text(), "на барахолке")]') # локатор для барахолки

SEARCH_CATALOG_RESULTS = (By.XPATH, '//div[@class="search__widget-item"]')

CATALOG = (By.XPATH, '//*[@id="container"]/div/div/header/div[2]/div/nav/ul[1]/li[1]/a[2]/span')

