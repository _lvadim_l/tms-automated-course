from selenium.webdriver.common.by import By

MENU = (By.CSS_SELECTOR, '.catalog-navigation-classifier')

AVTO_MOTO_ITEM = (By.XPATH, '//*[@id="container"]/div/div/div/div/div[1]/ul/li[7]/span[2]/span')

# AVTO_MOTO_ITEM = (By.XPATH, '//li[@data-id="6"]//span[@class="catalog-navigation-classifier__item-title"]')

AVTOACUSTIKA =  (By.XPATH, '//div[contains(text(), "Автоакустика")]')

# KABELI = (By.XPATH, '//span[contains(text(), "автоакустики"')

KABELI = (By.XPATH, '//*[@id="container"]/div/div/div/div/div[1]/div[3]/div/div[6]/div[1]/div/div[5]/div[2]/div/a[5]/span/span[2]')

APPOINTMENT_CATALOG = (By.XPATH, '//div[@title="Назначение"]/span[@class="schema-tags__text"]')

ITEM_FROM_CATALOG = (By.XPATH, '//div[@class="schema-product__title"]')


# +AUTO_MOTO = (By.XPATH, '//li[@data-id="6"]//span[@class="catalog-navigation-classifier__item-title"]')
# +AUTO_ACOUSTICS = (By.XPATH, '//div[contains(text(), "Автоакустика")]')
# +CABLES_FOR_AUTO_ACOUSTICS = (By.XPATH, '//span[contains(text(), "автоакустики")]')
# +
# +APPOINTMENT_CATALOG = (By.XPATH, '//div[@title="Назначение"]/span[@class="schema-tags__text"]')
# +ITEM_FROM_CATALOG = (By.XPATH, '//div[@class="schema-product__title"]')