import pytest
from selenium import webdriver


@pytest.fixture()
def driver_chrome():
    print("\nstart browser for test..")
    browser = webdriver.Chrome(executable_path="chromedriver.exe")
    browser.maximize_window()
    yield browser
    print("\nquit browser..")
    browser.quit()