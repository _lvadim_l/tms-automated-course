import time
from base_page import BasePage
from locators.Onliner import cart_locators

class CartPage(BasePage):

    def get_card_text(self) -> str:
        elem = self.find_element(cart_locators.PRODUCT_CARD)
        time.sleep(0.5)
        return elem.text

    def get_price(self) -> str:
        elem = self.find_element(cart_locators.PRICE)
        time.sleep(0.5)
        return elem.text

    def delete_product_from_cart(self):
        trash = self.find_element(cart_locators.DELETE_BUTTON)
        self.move_to_element(trash)
        self.find_element(cart_locators.DELETE_BUTTON).click()

    def check_absence_of_element_in_cart(self):
        return self.check_absence_element(cart_locators.ORDER_PROCESSING)

    def get_cart_title(self):
       return self.find_element(cart_locators.CART_TITLE).text

