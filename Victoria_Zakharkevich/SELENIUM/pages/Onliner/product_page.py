import time
from base_page import BasePage
from locators.Onliner import product_locators


class ProductPage(BasePage):
    
    def get_title(self):
        return self.find_element(product_locators.TITLE).text

    def click_add_to_cart_button(self):
        self.find_button(product_locators.ADD_TO_CART).click()

    def go_to_cart(self):
        self.find_button(product_locators.CART).click()
    