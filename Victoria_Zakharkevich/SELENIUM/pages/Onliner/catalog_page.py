import time
from base_page import BasePage
from locators.Onliner import catalog_locators
from typing import List

class catalog_page(BasePage):

    def avto_moto_click(self):
        self.find_element(catalog_locators.AVTO_MOTO_ITEM).click()
 
    def switch_to_avtoacustica(self):
        self.find_element(catalog_locators.AVTOACUSTIKA).click()

    def switch_to_cabeli(self):
        self.find_element(catalog_locators.KABELI).click()

    def find_value(self):
        return self.find_element(catalog_locators.APPOINTMENT_CATALOG).text

    def choose_title_catalog(self):
        self.find_element(catalog_locators.ITEM_FROM_CATALOG).click()


        




        

