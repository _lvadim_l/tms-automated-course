import requests
import json


# Получить список иностранных валют у НБРБ (см. http://www.nbrb.by/APIHelp/ExRates)
response = requests.get('https://www.nbrb.by/api/exrates/currencies')
assert response.status_code == 200
print(response.status_code)

# Напечатать курс USD и EUR к BYN
response_2 = requests.get('https://www.nbrb.by/api/exrates/rates?periodicity=0')
assert response.status_code == 200
all_rates = response_2.json()
current_USD_rate = 0
for rate in all_rates:
    if rate["Cur_Abbreviation"] == 'USD' or rate["Cur_Abbreviation"] == 'EUR':
        print(f'1 {rate["Cur_Abbreviation"]} is equal {rate["Cur_OfficialRate"]} BYN')
        if rate["Cur_Abbreviation"] == 'USD':
            current_USD_rate = rate["Cur_OfficialRate"]
    

# Напечатать только наименования валют USD, COP, MNT, ESP(на русском, белорусском и английском языках)
all_currencies = response.json()
for cur in all_currencies:
    if cur["Cur_Abbreviation"] in ['USD', 'COP', 'MNT', 'ESP']:
        print (f'{cur["Cur_Abbreviation"]} - {cur["Cur_Name"]}, {cur["Cur_Name_Bel"]}, {cur["Cur_Name_Eng"]}\n')

# Получить курс USD к BYN на 6 февраля 2019г.
response_3 = requests.get('https://www.nbrb.by/api/exrates/rates?ondate=2019-2-6&periodicity=0')
day_rate = response_3.json()
past_USD_rate = 0
for item in day_rate:
    if item["Cur_Abbreviation"] == 'USD':
        past_USD_rate = item["Cur_OfficialRate"]
        print(f'1 {item["Cur_Abbreviation"]} at 6.02.2019 is equal {item["Cur_OfficialRate"]} BYN')


# Посчитать сколько можно было купить USD по курсу 6 февраля 2019г и сегодня имея на руках 100 BYN.
sum = 100 #BYN

past_exchange = sum / past_USD_rate
print(past_exchange)

today_exchange = sum / current_USD_rate
print(today_exchange)















