import unittest
from HW_files.Library_hw import Book
from HW_files.Library_hw import Reader


#test creating book

class test_book(unittest.TestCase):
    def test_create_book(self):
        b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
        self.assertEqual(b1.book_name,'Три Товарища')


#test reader

class test_reader(unittest.TestCase):
    def test_reserve_book(self):
        book=Book(book_name='Мастер и маргарита', author='Михаил Булгаков', page_qty='345', isbn='YU2')
        user = Reader('Инна', 'Иванова')
        user.reserve_book(book)
        self.assertEqual(book.is_available, False)
        self.assertEqual(user.res_book, book)
    
    def test_reserve_reserved_book(self):
        book=Book(book_name='Книга', author='автор', page_qty='345', isbn='62')
        user1 = Reader('Инна', 'Иванова')
        user2 = Reader('Инна2', 'Иванова2')
        user1.reserve_book(book)
        user2.reserve_book(book)
        self.assertEqual(user2.res_book, None)

    def test_return_reserved_book(self):
        b2 = Book(book_name='Мастер и маргарита', author='Михаил Булгаков', page_qty='345', isbn='YU2')
        user2 = Reader('Инна', 'Иванова')
        user2.reserve_book(b2)
        user2.return_book()
        self.assertEquals(user2.book.is_available, True)
        self.assertEquals(user2.book, None)
    
    def test_take_book(self):
        b2 = Book(book_name='Мастер и маргарита', author='Михаил Булгаков', page_qty='345', isbn='YU2')
        user2 = Reader('Инна', 'Иванова')
        user2.take_book(b2)
        self.assertEquals(user2.book, b2)
    
    

    # def test_take_secondBook(self):

    # def test_take_reservedBook(self):
    
    
    
    # def test_reserve_secondbook(self):
    
    # def test_reserve_unavailableBook(self):

    # def test_remove_reserve(self):
    
    # def test_remove_reserve_empty(self):

if __name__ == '__main__':
   unittest.main()





