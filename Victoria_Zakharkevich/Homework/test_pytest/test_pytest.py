import pytest
from HW_files.Library_hw import Book
from HW_files.Library_hw import Reader


#test creating book
def test_create_book():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    assert b1.book_name == 'Три Товарища'

#test creating reader
def test_create_reader():
    user = Reader('Инна', 'Иванова')
    assert user.first_name == 'Инна'

# available book reservation
def test_reserve_book():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    user = Reader('Инна', 'Иванова')
    user.reserve_book(b1)
    assert b1.is_available == False
    assert user.res_book == b1

# not available book reservation
def test_reserve_unavailable_book():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    user1 = Reader('Инна', 'Иванова')
    user2 = Reader('Василий', 'Петров')
    user1.reserve_book(b1)
    user2.reserve_book(b1)
    assert b1.is_available == False
    assert user2.res_book == None

# take available book
def test_take_book():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    user1 = Reader('Инна', 'Иванова')
    assert b1.is_available == True
    user1.take_book(b1)
    assert user1.book == b1
    assert b1.is_available == False

 # Take second book:
def test_take_second_book():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    b2 = Book(book_name='Мастер и маргарита', author='Михаил Булгаков', page_qty='345', isbn='YU2')
    user = Reader('Инна', 'Иванова')
    user.take_book(b1)
    assert b1.is_available == False
    assert user.book == b1
    user.take_book(b2)
    # assert (print("У вас уже есть книга, верните"))
    assert b2.is_available == True

# take self reserved book
def test_take_reservedBook():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    user = Reader('Инна', 'Иванова')
    user.reserve_book(b1)
    assert b1.is_available == False
    assert user.res_book == b1
    user.take_book(b1)
    assert user.book == b1
    assert user.res_book == None
    
# remove reservation
def test_remove_reservation():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    user = Reader('Инна', 'Иванова')
    user.reserve_book(b1)
    assert b1.is_available == False
    assert user.res_book == b1
    user.remove_reserve()
    assert b1.is_available == True
    assert user.res_book == None

    
#trying to remove empty reservation
def test_remove_reserve_empty():
    b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
    user = Reader('Инна', 'Иванова')
    assert user.res_book == None
    user.remove_reserve()
    assert("У вас нечего возвращать")
    assert b1.is_available == True
    assert user.res_book == None


# #test creating book
# def test_create_book(new_book_creation):
#     assert b1.book_name == 'Три Товарища'

# #test creating reader
# def test_create_reader(new_reder_creation):
#     assert user.first_name == 'Инна'

    