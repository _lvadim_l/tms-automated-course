# -*- coding: utf-8 -*-
"""Untitled9.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1K2PTxWGTSjaIj-wKmjmajjEYr4MbqtTe
"""

class Book:
    def __init__(self,book_name:str, author:str, page_qty:str, isbn:str, is_available = True):
      self.book_name = book_name
      self.author = author
      self.page_qty = page_qty
      self.isbn = isbn
      self.is_available = is_available

class Reader:
  def __init__(self, first_name, last_name):
    self.book = None
    self.res_book = None
    self.first_name = first_name
    self.last_name = last_name

  def return_book(self):
    if self.book:      
      book_name = self.book.book_name
      self.book.is_available = True     
      self.book = None
      return f'Вы вернули книгу {book_name}'
    else: 
      return 'У вас нет книги для возврата'

  def take_book(self, book):
    if self.book:
      return "У вас уже есть книга, верните"
    elif book == self.res_book:
      self.book = book 
      self.res_book = None
      return "Вы взяли книгу, которую забронировали"
    elif not book.is_available:
      return "Эта книга забронирована"
    else:
      self.book = book
      self.book.is_available = False
      return f"Вы взяли книгу {book.book_name}"

  def reserve_book(self, book):
    if self.res_book:
      return "У вас уже есть зарезервированная книга"
    elif book.is_available:
      book.is_available = False
      self.res_book = book    
      return f"Вы забронировали книгу {book.book_name}"
    else:
      return "Эта книга недоступна к брони"

  def remove_reserve(self):
    if self.res_book:
      book_name = self.res_book.book_name
      self.res_book.is_available = True
      self.res_book = None
      return f"Вы отменили бронь {book_name}"
    else:
      return "У вас нечего возвращать"

    


    
  
# b1 = Book(book_name='Три Товарища', author='Эрих Мария Ремарк', page_qty='433', isbn='YU1' )
# b2 = Book(book_name='Мастер и маргарита', author='Михаил Булгаков', page_qty='345', isbn='YU2')
# b3 = Book(book_name='Сборник стихов', author='русские поэты', page_qty='456', isbn='YU3')


# user1 = Reader('Василий' , 'Васильев')
# user2 = Reader('Инна', 'Иванова')

# print(user1.take_book(b1))
# # print(user1.__dict__)
# # print(b1.__dict__)

# print(user2.take_book(b1))
# # print(user2.__dict__)
# # print(b2.__dict__)

# print(user2.reserve_book(b2))

# print(user1.reserve_book(b2))

# print(user1.return_book())

# print(user1.return_book())

# print(user1.remove_reserve())

# print(user2.remove_reserve())