import pytest
from random import randint
import randomaizer
from json import dumps,loads
import requests
from heders import HEADERS
import api_urls

@pytest.fixture()
def user_data():
    random_data = randomaizer.RandomData()
    random_name = random_data.generate_word(6)
    random_word = random_data.generate_word(6)
    data = {
          "id":randint(1,100),
          "username":random_name,
          "firstName": random_word,
          "lastName": random_word,
          "email": "test@test.com",
          "password": "123456789",
          "phone": "123456789",
          "userStatus": randint(1,100)
}
    return data

@pytest.fixture()
def user(user_data):
    user_response = requests.post(
        url=api_urls.USER, headers=HEADERS, data=dumps(user_data)
    )
    assert user_response.status_code == 200

    yield user_data

    delete_user_response =  requests.delete(
        url=api_urls.USER_NAME.format(username=(user_data['username'])),
        headers=HEADERS,
    )
    assert delete_user_response.status_code == 200

@pytest.fixture()
def pet_data():
    random_data = randomaizer.RandomData()
    random_name = random_data.generate_word(6)
    random_word = random_data.generate_word(6)
    data = {
  "id": randint(1,100),
  "category": {
    "id": randint(1,100),
    "name": random_name
  },
  "name": random_name,
  "photoUrls": [
    "https://www.google.com/google"
  ],
  "tags": [
    {
      "id": randint(1,100),
      "name": random_name
    }
  ],
  "status": random_word
}
    return  data

@pytest.fixture()
def store_data():
    random_data = randomaizer.RandomData()
    data = {
              "id": randint(1,10),
              "petId": randint(1,100),
              "quantity": randint(1,10),
              "shipDate": "2021-06-08T13:26:54.493Z",
              "status": "placed",
              "complete": 'true'
}
    return data