import requests
from heders import HEADERS
import api_urls
from json import dumps

def test_user_crud(user_data):
    user_response = requests.post(
        url=api_urls.USER,headers = HEADERS,data=dumps(user_data)
    )
    assert user_response.status_code == 200


    get_user_response = requests.get(
        url=api_urls.USER_NAME.format(username=(user_data['username'])),
        headers=HEADERS
    )
    assert get_user_response.status_code == 200
    assert get_user_response.json()['id'] == user_data['id']


    update_data = {"email": "test@test_123.com","phone": '987665'}
    update_user_data = {**user_data, **update_data}
    updated_response = requests.put(
        url=api_urls.USER_NAME.format(username=(user_data['username'])),
        headers=HEADERS,
        data = dumps(update_user_data)
    )
    assert updated_response.status_code == 200


    get_update_user = requests.get(
        url=api_urls.USER_NAME.format(username=(user_data['username'])),
        headers=HEADERS,
    )
    assert get_update_user.status_code == 200
    assert get_update_user.json()['email'] == update_user_data['email']


    delete_user_response = requests.delete(
        url=api_urls.USER_NAME.format(username=(user_data['username'])),
        headers=HEADERS,
    )
    assert delete_user_response.status_code == 200


    get_delete_user = requests.get(
        url=api_urls.USER_NAME.format(username=(user_data['username'])),
        headers=HEADERS,
    )
    assert get_delete_user.status_code == 404


def test_auth(user):
    login_response = requests.get(
        url=api_urls.LOGIN,
        headers = HEADERS,
        params = {"username": user['username'],"password": user['password']}
    )
    assert login_response.status_code == 200
    assert "logged in user session" in login_response.json()['message']


    logout_response = requests.get(
        url=api_urls.LOGOUT,
        headers = HEADERS,
    )
    assert logout_response.status_code == 200
    assert logout_response.json()['message'] == 'ok'


def test_store_crud(store_data):
    store_response = requests.post(
        url = api_urls.STORE,
        headers = HEADERS,
        data=dumps(store_data)
    )
    assert store_response.status_code == 200


    get_order =  requests.get(
        url=api_urls.GET_STORE.format(orderId=(store_data['id'])),
        headers=HEADERS
    )
    assert get_order.json()['id'] == store_data['id']
    assert get_order.status_code == 200

    delete_order = requests.delete(
        url=api_urls.GET_STORE.format(orderId=(store_data['id'])),
        headers=HEADERS
    )
    assert delete_order.status_code == 200


    get_delete_order = requests.get(
    url=api_urls.GET_STORE.format(orderId=(store_data['id'])),
        headers=HEADERS
    )
    assert get_delete_order.status_code == 404

