from selenium.webdriver.common.by import By
import time
def test_find_element(driver):
    driver.get("https://www.onliner.by/")
    elem = driver.find_element(By.XPATH, '//input[@class="fast-search__input"]')
    elem.send_keys('iPhone 12 64 Черный')
    time.sleep(1)
    driver.switch_to.frame(driver.find_element_by_xpath('//iframe[@class="modal-iframe"]'))
    driver.find_element(By.XPATH,'//div[contains(text(), "на барахолке")]').click()
    time.sleep(2)
    driver.find_element(By.XPATH,'//div[@class="search__widget-item"]/a[@href="https://catalog.onliner.by/mobile/apple/iphone12"]').click()
    url_get=driver.current_url
    url = "https://catalog.onliner.by/mobile/apple/iphone12"
    assert url == url_get

def test_cart(driver):
    driver.get("https://www.onliner.by/")
    elem = driver.find_element(By.XPATH, '//input[@class="fast-search__input"]')
    elem.send_keys('iPhone 12 64 Черный')
    time.sleep(2)
    driver.switch_to.frame(driver.find_element_by_xpath('//iframe[@class="modal-iframe"]'))
    driver.find_element(By.XPATH, '//div[contains(text(), "на барахолке")]').click()
    time.sleep(2)
    driver.find_element(By.XPATH, '//div[@class="search__widget-item"]/a[@href="https://catalog.onliner.by/mobile/apple/iphone12"]').click()
    time.sleep(2)
    driver.find_element(By.XPATH, '//a[@class="button-style button-style_expletive button-style_base-alter product-aside__item-button"]').click()
    time.sleep(2)
    driver.find_element(By.XPATH, '//a[@class="button-style button-style_base-alter product-aside__item-button button-style_primary"]').click()
    time.sleep(2)
    price=driver.find_element(By.XPATH, '//div[@class="cart-form__description cart-form__description_primary cart-form__description_base-alter cart-form__description_font-weight_semibold cart-form__description_ellipsis cart-form__description_condensed-another"]').get_attribute("innerHTML")[6:10]
    price1 = "2399"
    assert price1 == price

def test_quantity(driver):
    driver.get("https://www.onliner.by/")
    elem = driver.find_element(By.XPATH, '//input[@class="fast-search__input"]')
    elem.send_keys('iPhone 12 64 Черный')
    time.sleep(2)
    driver.switch_to.frame(driver.find_element_by_xpath('//iframe[@class="modal-iframe"]'))
    driver.find_element(By.XPATH, '//div[contains(text(), "на барахолке")]').click()
    time.sleep(3)
    driver.find_element(By.XPATH, '//div[@class="search__widget-item"]/a[@href="https://catalog.onliner.by/mobile/apple/iphone12"]').click()
    time.sleep(2)
    driver.find_element(By.XPATH, '//a[@class="button-style button-style_expletive button-style_base-alter product-aside__item-button"]').click()
    time.sleep(2)
    driver.find_element(By.XPATH, '//a[@class="button-style button-style_base-alter product-aside__item-button button-style_primary"]').click()
    time.sleep(2)
    price = driver.find_element(By.XPATH,'//div[@class="cart-form__description cart-form__description_primary cart-form__description_base-alter cart-form__description_font-weight_semibold cart-form__description_ellipsis cart-form__description_condensed-another"]').get_attribute("innerHTML")[6:10]
    elem_inp=driver.find_element(By.XPATH, '//input[@type="text"]')
    elem_inp.send_keys('0')
    total_price=int(price)*10
    price1= 23990
    assert total_price == price1

