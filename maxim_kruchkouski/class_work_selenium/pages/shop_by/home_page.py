
from maxim_kruchkouski.class_work_selenium.base_page import BasePage
from maxim_kruchkouski.class_work_selenium.locators.shop_by import home_page_locators

class HomePage(BasePage):

    def go_to_site(self):
        self.driver.get("https://shop.by/")

    def move_to_catalog(self):
        elem = self.find_element(home_page_locators.CATALOG)
        self.move_to_element(elem)

    def move_to_section_computer(self):
        elem = self.find_element(home_page_locators.COMPUTER)
        self.move_to_element(elem)

    def move_to_section_laptops_and_accessories(self):
        elem = self.find_element(home_page_locators.LAPTOPS_AND_ACCESSORIES)
        self.move_to_element(elem)

    def click_on_laptops(self):
        self.find_element(home_page_locators.LAPTOPS).click()

