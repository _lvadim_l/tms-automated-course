import time

from maxim_kruchkouski.class_work_selenium.base_page import BasePage
from maxim_kruchkouski.class_work_selenium.locators.shop_by import catalog_page_locators

class CATALOGPAGE(BasePage):

    def click_show_more(self, index):
        locator = catalog_page_locators.show_more(index)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()
        time.sleep(0.5)

    def click_on_check_box_laptop(self, laptop_name):
        locator = catalog_page_locators.laptop(laptop_name)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()
        time.sleep(0.5)


    def input_min(self):
        self.find_element(catalog_page_locators.INPUT_PRICE_MIN).send_keys("1500")

    def input_max(self):
        self.find_element(catalog_page_locators.INPUT_PRICE_MAX).send_keys("3000")

    def open_diagonal_menu(self):
        elem = self.find_element(catalog_page_locators.DIAGONAL)
        self.move_to_element_with_js(locator=catalog_page_locators.DIAGONAL, web_element=elem)
        elem.click()
        time.sleep(0.5)

    def click_show(self):
        time.sleep(0.5)
        self.find_element(catalog_page_locators.SHOW).click()

    def get_all_elements(self):
        return self.find_elements(catalog_page_locators.ELEMENTS_ON_CATALOG)

    def open_sorting_filter(self):
        self.find_element(catalog_page_locators.SORTING_FILTER).click()
        time.sleep(0.5)

    def click_sorting_filter_with_cheap_goods(self):
        self.find_element(catalog_page_locators.SORTING_FILTER_START_WITH_CHEAP_GOODS).click()

    def get_first_element_on_page(self):
       elem = self.find_element(catalog_page_locators.FIRST_ELEMENT_ON_PAGE)
       return elem.text

    def click_sorting_filter_with_expensive_goods(self):
        self.find_element(catalog_page_locators.SORTING_FILTER_START_WITH_EXPENSIVE_GOODS).click()

    def scroll_to_pages_switch_buttons(self):
        elem = self.find_element(catalog_page_locators.PAGES_SWITCH_BUTTONS)
        self.move_to_element_with_js(locator=catalog_page_locators.PAGES_SWITCH_BUTTONS, web_element=elem)



    def switch_to_last_page(self,index):
            locator = catalog_page_locators.pages_switch_buttons(index)
            elem = self.find_element(locator)
            self.move_to_element_with_js(locator=locator, web_element=elem)
            elem.click()

    def get_last_element(self,index):
        locator = catalog_page_locators.last_element_on_page(index)
        elem = self.find_element(locator)
        return elem.text
