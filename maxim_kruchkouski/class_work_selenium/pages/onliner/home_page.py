from maxim_kruchkouski.class_work_selenium.base_page import BasePage
from maxim_kruchkouski.class_work_selenium.locators.onliner import home_page_locators
from typing import List
import time


class HomePage(BasePage):

    def go_to_site(self):
        self.driver.get("https://www.onliner.by/")

    def open_category_catalog(self):
        self.find_element(home_page_locators.CATEGORY_CATALOG).click()

    def send_text_in_input(self, text):
        self.find_element(home_page_locators.MAIN_INPUT).send_keys(text)

    def switch_to_flea_market(self):
        self.switch_to_frame(home_page_locators.IFRAME)
        self.find_element(home_page_locators.FLEA_MARKET).click()

    def get_search_results_from_catalog(self) -> List:
        elements = self.find_elements(home_page_locators.SEARCH_CATALOG_RESULTS)
        assert len(elements) > 0, "Elements not found"
        return elements

    def click_on_element_by_index(self, index: int, web_elem_list: List):
        assert len(web_elem_list) > 0, "List of elements is empty"
        web_elem_list[index].click()





