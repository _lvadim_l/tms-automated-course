import time
from selenium.webdriver.common.by import By


from maxim_kruchkouski.class_work_selenium.base_page import BasePage

from maxim_kruchkouski.class_work_selenium.locators.onliner import category_catalog_locators
class CATALOGPAGE(BasePage):

    def open_category_in_catalog_auto_and_moto(self):
        self.find_element(category_catalog_locators.AUTO_AND_MOTO).click()

    def open_section_car_acoustics(self):
        self.find_element(category_catalog_locators.SECTION_CAR_ACOUSTICS).click()

    def open_subsection_cables_for_acoustics(self):
        self.find_element(category_catalog_locators.SUBSECTION_CABLES_FOR_ACOUSTICS).click()

    def get_name_section(self) -> str:
        element = self.find_element(category_catalog_locators.SECTION_CAR_ACOUSTICS)
        time.sleep(0.5) #TODO refactor sleeps in tests
        return element.text

    def click_on_the_first_product(self):
        self.find_element(category_catalog_locators.FIRST_PRODUCT_IN_CATALOG).click()

    def open_category_computer_and_networks(self):
        self.find_element(category_catalog_locators.COMPUTERS_AND_NETWORKS).click()

    def open_section_laptops_computers_monitors(self):
        self.find_element(category_catalog_locators.SECTION_LAPTOPS_COMPUTERS_MONITORS).click()

    def open_subsection_laptops(self):
        self.find_element(category_catalog_locators.SUBSECTION_LAPTOPS).click()


    def click_on_check_box_laptop(self,laptop_name):
        locator = category_catalog_locators.laptop(laptop_name)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()

    def click_show_more(self,index):
        locator = category_catalog_locators.show_more(index)
        elem = self.find_element(locator)
        self.move_to_element_with_js(locator=locator, web_element=elem)
        elem.click()

    def input_min_price(self,sum_min:str):
        self.find_element(category_catalog_locators.INPUT_PRICE_MIN).send_keys(sum_min)

    def input_max_price(self,sum_max:str):
        self.find_element(category_catalog_locators.INPUT_PRICE_MAX).send_keys(sum_max)

    def input_screen_diagonal_min(self,size:str):
        locator = category_catalog_locators.screen_diagonal_min(size)
        elem = self.find_element(locator)
        elem.click()


    def input_screen_diagonal_max(self,index):
        locator = category_catalog_locators.screen_diagonal_max(index)
        elem = self.find_element(locator)
        elem.click()

    def get_all_elements_on_page(self):
        return self.find_elements(category_catalog_locators.GET_ALL_ELEMENTS_ON_PAGE)

    def open_soring_filters(self):
        self.find_element(category_catalog_locators.SORTING_FILTERS).click()


    def filter_selections(self,name:str):
        time.sleep(1)
        self.find_element(category_catalog_locators.filter_selection(name)).click()

    def get_name_product(self,index:str):
        time.sleep(1)
        elem = self.find_element(category_catalog_locators.get_name_product(index))
        return elem.text

    def open_pagination(self):
        time.sleep(1)
        self.find_element(category_catalog_locators.PAGES_SWITCH_BUTTONS).click()

    def switch_to_last_page(self,index:str):
        locator = category_catalog_locators.pages_switch_buttons(index)
        elem = self.find_element(locator)
        elem.click()

    def get_pages_switch_buttons(self):
        elem = self.find_elements(category_catalog_locators.LAST_SWITCH_BUTTONS)
        return len(elem)

    def get_last_product_name(self):
        time.sleep(0.5)
        elem = self.find_elements(category_catalog_locators.LAST_PRODUCT_NAME)
        return len(elem)
