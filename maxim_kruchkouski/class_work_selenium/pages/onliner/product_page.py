import time

from maxim_kruchkouski.class_work_selenium.base_page import BasePage

from maxim_kruchkouski.class_work_selenium.locators.onliner import product_locators

class ProductPage(BasePage):

    def get_headline_text(self) -> str:
        element = self.find_element(product_locators.HEAD_LINE_IN_CATALOG)
        time.sleep(0.5) #TODO refactor sleeps in tests
        return element.text

    def click_on_cart_button(self):
        self.find_element(product_locators.ADD_TO_CART).click()

    def click_on_active_cart_button(self):
        self.find_element(product_locators.IN_CART_ACTIVE_BUTTON).click()