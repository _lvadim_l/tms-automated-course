import pytest
from selenium import webdriver


@pytest.fixture()
def driver():
    print("\nstart browser for test..")
    browser = webdriver.Chrome(executable_path="/home/maxim/PycharmProjects/tms-automated-course/maxim_kruchkouski/culc/chromedriver")
    browser.maximize_window()
    yield browser
    print("\nquit browser..")
    browser.quit()