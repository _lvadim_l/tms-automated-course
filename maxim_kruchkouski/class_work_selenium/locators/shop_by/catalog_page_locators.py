from selenium.webdriver.common.by import By


def show_more(index):
    return (By.XPATH,
            f'(//span[@class="ModelFilter__OpenHideAttrTxt Page__DarkDotLink"])[{index}]')

def pages_switch_buttons(index):
    return (By.XPATH,f'//body/div[2]/div[1]/div[1]/div[3]/div[4]/div[2]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/a[{index}]')

def laptop(name):
    return (By.XPATH, f'//label[@data-ga="{name}"]')

def last_element_on_page(index):
    return (By.XPATH,
            f'(//a[@class="ModelList__LinkModel"])[{index}]')

INPUT_PRICE_MIN = (By.XPATH,"//input[@id='minnum_45']")
INPUT_PRICE_MAX = (By.XPATH,"//input[@id='maxnum_45']")
DIAGONAL = (By.XPATH,'''//span[contains(text(),'Диагональ экрана, "')]''')
SHOW = (By.XPATH,'//body/div[2]/div[1]/div[1]/div[3]/div[4]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/div[1]/span[1]/span[1]')
ELEMENTS_ON_CATALOG =(By.XPATH, '//div[@class="ModelList__InfoModelBlock ModelList__DescModelBlock"]')
SORTING_FILTER = (By.XPATH,"//body/div[2]/div[1]/div[1]/div[3]/div[4]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/span[1]/span[1]")
SORTING_FILTER_START_WITH_CHEAP_GOODS = (By.XPATH,"//li[contains(text(),'С дешевых')]")
FIRST_ELEMENT_ON_PAGE = (By.XPATH,"//body/div[2]/div[1]/div[1]/div[3]/div[4]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]")
SORTING_FILTER_START_WITH_EXPENSIVE_GOODS = (By.XPATH,"//li[contains(text(),'С дорогих')]")
PAGES_SWITCH_BUTTONS = (By.XPATH,'//div[@class="Paging__InnerPages"]')