from selenium.webdriver.common.by import By

AUTO_AND_MOTO = (By.XPATH, '//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[7]')
SECTION_CAR_ACOUSTICS = (By.XPATH,'//div[contains(text(),"Автоакустика")]')
SUBSECTION_CABLES_FOR_ACOUSTICS = (By.XPATH, '//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[6]/div[1]/div[1]/div[5]/div[2]/div[1]/a[5]')
FIRST_PRODUCT_IN_CATALOG = (By.XPATH,'//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[4]/div[3]/div[4]/div[1]/div[1]/div[1]/div[2]/a[1]/img[1]')
COMPUTERS_AND_NETWORKS = (By.XPATH,"//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]")
SECTION_LAPTOPS_COMPUTERS_MONITORS = (By.XPATH,"//div[contains(text(),'Ноутбуки, компьютеры, мониторы')]")
SUBSECTION_LAPTOPS = (By.XPATH,"//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/a[1]")


def laptop(name):
    return (By.XPATH, f"//span[@class='schema-filter__checkbox-text'][contains(., '{name}')]")

def show_more(index):
    return (By.XPATH,
            f'(//div[@class="schema-filter-control__item"])[{index}]')

INPUT_PRICE_MIN = (By.XPATH,"//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[4]/div[2]/div[2]/div[1]/div[3]/div[7]/div[2]/div[1]/div[1]/input[1]")

INPUT_PRICE_MAX = (By.XPATH,"//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[4]/div[2]/div[2]/div[1]/div[3]/div[7]/div[2]/div[1]/div[2]/input[1]")

def screen_diagonal_min(size):
    return (By.XPATH,f'(//select[@class="schema-filter-control__item"]/option[contains(., "{size}")])[1]')

def screen_diagonal_max(size):
    return (By.XPATH,f'(//select[@class="schema-filter-control__item"]/option[contains(., "{size}")])[2]')

GET_ALL_ELEMENTS_ON_PAGE = (By.XPATH,'//div[@class="schema-product__group"]')

SORTING_FILTERS = (By.XPATH,"//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[4]/div[3]/div[1]/a[1]")

def filter_selection(filter_name):
    return (By.XPATH,f"//span[contains(text(),'{filter_name}')]")

def get_name_product(index):
    return (By.XPATH,f'(//span[@data-bind="html: product.extended_name || product.full_name"])[{index}]')

def pages_switch_buttons(index):
    return (By.XPATH,f'(//a[@class="schema-pagination__pages-link"])[{index}]')

PAGES_SWITCH_BUTTONS = (By.XPATH,"//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[4]/div[3]/div[6]/div[1]/div[1]")

LAST_SWITCH_BUTTONS = (By.XPATH,f'(//a[@class="schema-pagination__pages-link"])')

LAST_PRODUCT_NAME = (By.XPATH,'(//span[@data-bind="html: product.extended_name || product.full_name"])')
