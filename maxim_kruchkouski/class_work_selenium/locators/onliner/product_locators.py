from selenium.webdriver.common.by import By

HEAD_LINE_IN_CATALOG = (By.XPATH, '//h1[@class="catalog-masthead__title"]')

ADD_TO_CART = (By.XPATH, '//a[@class="button-style button-style_expletive button-style_base-alter product-aside__item-button"]')

IN_CART_ACTIVE_BUTTON = (By.XPATH, '//a[@class="button-style button-style_base-alter product-aside__item-button button-style_primary"]')