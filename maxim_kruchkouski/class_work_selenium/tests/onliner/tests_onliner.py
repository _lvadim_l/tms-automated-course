from maxim_kruchkouski.class_work_selenium.pages.onliner.home_page import HomePage

def test_product_discovery(driver):
    home_page = HomePage(driver)
    home_page.go_to_site()
    home_page.send_text_in_input('iPhone 12 64 Черный')
    home_page.swich_to_frame().click()
    home_page.find_iphone_12()
    assert home_page.url_get() == "https://catalog.onliner.by/mobile/apple/iphone12"

def test_card(driver):
    home_page = HomePage(driver)
    home_page.go_to_site()
    home_page.send_text_in_input('iPhone 12 64 Черный')
    home_page.swich_to_frame().click()
    home_page.find_iphone_12()
    home_page.in_card().click()
    home_page.go_to_checkout().click()
    assert home_page.get_price_device() == '2399'

def test_quantity(driver):
    home_page = HomePage(driver)
    home_page.go_to_site()
    home_page.send_text_in_input('iPhone 12 64 Черный')
    home_page.swich_to_frame().click()
    home_page.find_iphone_12()
    home_page.in_card().click()
    home_page.go_to_checkout().click()
    home_page.get_price_device()
    assert home_page.increase_quantities_devices_in_card() == 23990