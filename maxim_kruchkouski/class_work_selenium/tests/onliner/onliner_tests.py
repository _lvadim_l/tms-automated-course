import time

from maxim_kruchkouski.class_work_selenium.pages.onliner.home_page import HomePage
from maxim_kruchkouski.class_work_selenium.pages.onliner.product_page import ProductPage
from maxim_kruchkouski.class_work_selenium.pages.onliner.cart_page import CartPage
from maxim_kruchkouski.class_work_selenium.pages.onliner.category_catalog_page import CATALOGPAGE


"""
1. Перейти на сайт https://www.onliner.by/
2. Ввести в строку поиска iphone11
3. Переключиться на барахолку
4. Кликнуть на товар
5. Проверить header
6. Добавить товар в корзину
7. Перейти в корзину
8. Проверить наличие товара в корзине
9. Удалить товар из корзины
"""

iphone_text = "Apple iPhone"

def test_input(driver):
    home_page = HomePage(driver=driver)
    home_page.go_to_site()
    home_page.send_text_in_input("iphone11")
    home_page.switch_to_flea_market()
    elements = home_page.get_search_results_from_catalog()
    home_page.click_on_element_by_index(web_elem_list=elements, index=3)
    home_page.switch_to_current_window()
    product_page = ProductPage(driver=driver)
    headline_text = product_page.get_headline_text()
    assert iphone_text in headline_text
    product_page.click_on_cart_button()
    product_page.click_on_active_cart_button()
    cart_page = CartPage(driver=driver)
    card_text = cart_page.get_card_text()
    assert iphone_text in card_text
    cart_page.delete_product_from_cart()
    assert cart_page.check_absence_of_element_in_cart() is None

"""
1. Перейти на сайт https://www.onliner.by/
2. Перейти в каталог
3. Выбрать раздел "Авто и мото"
4. Выбрать "Автоакустика"
5. Выбрать "Кабели для автоакустики"
6. Убедиться, что мы находимся в разделе "Автоакустика"
7. Кликнуть на первый товар
8. Добавить товар в корзину
9. Перейти в корзину
10. Проверить наличие товара в корзине
11. Удалить товар из корзины
"""

def test_car_audio_cables(driver):
    home_page = HomePage(driver=driver)
    home_page.go_to_site()
    home_page.open_category_catalog()
    catalog_page = CATALOGPAGE(driver=driver)
    catalog_page.open_category_in_catalog_auto_and_moto()
    catalog_page.open_section_car_acoustics()
    name = catalog_page.get_name_section()
    assert name == "Автоакустика"
    catalog_page.open_subsection_cables_for_acoustics()
    catalog_page.click_on_the_first_product()
    product_page = ProductPage(driver=driver)
    product_page.click_on_cart_button()
    product_page.click_on_active_cart_button()
    cart_page = CartPage(driver=driver)
    card_text = cart_page.get_card_text()
    assert card_text is not None
    cart_page.delete_product_from_cart()
    assert cart_page.check_absence_of_element_in_cart() is None
"""
СЦЕНАРИЙ ДОЛЖЕН ВЫПОЛНЯТЬ СЛЕДУЮЩИЕ ДЕЙСТВИЯ:
1. Открыть браузер и развернуть на весь экран.
2. Зайти на shop.by.
3. Перейти к разделу "Компьютеры" и выбрать "Ноутбуки".
4. Задать параметры поиска:
	- Производитель: Xiaomi,Lenovo, Dell, HP
	- Цена: от 1500 руб. до 3000 руб.
	- Размер экрана от 12.5 до 13.4 дюймов.
5. Нажать кнопку Посмотреть * товаров.(неактуальный шаг для онлайнера)! 
6. Определить количество элементов на странице.
7. Отсортировать список по цене (по возрастанию).
8. Запомнить первый элемент в списке.
9. Отсортировать список по цене в обратном порядке.
10. Убедиться, что сохраненный элемент находится в другом конце списка.
11. Закрыть браузер.

ПРИМЕЧАНИЯ:
Предполагается полная свобода в действиях. При оценке в обязательном порядке будут учитываться:
- работоспособность сценария в браузере на ваш выбор;
- проверки осуществляемые во время выполнения теста;
 гибкость и адаптивность сценария (насколько просто заменить часть входных данных, например, изменить диагональ или цену);
стабильность работы сценария (обработка ошибок и исключений);
инструкция/скрипт по настройке запуска тестов"""


def test_soring_filters(driver):
    home_page = HomePage(driver)
    home_page.go_to_site()
    home_page.open_category_catalog()
    catalog_page = CATALOGPAGE(driver)
    catalog_page.open_category_computer_and_networks()
    catalog_page.open_section_laptops_computers_monitors()
    catalog_page.open_subsection_laptops()
    catalog_page.click_show_more("2")
    catalog_page.click_on_check_box_laptop("Xiaomi")
    catalog_page.click_on_check_box_laptop("Lenovo")
    catalog_page.click_on_check_box_laptop("Dell")
    catalog_page.click_on_check_box_laptop("HP")
    catalog_page.input_min_price("1500")
    catalog_page.input_max_price('3000')
    catalog_page.input_screen_diagonal_min('12.5')
    catalog_page.input_screen_diagonal_max('13.4')
    all_elements_on_page = catalog_page.get_all_elements_on_page()
    assert len(all_elements_on_page) == 30
    catalog_page.open_soring_filters()
    catalog_page.filter_selections("Дешевые")
    first_product_name = catalog_page.get_name_product('1')
    catalog_page.open_soring_filters()
    catalog_page.filter_selections("Дорогие")
    catalog_page.open_pagination()
    get_last_pages_switch_buttons = catalog_page.get_pages_switch_buttons()
    catalog_page.switch_to_last_page(get_last_pages_switch_buttons)
    get_last_product_name = catalog_page.get_last_product_name()
    last_product_name = catalog_page.get_name_product(get_last_product_name)
    assert first_product_name == last_product_name