import json
import requests

"""
Получить список иностранных валют у НБРБ (см. http://www.nbrb.by/APIHelp/ExRates)
Напечатать курс USD и EUR к BYN
Напечатать только наименования валют USD, COP, MNT, ESP(на русском, белорусском и английском языках)
Получить курс USD к BYN на 6 февраля 2019г.
Посчитать сколько можно было купить USD по курсу 6 февраля 2019г и сегодня имея на руках 100 BYN.
"""
response = requests.get("https://www.nbrb.by/api/exrates/currencies")
print(response.status_code)
assert response.status_code == 200, f"Failed to request. Response: {response}"
assert response.status_code == requests.codes.ok, f"Failed to request. Response: {response}"
data = json.loads(response.text)


curr = ["USD", "EUR"]

cur_ids = []

for cur in data:
    if cur["Cur_Abbreviation"] in curr:
        cur_ids.append(cur["Cur_ID"])


for cur_id in cur_ids:
    response2 = requests.get(f"https://www.nbrb.by/api/exrates/rates/{cur_id}")
    if response2.status_code == 200:
        print(json.loads(response2.text)["Cur_OfficialRate"])

curr2 = ["USD", "COP", "MNT", "ESP"]

for cur_abb in data:
    if cur_abb["Cur_Abbreviation"] in curr2:
        print(cur_abb["Cur_Name"], cur_abb["Cur_Name_Bel"], cur_abb["Cur_Name_Eng"])

response = requests.get("https://www.nbrb.by/api/exrates/rates/145?ondate=2019-2-6")
print(response.status_code)
data = json.loads(response.text)['Cur_OfficialRate']
print(data)

print(f'december 6,2019 = {100/2.1579}')
print(f'today = {100/2.5312}')


# # print(response.content)
# # print(response.json())
# print(response.headers)
# import requests
# from requests.exceptions import HTTPError
#
# for url in ['https://api.github.com', 'https://api.github.com/invalid']:
#     try:
#         response = requests.get(url)
#         response.raise_for_status()
#     except HTTPError as http_err:
#         print(f'HTTP error occurred: {http_err}')
#     else:
#         print('Success!')
