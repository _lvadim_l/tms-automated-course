class Store:

    store = dict()

    def __init__(self, item, price: int):
        self.item = item
        self.price = price

    def add_item(self):
        new_dict = []
        new_dict.append([self.item, self.price])
        self.store.update(new_dict)

    def show_store_list(self):
        print(self.store)
    
    def remove_item(self):
        for key in self.store.copy():
            self.store.pop(key)

    def overall_price_no_discount(self):
        summ = 0
        for i in self.store.values():
            summ += i
        return summ

    def overall_price_discount(self, procent):
        s = self.overall_price_no_discount()
        total_s = (s * procent) / 100
        return s - total_s


a = Store('item1', 20)
b = Store('item2', 30)
c = Store('item3', 30)
a.add_item()
b.add_item()
c.add_item()
c.show_store_list()
print('Сумма без скиндки = ', c.overall_price_no_discount())
print('Сумма со скиндкой = ', c.overall_price_discount(20))