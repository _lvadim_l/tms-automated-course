class Student:
    students = {}

    def __init__(self, name, money):
        self.name = name
        self.money = money

    def add_student(self):
        self.students[self.name] = self.money
        
    def list_all_student(self):
        return self.students

    def most_money(self):
        if self.students['Sergei'] == self.students['Ivan'] == self.students['Oleg']:
            return 'All'
        return max(self.students)

sergei = Student("Sergei", 2)
ivan = Student("Ivan", 1)
oleg = Student("Oleg", 0)

sergei.add_student()
ivan.add_student()
oleg.add_student()

print(sergei.list_all_student())
print('Больше всего денег: ', sergei.most_money())