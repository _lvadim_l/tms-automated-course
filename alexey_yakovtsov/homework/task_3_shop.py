class Shop:
    
    shop = {}

    def __init__(self, product_name, price):
        self.product_name = product_name
        self.price = price

    def add_product(self):
        self.shop[self.product_name] = self.price

    def add_price(self, new_product, new_price):
        self.shop[new_product] = new_price

    def shop_list(self):
        return self.shop.items()

    @property
    def sum_overall_price(self):
        summ = 0
        for i in self.shop.keys():
            summ += self.shop.get(i)
        return summ

p1 = Shop('tovar1', 100)
p2 = Shop('tovar2', 200)

p1.add_product()
p2.add_product()

p1.add_price('tovar3', 100)

print(p1.shop_list())

print('Сумма всех добавленых цен: ', p1.sum_overall_price)