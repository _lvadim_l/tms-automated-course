
class Students:

    students_list = []

    def __init__(self, fio: str, group: int, progress: int):
        self.fio = fio
        self.group = group
        self.progress = progress

    def add_student(self):
        self.students_list.append([self.fio, self.group, self.progress])

    def print_student_list(self):
        return self.students_list

    def good_progress(self):
        for i in self.students_list:
            if i[2] == 5 or i[2] == 6:
                print(f'Good student is {i[0]} from group {i[1]}')

    def group_of_student(self, group: int):
        for i in self.students_list:
            if i[1] == group:
                print(f'Found student: Name - {i[0]}, Group - {i[1]}, Progress - {i[2]}')

    def auto_exam(self):
        for i in self.students_list:
            if i[2] >= 7:
                print(f'The student {i[0]} has passed the exam automatically' )
    
a = Students('student', 2, 3)
b = Students('stundent2', 1, 5)
c = Students('stundent3', 3, 8)
a.add_student()
b.add_student()
c.add_student()

print(a.print_student_list())
a.good_progress()
a.group_of_student(3)
a.auto_exam()
